Rails.application.routes.draw do

  post "/sendkeys" => "content#getJSON", :as => :getJSON
  post "/sendNo" => "content#sendPush", :as => :sendPush

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  post 'admin/add_supplier_brand/:supplier_id/', to: 'admin/brands#add_supplier_brand', as: :add_supplier_brand
  get 'admin/new_supplier_brand/:supplier_id/', to: 'admin/brands#new_supplier_brand', as: :new_supplier_brand
  delete 'admin/remove_supplier_brand/:supplier_id/', to: 'admin/brands#remove_supplier_brand', as: :remove_supplier_brand

  delete 'admin/remove_supplier_category_part/:supplier_id/', to: 'admin/brands#remove_supplier_category_part', as: :remove_supplier_category_part

  post 'admin/add_supplier_category/:supplier_id/', to: 'admin/categories#add_supplier_category', as: :add_supplier_category
  get 'admin/new_supplier_category/:supplier_id/', to: 'admin/categories#new_supplier_category', as: :new_supplier_category
  delete 'admin/remove_supplier_category/:supplier_id/', to: 'admin/categories#remove_supplier_category', as: :remove_supplier_category

  post 'admin/add_user/:coupon_id/', to: 'admin/coupons#add_user', as: :add_user
  post 'admin/assign_all/:coupon_id/', to: 'admin/coupons#assign_all', as: :assign_all
  get 'admin/new_user_coupon/:coupon_id/', to: 'admin/coupons#new_user_coupon', as: :new_user_coupon
  delete 'admin/remove_coupon_user/:coupon_id/', to: 'admin/coupons#remove_coupon_user', as: :remove_coupon_user
  delete 'admin/category_sub_part_items/:category_sub_part_id', to: 'admin/category_part_items#destroy_category_sub_part', as: :remove_category_sub_part
  get 'open_image_to_process/:id', to: 'admin/item_types#open_image_to_process', as: :open_image_to_process
  
  get 'active/:id', to: 'admin/part_catalogs#active', as: :active
  get 'active_model/:id', to: 'admin/api_models#active_model', as: :active_model
  get 'active_part/:id', to: 'admin/api_parts#active_part', as: :active_part
  get 'open_image_to_process_part/:id', to: 'admin/api_parts#open_image_to_process_part', as: :open_image_to_process_part
  post 'create_hot_spot', to: 'admin/item_types#create_hot_spot', as: :create_hot_spot
  delete 'delete_hot_spot/:id', to: 'admin/item_types#delete_hot_spot', as: :delete_hot_spot
  
  get  '/', to: 'public/landing#index', as: :landing_index
  post  '/cotizar', to: 'public/landing#quote_create'
  get  '/quote', to: 'public/landing#quote', as: :landing_quote
  get  '/models', to: 'public/landing#models'
  get  '/get_models', to: 'public/landing#get_models'
  get  '/login_popup', to: 'public/landing#login_popup'
  get  '/get_fields', to: 'public/landing#get_fields'
  get  '/get_brands', to: 'public/landing#get_brands'
  get  '/get_date', to: 'public/landing#get_date'
  get  '/part_items', to: 'public/landing#part_items'
  get  '/category_parts', to: 'public/landing#category_parts'
  get  '/brands', to: 'public/landing#brands'
  post '/cotizar/ingreso', to: 'public/landing#quote_with_session', as: :landing_quote_with_session
  post '/login_with_address', to: 'public/landing#login_with_address', as: :landing_login_with_address
  post '/cotizar/registro', to: 'public/landing#quote_with_registration', as: :landing_quote_with_registration
  post '/cotizar/omniauth', to: 'public/landing#quote_with_omniauth', as: :landing_quote_with_omniauth
  post '/quote_with_current_user', to: 'public/landing#quote_with_current_user', as: :quote_with_current_user
  post '/create_quotation_request', to: 'public/landing#after_session_create_quotation_request'
  get 'get_item_sub_parts/', to: 'public/landing#category_sub_parts'
  get 'checkout/', to: 'public/landing#checkout'
  get 'cart/', to: 'public/landing#cart'
  get 'final_cart/', to: 'public/landing#final_cart'
  get 'remove_cart/', to: 'public/landing#remove_cart'
  get 'empty_cart/', to: 'public/landing#empty_cart'
  get 'update_quantity/', to: 'public/landing#update_quantity'
  get 'get_part_catalogs/', to: 'public/landing#get_part_catalogs'
  post 'checkout_process/', to: 'public/landing#checkout_process'
  post 'get_vehicle/', to: 'public/landing#get_vehicle'

  namespace :public, path: "" do 
    namespace :api do 
      resources :brands, only: [:index, :show]
      resources :models, only: [:index]
    end
  end

  namespace :api_data, path: "" do
    resources :catalogs
    resources :catalog_types
    resources :api_catalogs
    resources :api_models
    resources :api_sub_models
    resources :api_vehicles do
      collection do
        get 'user_vehicles'
      end
    end
    resources :api_vehicle_models
    resources :part_catalogs
    resources :api_parts do
      collection do
        get 'get_sub_part'
      end
    end
  end

  resources :category_part_items do
    get :autocomplete_category_part_item_name, :on => :collection
  end

  resources :vehicles
  resources :messages do 
    collection do 
      get :new_message
      post :send_message
    end
  end
  namespace :private, path: "" do 
    namespace :api do
      resources :quotations, only: [:index, :show]
      resources :quotation_requests do
        get "quotation_request_detail"
        resources :quotations, only: [:index, :show]
      end
      get 'get_user_vehicle', to: 'quotation_requests#get_user_vehicle'
      resources :users, only: [:index]
      #resources :suppliers_brands, only: [:index, :create, :destroy]

      get '/mi-perfil', to: 'profile#show', as: :profile_show
      post '/mi-perfil', to: 'profile#update', as: :profile_update
      get '/mi-perfil-edit', to: 'profile#edit', as: :profile_edit
      get '/new_address', to: 'profile#new_address', as: :new_address
      post '/create_address', to: 'profile#address_create', as: :create_address
      delete '/destroy_address/:id', to: 'profile#destroy_address', as: :destroy_address
      get '/update_password', to: 'profile#update_password', as: :update_password
      get '/edit_notify_preference', to: 'profile#edit_notify_preference', as: :edit_notify_preference
      post '/update_notify_preference', to: 'profile#update_notify_preference', as: :update_notify_preference
      get '/edit_address/:id', to: 'profile#edit_address', as: :edit_address
      post '/update_address/:id', to: 'profile#update_address', as: :update_address
      namespace :suppliers do 
        resources :brands, only: [:index, :create, :destroy, :new]
        resources :quotation_requests
        resources :quotations
        get '/approve_quote/:id', to: 'quotations#approve_quote', as: :approve_quote
      end
      get 'show_details/:id', to: 'quotations#show_details', as: :show_details
    end
  end

  namespace :employees, path: "" do
    resources :quotation_requests
    resources :quotations
  end

  devise_for :users, path: '/', path_names: { 
    sign_in: 'ingresar', 
    sign_out: 'salir', 
    registration: 'registro',
    sign_up: ''
  }, controllers: {
    registrations: 'public/registrations',
    sessions: 'public/sessions',
    omniauth_callbacks: 'public/omniauth_callbacks',
    passwords: 'public/passwords'
  }
  devise_for :employees, path: '/', path_names: {
    sign_in: 'employee_sign_in',
    sign_out: 'employee_sign_out',
    registration: 'employee_registration'
  }, controllers: { 
    sessions: "employees/sessions",
    registrations: 'employees/registrations',
  }
  get '/employee_dashboard', to: 'employees/dashboard#index', as: :employee_dashboard

  get '/app', to: 'private/dashboard#index', as: :dashboard_index
  get '/notices', to: 'private/dashboard#notices', as: :notices
  get '/app/:some', to: 'private/dashboard#index'
  get '/app/:some/:some_again', to: 'private/dashboard#index'
  get '/app/:some/:some_again/:oh_god', to: 'private/dashboard#index'
  get '/app/:some/:some_again/:oh_god/:how_the_fuck_i_do_a_wildcard', to: 'private/dashboard#index'

  resources :contacts

  resources :replies
  get 'how_its_works', to: 'pages#how_its_works'
  get 'about_us', to: 'pages#about_us'

  resources :questions do
    collection do
      get "show_pdf"
    end
    resources :replies
  end
  get 'all_questions', to: 'questions#all_questions'
  post 'filter_question', to: 'questions#filter_question'
  resources :quote_questions
  resources :referral_links
  resources :referral_users
  resources :orders do
    collection do
      get 'supplier_detail'
    end
  end

  resources :order_items, only:[] do
    get 'delivered'
    get 'approve'
    resources :courier_jobs, only: [:new, :create]
  end

  resources :courier_jobs, only: [:index, :edit, :update]
  resources :notifications, only: [:index]
  
  #****paypal******
  post "/orders/:id" => "orders#show"
  post "/hook" => "orders#hook"
  get "paypal_payment" => "orders#paypal"

  #*****mercado_pago*** 
  get "mercado_proccess" => "orders#mercado_payment_proccess"
  post "/mercado_hook" => "orders#mercado_hook"
  get "mercado_return", to: "orders#mercado_return"

  #*****payment_notification****
  get "payment_method" => "orders#payment_method"
  get "payment_failed" => "orders#payment_failed"
  
  #******onepay****
  get "onepay" => "orders#onepay"
  get 'onepay_hook' => 'orders#onepay_hook'
  get 'commit' => 'orders#commit'

  #****carts****
  resources :carts
  get 'entrega', to: 'carts#entrega'
  post 'finalizar', to: 'carts#finalizar'
  get 'user_cart', to: 'carts#cart'
  get 'change_address', to: 'carts#change_address'
  get 'apply_coupon', to: 'carts#apply_coupon'
  get 'remove_coupon', to: 'carts#remove_coupon'
  get 'get_cities', to: 'carts#get_cities'
  delete 'delete_cart', to: 'carts#delete_cart'

end