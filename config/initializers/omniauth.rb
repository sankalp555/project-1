Rails.application.config.middleware.use OmniAuth::Builder do
  provider :facebook, ENV['FACEBOOK_KEY'], ENV['FACEBOOK_SECRET'], { scope: 'email', display: 'popup' }
end

OmniAuth.config.full_host = Rails.env.production? ? 'live.com' : 'live.com'
