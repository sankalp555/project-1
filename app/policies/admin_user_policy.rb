class AdminUserPolicy < ApplicationPolicy
  def index?
    is_superadmin?
  end

  def show?
    is_superadmin?
  end

  def new?
    is_superadmin?
  end

  def create?
    is_superadmin?
  end

  def update?
    is_superadmin?
  end

  def destroy?
    is_superadmin?
  end

  class Scope < Struct.new(:user, :scope)
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      scope
    end
  end
end
