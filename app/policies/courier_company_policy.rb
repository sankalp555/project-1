class CourierCompanyPolicy < ApplicationPolicy
  def index?
    true
  end

  def show?
    true
  end

  def new?
    true
  end

  def create?
    true
  end

  def update?
    true
  end

  def destroy?
    true
  end

  class Scope < Struct.new(:courier_company, :scope)
    attr_reader :courier_company, :scope

    def initialize(courier_company, scope)
      @courier_company = courier_company
      @scope = scope
    end

    def resolve
      scope
    end
  end
end
