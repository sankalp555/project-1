class NoticePolicy < ApplicationPolicy
  def index?
    true
  end

  def show?
    true
  end

  def new?
    true
  end

  def create?
    true
  end

  def update?
    true
  end

  def destroy?
    true
  end

  class Scope < Struct.new(:notice, :scope)
    attr_reader :notice, :scope

    def initialize(user, scope)
      @notice = notice
      @scope = scope
    end

    def resolve
      scope
    end
  end
end
