# == Schema Information
#
# Table name: brands
#
#  id          :bigint(8)        not null, primary key
#  name        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  category_id :bigint(8)
#
# Indexes
#
#  index_brands_on_category_id  (category_id)
#

class BrandSerializer < ActiveModel::Serializer
  attributes :id, :name
end
