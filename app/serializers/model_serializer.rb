# == Schema Information
#
# Table name: models
#
#  id         :bigint(8)        not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  brand_id   :bigint(8)
#
# Indexes
#
#  index_models_on_brand_id  (brand_id)
#
# Foreign Keys
#
#  fk_rails_...  (brand_id => brands.id)
#

class ModelSerializer < ActiveModel::Serializer
  attributes :id, :name
end
