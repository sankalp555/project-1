# == Schema Information
#
# Table name: quotation_requests
#
#  id         :bigint(8)        not null, primary key
#  status     :string           default("pending")
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :bigint(8)
#
# Indexes
#
#  index_quotation_requests_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#

class QuotationRequest < ApplicationRecord
  paginates_per 20
  belongs_to :user
  has_many :quotation_request_parts, dependent: :destroy
  has_many :quotations
  has_many :messages
  has_many :notifications, as: :notifiable

  YEAR = (1950..2018).to_a.reverse
  STATUS = ['pending','in_process', 'approved', 'cancelled']

  scope :pending,   -> { where(status: 'pending')}
  scope :in_process,   -> { where(status: 'in_process')}
  scope :approved,   -> { where(status: 'approved')}
  scope :cancelled,   -> { where(status: 'cancelled')}
  
  scope :today, -> { where(created_at: Time.now.beginning_of_day..Time.now.end_of_day) }
  scope :this_week, -> { where(created_at: Time.now.beginning_of_week..Time.now.end_of_week) }
  scope :this_month, -> { where(created_at: Time.now.beginning_of_month..Time.now.end_of_month) }
  scope :this_year, -> { where(created_at: Time.now.beginning_of_year..Time.now.end_of_year) }

  validates :status, inclusion: { in: STATUS, message: "Por favor proporcione un estado válido" }

  def as_json_summary
    parts_count = quotation_request_parts.count
    brand_list = quotation_request_parts.map { |it| it.model.brand.name }.uniq
    {
      id: self.id,
      created_at: self.created_at,
      brands: brand_list,
      parts_count: parts_count
    }
  end
end
