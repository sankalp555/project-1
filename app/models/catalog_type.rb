# == Schema Information
#
# Table name: catalog_types
#
#  id           :bigint(8)        not null, primary key
#  catalog_type :string
#  name         :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class CatalogType < ApplicationRecord
  validates :name, uniqueness: { message: "Catalog name should be unique!" }
  validates :catalog_type, uniqueness: { message: "Catalog type already used!" }
end
