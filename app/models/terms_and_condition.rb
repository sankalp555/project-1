# == Schema Information
#
# Table name: terms_and_conditions
#
#  id         :bigint(8)        not null, primary key
#  content    :text
#  title      :string
#  updated_on :datetime
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class TermsAndCondition < ApplicationRecord
end
