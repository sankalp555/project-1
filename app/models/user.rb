# == Schema Information
#
# Table name: users
#
#  id                     :bigint(8)        not null, primary key
#  cellular               :string
#  current_sign_in_at     :datetime
#  current_sign_in_ip     :inet
#  deleted_at             :datetime
#  dob                    :date
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  last_name              :string
#  last_sign_in_at        :datetime
#  last_sign_in_ip        :inet
#  name                   :string
#  office                 :string
#  password_salt          :string
#  phone                  :string
#  reedim_amount          :float            default(0.0)
#  remember_created_at    :datetime
#  reset_password_sent_at :datetime
#  reset_password_token   :string
#  sign_in_count          :integer          default(0), not null
#  status                 :boolean          default(TRUE)
#  total_purchasing       :decimal(, )      default(0.0)
#  web_push_subscription  :json
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  cart_id                :bigint(8)
#
# Indexes
#
#  index_users_on_cart_id               (cart_id)
#  index_users_on_deleted_at            (deleted_at)
#  index_users_on_email                 (email)
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#

class User < ApplicationRecord
  acts_as_paranoid
  include ActiveModel::Validations

  has_many :quotation_requests
  has_many :suppliers_users
  has_many :suppliers, through: :suppliers_users
  has_many :user_coupons
  has_many :coupons, through: :user_coupons
  has_many :api_vehicles
  has_many :referral_links
  has_many :addresses
  has_many :addresses, as: :addressable, dependent: :destroy
  has_many :orders, dependent: :destroy
  belongs_to :cart, optional: true
  has_one :user_notify_preference
  has_many :quotations
  has_many :attachments
  accepts_nested_attributes_for :addresses, reject_if: :all_blank, allow_destroy: true
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :encryptable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable, 
         omniauth_providers: [:google_oauth2, :facebook], encryptor: :authlogic_sha512


  #validates_strength_of :password, :with => :email
  validates :name, :phone, :last_name, presence: true

  validates :email, :uniqueness => {:allow_blank => true}
  
  has_many :messages, foreign_key: :sender_id, class_name: 'Message'

  REGISTRATION_PARAMS = [:email, :name, :last_name, :password, :password_confirmation, :phone]

  after_create :create_cart, :create_user_notify_preference, :send_welcome_mail
  after_save :check_referal_amount, :if => Proc.new {|user| user.total_purchasing_changed? }

  
  def send_welcome_mail
    UserMailer.send_new_user_message(self).deliver
  end


  def self.from_omniauth(access_token)
    data = access_token.info
    user = User.where(email: data['email']).first

    # Uncomment the section below if you want users to be created if they don't exist
    unless user
      user = User.create(
        name: data['name'],
        phone: "+5600000000",
        email: data['email'],
        password: Devise.friendly_token[0,20]
      )
    end
    user
  end

  def profile_summary
    result = self.as_json
    result[:roles] = suppliers_users.map do |role|
      {
        role: role.role,
        supplier: role.supplier
      }
    end
    result
  end

  def generate_reset_password_token_and_notify
    token = set_reset_password_token
    UserMailer.new_supplier_user(self, token).deliver_now # use deliver_later when sidekiq
  end

  def supplier
    suppliers&.first
  end

  def supplier_brands
    supplier&.brands
  end

  def supplier_catalogs
    supplier&.part_catalogs
  end

  def create_cart
    if self.suppliers.blank?
      crt = self.build_cart(total_amount: 0)
      crt.save
    end
  end

  def create_user_notify_preference
    if self.suppliers.blank?
      unp = self.build_user_notify_preference
      unp.save
    end
  end

  private
  def check_referal_amount
    referral = ReferralLink.find_by_refer_to(self.email)
    referral_instruction = referral.referral_instruction if referral.present?
    if referral.present? && (self.total_purchasing >= referral_instruction.referral_max_amount) && !referral.awarded
      referred_by = referral.user
      ref_amount = referred_by.reedim_amount
      ref_amount += referral_instruction.amount_to_be_awarded_per_refer
      referred_by.update_column('reedim_amount', ref_amount)
      UserMailer.referral_amount_added(referred_by, referral)
    end
  end
end
