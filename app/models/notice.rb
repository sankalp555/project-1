# == Schema Information
#
# Table name: notices
#
#  id          :bigint(8)        not null, primary key
#  discription :text
#  notice_type :string
#  page        :string
#  publish     :boolean
#  title       :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Notice < ApplicationRecord
  TYPE = ['notice', 'tips']
  Page = [ 'Dashboard', 'Mis vehiculos', 'Mis pedidos', 'Mis compras']
  scope :dashboard, -> { where page: "Dashboard" }
  scope :mis_vehiculos, -> { where page: "Mis vehiculos" }
  scope :mis_pedidos, -> { where page: "Mis pedidos" }
  scope :mis_compras, -> { where page: "Mis compras" }
  scope :published, -> { where("publish = true") }
  scope :notice, -> { where notice_type: "notice" }
  scope :tip, -> { where notice_type: "tips"  }
end
