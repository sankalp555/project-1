# == Schema Information
#
# Table name: cart_items
#
#  id                :bigint(8)        not null, primary key
#  delivery_type     :string
#  quantity          :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  cart_id           :bigint(8)
#  order_id          :integer
#  quotation_part_id :bigint(8)
#
# Indexes
#
#  index_cart_items_on_cart_id            (cart_id)
#  index_cart_items_on_order_id           (order_id)
#  index_cart_items_on_quotation_part_id  (quotation_part_id)
#

class CartItem < ApplicationRecord
  belongs_to :cart
  belongs_to :quotation_part
end
