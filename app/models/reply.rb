# == Schema Information
#
# Table name: replies
#
#  id          :bigint(8)        not null, primary key
#  read        :boolean          default(FALSE)
#  reply       :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  question_id :bigint(8)
#
# Indexes
#
#  index_replies_on_question_id  (question_id)
#

class Reply < ApplicationRecord
  belongs_to :question
end
