# == Schema Information
#
# Table name: reply_notifications
#
#  id             :bigint(8)        not null, primary key
#  read           :boolean          default(FALSE)
#  reply          :text
#  replyable_type :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  replyable_id   :bigint(8)
#
# Indexes
#
#  index_reply_notifications_on_replyable_type_and_replyable_id  (replyable_type,replyable_id)
#

class ReplyNotification < ApplicationRecord
  belongs_to :replyable, polymorphic: true

  def send_mail_to_contact_user(email, reply)
    ReplyMailer.reply_to_contact(email, reply).deliver_now # use deliver_later when sidekiq
  end
end
