# == Schema Information
#
# Table name: referral_users
#
#  id               :bigint(8)        not null, primary key
#  purchase         :boolean          default(FALSE)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  referral_link_id :bigint(8)
#  referral_user_id :integer
#
# Indexes
#
#  index_referral_users_on_referral_link_id  (referral_link_id)
#

class ReferralUser < ApplicationRecord
end
