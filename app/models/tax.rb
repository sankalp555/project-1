# == Schema Information
#
# Table name: taxes
#
#  id           :bigint(8)        not null, primary key
#  active       :boolean          default(FALSE)
#  lower_amount :integer
#  name         :string
#  percent      :decimal(, )
#  upper_amount :integer
#  year         :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Tax < ApplicationRecord
end
