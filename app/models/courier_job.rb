# == Schema Information
#
# Table name: courier_jobs
#
#  id                 :bigint(8)        not null, primary key
#  final_date         :date
#  initial_date       :date
#  message            :text
#  send_detail        :boolean          default(TRUE)
#  status             :string
#  tracking_number    :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  courier_company_id :bigint(8)
#  order_id           :bigint(8)
#  order_item_id      :integer
#
# Indexes
#
#  index_courier_jobs_on_courier_company_id  (courier_company_id)
#  index_courier_jobs_on_order_id            (order_id)
#

class CourierJob < ApplicationRecord
  belongs_to :courier_company
  belongs_to :order_item

  validates :tracking_number, :initial_date, :final_date, presence: true

  before_create :check_date
  after_create :update_item_and_user
  delegate :name, to: :courier_company, prefix: true


  def update_item_and_user
    self.order_item.update_attributes(status: 'approved')
    UserMailer.courier_job(self.order_item.order.user, self.order_item.id).deliver_now
  end

  def check_date
    return if (initial_date.blank? || final_date.blank?)
    errors.add(:initial_date, "debe ser hoy o en el futuro.")  if initial_date <  Date.today
    errors.add(:final_date, "debe ser hoy o en el futuro.")  if final_date <=  Date.today
    errors.add(:final_date, "debe ser posterior a la fecha inicial")  if final_date <=  initial_date
    errors.add(:initial_date, "debe ser antes de la fecha final")  if initial_date >=  final_date
  end

end
