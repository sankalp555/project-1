# == Schema Information
#
# Table name: referral_instructions
#
#  id                             :bigint(8)        not null, primary key
#  active                         :boolean          default(FALSE)
#  amount_to_be_awarded_per_refer :integer
#  expire_in_days                 :integer
#  max_no_of_refer                :integer
#  refer_description              :text
#  referral_max_amount            :integer
#  referral_type                  :string
#  created_at                     :datetime         not null
#  updated_at                     :datetime         not null
#

class ReferralInstruction < ApplicationRecord
  scope :active, -> { where active: true }
  has_many :referral_links
end
