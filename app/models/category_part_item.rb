# == Schema Information
#
# Table name: category_part_items
#
#  id           :bigint(8)        not null, primary key
#  active       :boolean          default(FALSE)
#  name         :string
#  price        :decimal(, )
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  item_type_id :bigint(8)
#
# Indexes
#
#  index_category_part_items_on_item_type_id  (item_type_id)
#

#  sub_parts    :boolean

#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  item_type_id :bigint(8)
#
# Indexes
#
#  index_category_part_items_on_item_type_id  (item_type_id)
#

class CategoryPartItem < ApplicationRecord

	belongs_to :item_type
  has_many :category_sub_options
	has_many :category_sub_part_items, :dependent => :destroy
  has_many :attachments, as: :attached_item, dependent: :destroy
  has_one :hot_spot, :dependent => :destroy
  accepts_nested_attributes_for :attachments, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :category_sub_part_items, reject_if: :all_blank, allow_destroy: true

  def sub_parts
    sub_parts = []
    if self.category_sub_part_items.present?
      self.category_sub_part_items.each do |sub_part|
        opt = CategorySubOption.find_by_id(sub_part.category_sub_option_id)
        sub_parts << opt.name if opt.present?
      end
    end
    sub_parts.compact.join(', ')
  end
end
