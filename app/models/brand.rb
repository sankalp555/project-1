# == Schema Information
#
# Table name: brands
#
#  id          :bigint(8)        not null, primary key
#  name        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  category_id :bigint(8)
#
# Indexes
#
#  index_brands_on_category_id  (category_id)
#

class Brand < ApplicationRecord
  has_many :models, dependent: :destroy
  has_many :brands_suppliers, dependent: :destroy
  has_one :attachment, as: :attached_item, dependent: :destroy
  accepts_nested_attributes_for :attachment, reject_if: :all_blank, allow_destroy: true
  has_many :suppliers, through: :brands_suppliers, dependent: :destroy
  belongs_to :category
end
