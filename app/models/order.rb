# == Schema Information
#
# Table name: orders
#
#  id                  :bigint(8)        not null, primary key
#  delivery_status     :string
#  notification_params :string
#  payment_type        :string
#  purchased_at        :datetime
#  reference_number    :string
#  status              :string
#  total_pay_amount    :decimal(, )
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  coupon_id           :integer
#  supplier_id         :bigint(8)
#  transaction_id      :string
#  user_id             :bigint(8)
#
# Indexes
#
#  index_orders_on_supplier_id  (supplier_id)
#  index_orders_on_user_id      (user_id)
#

class Order < ApplicationRecord
  paginates_per 20
  belongs_to :user
  has_many :order_items, dependent: :destroy
  has_many :courier_jobs

  PAYMENTSTATUS = ["Confirmation Pending", "Failed", "Success", "Cancel"]
  ORDERSTATUS = ["confirmation_pending", "approved", "in_process", 'delivered']
  DELIVERYSTATUS = ["Not Shipped Yet", "En Tránsito"]
  
  scope :inprocess,   -> { where(status: 'in_process')}
  
  validates :status, inclusion: { in: ORDERSTATUS, message: "Por favor proporcione un estado válido" }
  scope :today, -> { where(created_at: Time.now.beginning_of_day..Time.now.end_of_day) }
  scope :this_week, -> { where(created_at: Time.now.beginning_of_week..Time.now.end_of_week) }
  scope :this_month, -> { where(created_at: Time.now.beginning_of_month..Time.now.end_of_month) }
  scope :this_year, -> { where(created_at: Time.now.beginning_of_year..Time.now.end_of_year) }

  after_create :create_reference_number
  
  def create_reference_number
    flag = false
    until flag do
      number = 5.times.map{rand(10)}.join
      order = Order.where(reference_number: number)
      if order.blank?
        self.reference_number = number
        self.save
        flag = true
      end
    end
  end

  def paypal_url(return_path, price, cart_id)
    values = {
        business: "seller_inparts@gmail.com",
        cmd: "_xclick",
        upload: 1,
        return: "#{Rails.application.secrets.app_host}#{return_path}",
        invoice: id,
        amount: price,
        # quotation_part_id: order_params[:quotation_part_id],
        # quantity: order_params[:quantity],
        notify_url: "#{Rails.application.secrets.app_host}/hook?cart_id="+cart_id.to_s
    }
    "#{Rails.application.secrets.paypal_host}/cgi-bin/webscr?" + values.to_query
  end

  def is_success?
    ["approved", "Completed"].include?(status)
  end
  
end
