# == Schema Information
#
# Table name: referral_links
#
#  id                      :bigint(8)        not null, primary key
#  awarded                 :boolean          default(FALSE)
#  refer_code              :string
#  refer_to                :string
#  used                    :boolean          default(FALSE)
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  referral_instruction_id :bigint(8)
#  user_id                 :bigint(8)
#
# Indexes
#
#  index_referral_links_on_referral_instruction_id  (referral_instruction_id)
#  index_referral_links_on_user_id                  (user_id)
#

class ReferralLink < ApplicationRecord
  belongs_to :user
  belongs_to :referral_instruction

  after_create do
    ReferralMailer.new_referral_user(self).deliver_now # use deliver_later when sidekiq
  end

end
