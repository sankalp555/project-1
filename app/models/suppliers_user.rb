# == Schema Information
#
# Table name: suppliers_users
#
#  id          :bigint(8)        not null, primary key
#  role        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  supplier_id :bigint(8)
#  user_id     :bigint(8)
#
# Indexes
#
#  index_suppliers_users_on_supplier_id  (supplier_id)
#  index_suppliers_users_on_user_id      (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (supplier_id => suppliers.id)
#  fk_rails_...  (user_id => users.id)
#

class SuppliersUser < ApplicationRecord
  belongs_to :supplier
  belongs_to :user
  validate :ensure_unique_role, on: :create
  accepts_nested_attributes_for :user
  
  
  private 

  def ensure_unique_role
    unless self.class.where(supplier: supplier, user: user).count.zero?
      errors.add(:role, "No es posible agregar otro rol a este usuario")
    end
  end
end
