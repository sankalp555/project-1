# == Schema Information
#
# Table name: hot_spots
#
#  id              :bigint(8)        not null, primary key
#  hot_spot_no     :integer
#  note            :text
#  x_position      :float
#  y_position      :float
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  api_part_id     :integer
#  api_sub_part_id :integer
#  attachment_id   :bigint(8)
#
# Indexes
#
#  index_hot_spots_on_attachment_id  (attachment_id)
#

class HotSpot < ApplicationRecord
  belongs_to :item_type
  belongs_to :attachment
  belongs_to :api_sub_part
  belongs_to :api_part
end
