# == Schema Information
#
# Table name: questions
#
#  id                        :bigint(8)        not null, primary key
#  question                  :text
#  read                      :boolean          default(FALSE)
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  quotation_request_part_id :bigint(8)
#
# Indexes
#
#  index_questions_on_quotation_request_part_id  (quotation_request_part_id)
#

class Question < ApplicationRecord
  belongs_to :quotation_request_part
  has_many :replies
  has_many :attachments, as: :attached_item, dependent: :destroy
end
