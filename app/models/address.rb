# == Schema Information
#
# Table name: addresses
#
#  id               :bigint(8)        not null, primary key
#  addressable_type :string
#  city             :string
#  comuna           :string
#  name             :string
#  number           :integer
#  phone            :bigint(8)
#  primary          :boolean          default(FALSE)
#  region           :string
#  street_address   :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  addressable_id   :bigint(8)
#
# Indexes
#
#  index_addresses_on_addressable_id  (addressable_id)
#

class Address < ApplicationRecord
  belongs_to :addressable, polymorphic: true, optional: true
  validates :phone, :presence => {:message => 'Phone number required!'},
                    :numericality => true


  scope :primary_address, -> { where(primary: true)}


  def city_name
  end

  def state_name
  end
end
