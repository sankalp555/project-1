# == Schema Information
#
# Table name: category_sub_part_items
#
#  id                     :bigint(8)        not null, primary key
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  category_part_item_id  :bigint(8)
#  category_sub_option_id :bigint(8)
#
# Indexes
#
#  index_category_sub_part_items_on_category_part_item_id   (category_part_item_id)
#  index_category_sub_part_items_on_category_sub_option_id  (category_sub_option_id)
#

class CategorySubPartItem < ApplicationRecord
  belongs_to :category_part_item
end
