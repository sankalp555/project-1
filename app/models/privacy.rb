# == Schema Information
#
# Table name: privacies
#
#  id         :bigint(8)        not null, primary key
#  content    :text
#  title      :string
#  updated_on :datetime
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Privacy < ApplicationRecord
end
