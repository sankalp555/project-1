# == Schema Information
#
# Table name: item_types
#
#  id               :bigint(8)        not null, primary key
#  active           :boolean          default(FALSE)
#  hot_spot_no      :boolean          default(TRUE)
#  name             :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  category_part_id :bigint(8)
#
# Indexes
#
#  index_item_types_on_category_part_id  (category_part_id)
#

class ItemType < ApplicationRecord
	has_many :category_part_items
	belongs_to :category_part
  has_many :hot_spots
	has_many :attachments, as: :attached_item, dependent: :destroy
  accepts_nested_attributes_for :attachments, reject_if: :all_blank, allow_destroy: true
end
