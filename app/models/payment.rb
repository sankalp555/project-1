# == Schema Information
#
# Table name: payments
#
#  id                 :bigint(8)        not null, primary key
#  authorization_code :string
#  payment_gateway    :string
#  payment_type       :string
#  status             :string
#  total_amount       :decimal(, )      default(0.0)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  order_id           :integer
#

class Payment < ApplicationRecord
	belongs_to :order
end
