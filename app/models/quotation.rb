# == Schema Information
#
# Table name: quotations
#
#  id                   :bigint(8)        not null, primary key
#  flag                 :boolean          default(FALSE)
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  quotation_request_id :bigint(8)
#  supplier_id          :bigint(8)
#  user_id              :bigint(8)
#
# Indexes
#
#  index_quotations_on_quotation_request_id  (quotation_request_id)
#  index_quotations_on_supplier_id           (supplier_id)
#  index_quotations_on_user_id               (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (quotation_request_id => quotation_requests.id)
#  fk_rails_...  (supplier_id => suppliers.id)
#  fk_rails_...  (user_id => users.id)
#

class Quotation < ApplicationRecord
  paginates_per 20
  belongs_to :quotation_request
  belongs_to :supplier
  belongs_to :user
  has_many :quotation_parts, dependent: :destroy
  has_many :notifications, as: :notifyable

  scope :today, -> { where(created_at: Time.now.beginning_of_day..Time.now.end_of_day) }
  scope :this_week, -> { where(created_at: Time.now.beginning_of_week..Time.now.end_of_week) }
  scope :this_month, -> { where(created_at: Time.now.beginning_of_month..Time.now.end_of_month) }
  scope :this_year, -> { where(created_at: Time.now.beginning_of_year..Time.now.end_of_year) }

  after_create :change_request_status

  def change_request_status
    self.quotation_request.update_attributes(status: "in_process")
  end

  def new_quote
    if self.created_at >= 2.days.ago
      return true
    else
      return false
    end
  end

  def quotation_total_amount
    amount = 0
    self.quotation_parts.each do |part|
      amount += (part.price * part.amount rescue 0)
    end
    amount
  end
end
