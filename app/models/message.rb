# == Schema Information
#
# Table name: messages
#
#  id                   :bigint(8)        not null, primary key
#  message              :text
#  read                 :boolean          default(FALSE)
#  sent_at              :datetime
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  order_id             :integer
#  quotation_request_id :bigint(8)
#  receiver_id          :integer
#  sender_id            :integer
#
# Indexes
#
#  index_messages_on_quotation_request_id  (quotation_request_id)
#
# Foreign Keys
#
#  fk_rails_...  (quotation_request_id => quotation_requests.id)
#

class Message < ApplicationRecord
  belongs_to :sender, class_name: 'User'
  belongs_to :receiver, class_name: 'User'
  belongs_to :quotation_request

  has_many :notifications, as: :notifyable
  has_many :attachments, as: :attached_item, dependent: :destroy

  scope :unread,   -> { where(read: false)}
  scope :read,   -> { where(read: true)}
  scope :between, -> (sender_id,receiver_id) do
		where("(messages.sender_id = ? AND messages.receiver_id =?) OR (messages.sender_id = ? AND messages.receiver_id =?)", sender_id,receiver_id, receiver_id, sender_id)
	end

end
