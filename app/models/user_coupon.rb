# == Schema Information
#
# Table name: user_coupons
#
#  id         :bigint(8)        not null, primary key
#  applied    :boolean          default(FALSE)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  coupon_id  :integer
#  user_id    :integer
#

class UserCoupon < ApplicationRecord
	belongs_to :user
	belongs_to :coupon
end
