# == Schema Information
#
# Table name: vehicles
#
#  id            :bigint(8)        not null, primary key
#  chasis_number :string
#  patent        :string
#  year          :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  brand_id      :bigint(8)
#  category_id   :bigint(8)
#  model_id      :bigint(8)
#  user_id       :bigint(8)
#
# Indexes
#
#  index_vehicles_on_brand_id     (brand_id)
#  index_vehicles_on_category_id  (category_id)
#  index_vehicles_on_model_id     (model_id)
#  index_vehicles_on_user_id      (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (brand_id => brands.id)
#  fk_rails_...  (model_id => models.id)
#  fk_rails_...  (user_id => users.id)
#

class Vehicle < ApplicationRecord
  paginates_per 20
  belongs_to :user
  belongs_to :model
  belongs_to :brand

  validates_presence_of :chasis_number, :brand_id, :model_id
  validates_uniqueness_of :patent, :chasis_number
end
