# == Schema Information
#
# Table name: quotation_parts
#
#  id                        :bigint(8)        not null, primary key
#  accepted                  :boolean          default(FALSE)
#  amount                    :integer
#  brand                     :string
#  discount                  :integer
#  price                     :decimal(, )
#  selling_price             :decimal(, )
#  shipping                  :datetime
#  shipping_price            :decimal(, )
#  shipping_type             :string           default("deliver")
#  status                    :string           default("pending")
#  type_of_request           :string           default("original")
#  warranty                  :string
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  quotation_id              :bigint(8)
#  quotation_request_part_id :bigint(8)
#
# Indexes
#
#  index_quotation_parts_on_quotation_id               (quotation_id)
#  index_quotation_parts_on_quotation_request_part_id  (quotation_request_part_id)
#
# Foreign Keys
#
#  fk_rails_...  (quotation_id => quotations.id)
#  fk_rails_...  (quotation_request_part_id => quotation_request_parts.id)
#

class QuotationPart < ApplicationRecord
  belongs_to :quotation
  belongs_to :quotation_request_part
  has_one :order_item

  # after_create :send_mail_to_buyer
  before_save :update_selling_price

  SHIPPING_TYPE = %w(pickup deliver)
  TYPE_OF_REQUEST = %w(original alternative)
  STATUS = ["pending", "approved"]

  def sub_part_name
    id = self.quotation_request_part.api_sub_part_id
    return ApiSubPart.find(id).name
  end

  private

  # def send_mail_to_buyer
  #   QuotationMailer.send_mail_buyer_quotes_created(self).deliver_now
  # end

  def update_selling_price
    supplier = self.quotation.supplier
    markups = supplier.markups.where(active: true)
    if !markups.blank?
      markup_role = markups.last.role
      markup_input = markups.last.input.to_f
      price = self.price.to_f
      new_selling_price = price
      if markup_role=='fix'
        new_selling_price = price+markup_input
      else
        precentage_amount_to_add = (price*markup_input/100)
        new_selling_price = price+precentage_amount_to_add
      end
      self.selling_price = new_selling_price
    else
      self.selling_price = self.price
    end
  end

end
