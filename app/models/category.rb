# == Schema Information
#
# Table name: categories
#
#  id         :bigint(8)        not null, primary key
#  active     :boolean          default(FALSE)
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Category < ApplicationRecord
  has_many :brands
  has_many :categories_suppliers
  has_many :suppliers, through: :categories_suppliers
end