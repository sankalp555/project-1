# == Schema Information
#
# Table name: category_sub_options
#
#  id         :bigint(8)        not null, primary key
#  active     :boolean
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class CategorySubOption < ApplicationRecord
end
