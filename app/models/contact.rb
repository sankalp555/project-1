# == Schema Information
#
# Table name: contacts
#
#  id          :bigint(8)        not null, primary key
#  alternative :boolean
#  brand       :string
#  comment     :text
#  company     :string
#  email       :string
#  full_name   :string
#  original    :boolean
#  phone       :string
#  reason      :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Contact < ApplicationRecord
  has_many :reply_notifications, as: :replyable

  validates :company, :brand, presence: true
end
