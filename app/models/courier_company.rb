# == Schema Information
#
# Table name: courier_companies
#
#  id         :bigint(8)        not null, primary key
#  active     :boolean          default(FALSE)
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class CourierCompany < ApplicationRecord
  scope :active, -> { where active: true }
  validates :name, presence: true, :uniqueness => true 

  has_many :courier_jobs

end
