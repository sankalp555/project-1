# == Schema Information
#
# Table name: order_items
#
#  id                :bigint(8)        not null, primary key
#  delivery_type     :string
#  quantity          :integer
#  status            :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  order_id          :bigint(8)
#  quotation_part_id :bigint(8)
#  supplier_id       :integer
#
# Indexes
#
#  index_order_items_on_order_id           (order_id)
#  index_order_items_on_quotation_part_id  (quotation_part_id)
#

class OrderItem < ApplicationRecord
  belongs_to :order
  belongs_to :quotation_part
  belongs_to :supplier
  has_one :courier_job
  STATUS = ['pending','in_process', 'approved', 'confirmation_pending', 'delivered', 'cancelled']
  validates :status, inclusion: { in: STATUS, message: "Por favor proporcione un estado válido" }

  after_create :notify_supplier
  after_save :change_order_status, :send_confirmation , :if => Proc.new {|order_item| order_item.status_changed?}

  def change_order_status
    if self.order.order_items.map(&:status).all?{ |x| (x == "approved" || x == "cancelled") }
      self.order.update_attributes(status: "approved")
    end

  #   order_items = self.order.order_items
  #   status = order_items.pluck :status
    # unless status.include?([['pending','in_process']])
  #     self.order.update_attributes(status: "approved")
  #   end
  end

  def send_confirmation
    if self.status == 'confirmation_pending'
      UserMailer.send_delivery_confirmation(self.id, self.order.user.id).deliver_now
    end
  end

  def notify_supplier
    QuotationMailer.quote_accepted_mail_to_supplier(self.id).deliver_now
  end

end
