# == Schema Information
#
# Table name: attachments
#
#  id                 :bigint(8)        not null, primary key
#  attached_item_type :string
#  attachment         :string
#  description        :text
#  from_year          :integer
#  name               :string
#  to_year            :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  attached_item_id   :integer
#  brand_id           :integer
#  model_id           :integer
#  user_id            :bigint(8)
#
# Indexes
#
#  index_attachments_on_user_id  (user_id)
#

class Attachment < ApplicationRecord
  mount_uploader :attachment, AvatarUploader
  belongs_to :attached_item, polymorphic: true, optional: true
  validates :attachment, presence: true
  belongs_to :user, optional: true
  has_many :hot_spots
  validate :date_validation


  def date_validation
    if self.attached_item_type == "ItemType" && self.attached_item_type == "CategoryPartItem"
      if from_year.nil?
        errors.add(:from_year, "Date should be present.") if from_year.nil?
      elsif 
        to_year.nil?
        self.to_year = self.from_year if self.from_year.present?
        return true
      else
        if to_year < from_year
          errors.add(:to_year, "must be after the start date") 
          return false
        else
          return true
        end
      end
    end
  end


end
