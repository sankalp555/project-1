# == Schema Information
#
# Table name: brands_suppliers
#
#  brand_id         :bigint(8)        not null
#  category_part_id :bigint(8)
#  supplier_id      :bigint(8)        not null
#
# Indexes
#
#  index_brands_suppliers_on_category_part_id  (category_part_id)
#

class BrandsSupplier < ApplicationRecord
  validates :brand_id, uniqueness: { scope: [:supplier_id, :category_part_id], message: "Already assigned!" }
  belongs_to :supplier
  belongs_to :brand
  belongs_to :category_part
end
