# == Schema Information
#
# Table name: quotation_request_parts
#
#  id                   :bigint(8)        not null, primary key
#  alternative          :boolean
#  amount               :integer
#  description          :string
#  original             :boolean
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  api_sub_model_id     :integer
#  api_sub_part_id      :integer
#  api_vehicle_id       :integer
#  part_catalog_id      :integer
#  quotation_request_id :bigint(8)
#
# Indexes
#
#  index_quotation_request_parts_on_quotation_request_id  (quotation_request_id)
#
# Foreign Keys
#
#  fk_rails_...  (quotation_request_id => quotation_requests.id)
#

class QuotationRequestPart < ApplicationRecord
  paginates_per 10
  belongs_to :quotation_request
  belongs_to :api_sub_part
  has_many :questions
  belongs_to :part_catalog
  belongs_to :api_vehicle
  belongs_to :api_sub_model
  after_create :send_mail_to_supplier

  def send_mail_to_supplier
    QuotationMailer.send_mail_supplier_quotation_request(self).deliver_now 
  end

  def self.vehicle_model_name(id)
    part = QuotationRequestPart.find_by_quotation_request_id(id)
  	sub_model = part.api_sub_model
    vehicle = part.api_vehicle
    model = sub_model.api_model if sub_model.present?
  	if vehicle.present?
	  	name = model.name + '-' + sub_model.name rescue ''
	  	return name
	  else
	  	return ''
	  end
  end

end
