# == Schema Information
#
# Table name: spot_numbers
#
#  id          :bigint(8)        not null, primary key
#  height      :string
#  left        :string
#  name        :string
#  top         :string
#  width       :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  api_part_id :integer
#

class SpotNumber < ApplicationRecord
	belongs_to :api_part
end
