# == Schema Information
#
# Table name: category_parts
#
#  id         :bigint(8)        not null, primary key
#  active     :boolean          default(FALSE)
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class CategoryPart < ApplicationRecord

	has_many :category_part_items
	has_many :brands_suppliers
	has_many :item_types
  has_many :suppliers, through: :brands_suppliers
  has_many :attachments, as: :attached_item, dependent: :destroy
  accepts_nested_attributes_for :attachments, reject_if: :all_blank, allow_destroy: true
end
