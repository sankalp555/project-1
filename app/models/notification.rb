# == Schema Information
#
# Table name: notifications
#
#  id              :bigint(8)        not null, primary key
#  notifiable_type :string
#  read            :boolean          default(FALSE)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  notifiable_id   :bigint(8)
#
# Indexes
#
#  index_notifications_on_notifiable_type_and_notifiable_id  (notifiable_type,notifiable_id)
#

class Notification < ApplicationRecord
  belongs_to :notifyable, polymorphic: true
end
