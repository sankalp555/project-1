# == Schema Information
#
# Table name: part_catalogs
#
#  id                   :bigint(8)        not null, primary key
#  active               :boolean          default(TRUE)
#  group_name           :string
#  link                 :string
#  logo                 :string
#  name                 :string
#  next                 :string
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  api_catalog_id       :integer
#  api_vehicle_model_id :integer
#

class PartCatalog < ApplicationRecord
  belongs_to :api_catalog
  belongs_to :api_vehicle_model
  has_many :api_parts
  scope :active, -> { where active: true }
  has_many :api_catalogs_suppliers
  has_many :suppliers, through: :api_catalogs_suppliers
end
