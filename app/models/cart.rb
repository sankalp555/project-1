# == Schema Information
#
# Table name: carts
#
#  id             :bigint(8)        not null, primary key
#  coupon_applied :boolean          default(FALSE)
#  total_amount   :float
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  coupon_id      :integer
#

class Cart < ApplicationRecord
  has_one :user
  has_many :cart_items, dependent: :destroy
  belongs_to :coupon, optional: true
end
