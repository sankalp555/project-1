# == Schema Information
#
# Table name: markups
#
#  id          :bigint(8)        not null, primary key
#  active      :boolean
#  input       :decimal(, )
#  role        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  supplier_id :bigint(8)
#
# Indexes
#
#  index_markups_on_supplier_id  (supplier_id)
#

class Markup < ApplicationRecord
  Role = ['fix', 'percentage']
  validates_uniqueness_of :active,conditions: -> { where(active: true) }, scope: :supplier_id
  belongs_to :supplier

end
