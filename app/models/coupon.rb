# == Schema Information
#
# Table name: coupons
#
#  id            :bigint(8)        not null, primary key
#  active        :boolean          default(FALSE)
#  code          :string
#  discount      :decimal(, )      default(0.0)
#  discount_type :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Coupon < ApplicationRecord
  DiscountType = ['percent', 'amount']
  validates_uniqueness_of :code
end
