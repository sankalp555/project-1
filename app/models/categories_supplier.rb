# == Schema Information
#
# Table name: categories_suppliers
#
#  category_id :bigint(8)        not null
#  supplier_id :bigint(8)        not null
#
# Indexes
#
#  index_categories_suppliers_on_supplier_id_and_category_id  (supplier_id,category_id)
#

class CategoriesSupplier < ApplicationRecord
  validates :category_id, uniqueness: { scope: :supplier_id, message: "Already assigned!" }
  belongs_to :supplier
  belongs_to :category
end
