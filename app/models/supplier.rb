# == Schema Information
#
# Table name: suppliers
#
#  id              :bigint(8)        not null, primary key
#  active          :boolean          default(FALSE)
#  delivery_option :string
#  name            :string
#  phone           :string
#  url             :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class Supplier < ApplicationRecord
  has_many :suppliers_users, dependent: :destroy
  has_many :users, through: :suppliers_users
  has_one :address, as: :addressable, dependent: :destroy
  has_many :orders
  has_many :brands_suppliers
  has_many :categories_suppliers
  has_many :markups

  has_many :brands, through: :brands_suppliers


  has_many :category_parts, through: :brands_suppliers
  has_many :categories, through: :categories_suppliers
  
  has_many :api_catalogs_suppliers
  has_many :part_catalogs, through: :api_catalogs_suppliers
  
  has_many :quotations, dependent: :destroy
  has_and_belongs_to_many :brands
  scope :active, -> { where active: true  }
  accepts_nested_attributes_for :address
  DELIVERY_OPTION = [['Entrega', 'delivery'], ['Recoger','pickup'], ['Ambos', 'both']]
end
