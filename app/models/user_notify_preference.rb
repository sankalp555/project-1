# == Schema Information
#
# Table name: user_notify_preferences
#
#  id                 :bigint(8)        not null, primary key
#  dispatch_delivery  :boolean          default(FALSE)
#  new_conf_shipments :boolean          default(FALSE)
#  payment            :boolean          default(FALSE)
#  promo_discount     :boolean          default(FALSE)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  user_id            :bigint(8)
#
# Indexes
#
#  index_user_notify_preferences_on_user_id  (user_id)
#

class UserNotifyPreference < ApplicationRecord
  belongs_to :user
end
