ActiveAdmin.register ApiPart, { :sort_order => :name_asc } do
  permit_params :name, :group_name, :logo, :link, :next, :part_catalog_id, :internal_parts, :active
  menu parent: "API Data"
  belongs_to :part_catalog

  show do
    attributes_table do
      row :name
      row :group_name
      row :logo
      row :link
      row :active
      row :part_catalog_id do |item|
        item.part_catalog.name
      end
      row :updated_at
      row "Attachment" do |q|
        span do
          link_to "Make Hotspots", open_image_to_process_part_path(id: api_part.id)
        end
      end
    end
  end

  controller do

    def active_part
      part = ApiPart.find(params['part_id'])
      if part.active?
        part.update_attributes(active: false)
      else
        part.update_attributes(active: true)
      end
      redirect_to request.referer
    end
    

    def open_image_to_process_part
      @api_part = ApiPart.find params[:id]
      @item_attachment = @api_part.attachments.last
    end
    def create_hot_spot
      no_of_hot_spot = 1
      hot_spot_params[:hot_spots].each do |spot|
        check_position = HotSpot.where({x_position: (hot_spot_params[:hot_spots][spot]["x_position"].to_f - 0.00001)..(hot_spot_params[:hot_spots][spot]["x_position"].to_f + 0.00001), y_position: (hot_spot_params[:hot_spots][spot]["y_position"].to_f - 0.00001)..(hot_spot_params[:hot_spots][spot]["y_position"].to_f + 0.00001), attachment_id: hot_spot_params["attachment_id"].to_i})
        if check_position.blank?
          category_part_item = CategoryPartItem.create({name: hot_spot_params[:hot_spots][spot]["category_part_item_name"], item_type_id: hot_spot_params["item_type_id"].to_i, active: hot_spot_params["available"].eql?('true')})
          if !hot_spot_params[:hot_spots][spot]["category_sub_part_item_ids"].blank? 
            hot_spot_params[:hot_spots][spot]["category_sub_part_item_ids"].each do |sub_option_id|
              category_part_item.category_sub_part_items.create(category_sub_option_id: sub_option_id) if !sub_option_id.blank?
            end
          end
          if !hot_spot_params[:hot_spots][spot]["category_part_item_attachment"].blank? && hot_spot_params[:hot_spots][spot]["category_part_item_attachment"] != "undefined"
            extension = 'png'
            tempfile = File.join(Rails.root, 'tmp', "#{ SecureRandom.uuid}.#{extension}")
            File.open(tempfile, "wb") do |file|
              file.write(Base64.decode64(hot_spot_params[:hot_spots][spot]["category_part_item_attachment"]))
            end
            category_part_item.attachments.create!(attachment: File.open(tempfile))
            File.delete(tempfile) if File.exists? tempfile
          end
          hot_spot = HotSpot.create({x_position: hot_spot_params[:hot_spots][spot]["x_position"], y_position: hot_spot_params[:hot_spots][spot]["y_position"], note: hot_spot_params[:hot_spots][spot]["note"], category_part_item_id: category_part_item.id, attachment_id: hot_spot_params["attachment_id"].to_i, item_type_id: hot_spot_params["item_type_id"].to_i, hot_spot_no: no_of_hot_spot})
        end
        no_of_hot_spot +=1
      end
      render json: { status: "ok", message: "Successfully created all hot spot!" }
    end
    def delete_hot_spot
      hot_spot = HotSpot.find params[:id]
      category_part_item = CategoryPartItem.find hot_spot.category_part_item_id
      category_part_item.destroy
      if hot_spot.destroy
        render json: { status: "ok", message: "Successfully deleted hot spot!" }
      else
        render json: { status: "error", message: "Not Hot Spot found!" }
      end
    end
    private
    def hot_spot_params
      params.permit(:attachment_id, :item_type_id, hot_spots: [:x_position, :y_position, :api_sub_part_id, :api_part_id])
    end
  end

end
