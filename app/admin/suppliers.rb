ActiveAdmin.register Supplier do
  menu label: "Empresas", parent: "Proveedores"
  permit_params :name, :phone, :url, :active, :delivery_option, address_attributes: [:id, :name, :street_address, :number, :region, :city, :comuna, :phone, :primary]

  filter :name
  filter :created_at

  index do 
    selectable_column
    id_column
    column :name
    column :url
    column :delivery_option
    column :active
    column :created_at
    actions
  end

  show do 
    attributes_table do
      row :name
      row :phone
      row :url
      row :delivery_option
      row :updated_at
    end

    panel "Usuarios" do
      table_for supplier.users do
        column :id
        column :name
        column :email
        column :phone
        column :role
      end
    end

    panel "Dirección" do
      table_for supplier.address do
        column :id
        column :name
        column :phone
        column :city
        column :region
        column :comuna
        column :number
      end
    end

    panel "Categories" do
      table_for(supplier.categories) do |category|
        column :id do |categories|
          link_to "#{categories.id}", admin_category_path(categories.id)
        end
        column :name
        column "Action" do |categories|
          link_to "retirar", remove_supplier_category_path(supplier.id, category_id: categories.id), :method => :delete, :data => {:confirm => "¿Está seguro de que quiere eliminar esto?"}
        end
      end
    end
  end

  form do |f|  
    f.inputs do
      f.input :name
      f.input :phone
      f.input :url
      f.input :delivery_option, as: :select, :collection => Supplier::DELIVERY_OPTION
      f.input :active
    end
    
    a = CS.states(:cl)
    
    f.inputs "Dirección", for: [:address, supplier.address || supplier.build_address] do |address|
      address.input :name
      address.input :street_address
      address.input :number
      address.input :region, :as => :select, :collection => options_for_select(a.collect{|p| [p[1], p[1]]}, supplier.address.region), :input_html => { :id => "region" }
      if supplier.address.present?
        region = CS.states(:cl)
        code = region.invert[supplier.address.region]
        cities = CS.cities(code, :cl)
        address.input :city, :as => :select, :collection => options_for_select(cities, supplier.address.city), :input_html => { :id => "city" }
      else
        address.input :city, :as => :select, :collection => options_for_select({}, ''), :input_html => { :id => "city" }
      end    
      address.input :comuna
      address.input :phone
      address.input :primary
    end
    f.actions
  end

  action_item :view, only: :show do
    link_to 'Añadir Categories', new_supplier_category_path(supplier_id: resource.id)
  end


  # controller do 
  #   def create
  #     s = Supplier.new(supplier_params)
  #     if s.save
  #       flash[:success] = "Successfully added"
  #       redirect_to admin_supplier_path(s.id)
  #     else
  #       flash[:error] = s.errors.full_messages.join(", ")
  #       redirect_to new_admin_supplier_path
  #     end
  #   end

  #   private
  #   def supplier_params
  #     params.require(:supplier).permit(:name, :phone, :url, :active, :delivery_option, address_attributes: [:name, :street_address, :number, :region, :city, :comuna, :phone, :primary])
  #   end
  # end
end
