ActiveAdmin.register Employee do 
  menu parent: "Usuarios", label: "Employees", priority: 100
  permit_params :email, :privilege, :password, :password_confirmation

  index do 
    selectable_column
    id_column
    column :email
    column :privilege
    actions
  end

  filter :email
  filter :privilege

  form do |f|
    f.inputs do
      f.input :email
      f.input :password
      f.input :password_confirmation
      f.input :privilege, as: :boolean
    end
    f.actions
  end

end
