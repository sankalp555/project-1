ActiveAdmin.register Category do 
  menu parent: "Catalogar"
  # permit_params :name, :active
  permit_params :name, :active

  filter :name
  filter :created_at


  controller do
    def add_supplier_category
      @category_supplier = CategoriesSupplier.new(supplier_category_params)
      if @category_supplier.save
        flash[:success] = "Succesfully added!"
        redirect_to admin_supplier_path(params[:supplier_id])
      else
        flash[:error] = @category_supplier.errors.full_messages.join(', ')
        redirect_to admin_supplier_path(params[:supplier_id])
      end
    end

    def new_supplier_category
    end

    def remove_supplier_category
      @category_supplier = CategoriesSupplier.where(category_id: params["category_id"], supplier_id: params["supplier_id"])
      if @category_supplier.present?
        @category_supplier.delete_all
        flash[:success] = "Succesfully deleted brand for supplier!"
        redirect_to request.referer
      else
        flash[:error] = "Brand cant be removed!"
        redirect_to request.referer
      end
    end

    private

    def supplier_category_params
      params.permit(:category_id,:supplier_id)
    end
  end


end
