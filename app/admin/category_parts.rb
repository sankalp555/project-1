ActiveAdmin.register CategoryPart do
  menu parent: "Catalogar"
  permit_params :name, :active, attachments_attributes: [:id, :name, :attachment, :_destroy]

  filter :name
  filter :active

  form do |f|
    f.inputs do
      f.input :name
      f.input :active
      f.has_many :attachments do |attachment|
        attachment.input :attachment, :type => :file
        attachment.input :_destroy, :as => :boolean, :required => false, :label => 'Remove' 
      end
    end


    f.actions
  end

  show do
    attributes_table do
      row :name
      row :active
      row "Attachment" do |q|
        if q.attachments.present?
          q.attachments.each do |attachment|
            span do
              image_tag attachment.attachment, :style => "width:350px; height: 220px;"
            end
          end
          raw('&nbsp;')
        end
      end
    end

    panel "Category Part Item Types" do
      table_for(category_part.item_types) do |item_type|
        column :id do |item_types|
          link_to "#{item_types.id}", admin_item_type_path(item_types.id)
        end
        column :name
      end
    end
  end
end