ActiveAdmin.register ApiVehicle do
  permit_params :api_sub_model_id, :ssd, :vin, :frame, :api_catalog_id
  menu parent: "API Data"

  show do
    attributes_table do
    	row :api_catalog_id do |item|
        item.api_catalog.name
      end
      row :api_sub_model_id do |item|
        item.api_sub_model.name
      end
      row :ssd
      row :vin
      row :frame
      row :plate_no
    end
    panel "Vehicle Models" do
      table_for(api_vehicle.api_vehicle_models) do |vehicle_modl|
        column :engine
        column :market
        column :release_date
        column "Action", -> (api_vehicle_model) {link_to "Edit", admin_api_vehicle_api_vehicle_model_path(api_vehicle_id: api_vehicle.id, id: api_vehicle_model.id), method: "get"}
      end
    end
  end

end
