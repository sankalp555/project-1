ActiveAdmin.register ReplyNotification do
  config.clear_action_items!
  permit_params  :reply
  index do 
    selectable_column
    id_column
    column :replyable_type
    column :reply_to, -> (reply_notification) {Contact.find(reply_notification.replyable_id).email}
    column :reply
    actions
  end

  show do |f|
    panel "Reply" do
      attributes_table_for f do
        row :replyable_id
        row :replyable_type
        row :reply
      end
    end
    panel "Contact's Enqurey" do
      reply = ReplyNotification.find(params[:id])
      attributes_table_for Contact.find(reply.replyable_id) do
        row :full_name
        row :email
        row :brand
        row :comment
        row :company
        row :phone
        row :original, -> (contact) {contact.original==true ? "Original" : "Do not"}
        row :alternative, -> (contact) {contact.alternative==true ? "Alternative" : "Do not"}
      end
    end
  end

  form do |f|
    panel "Contact's Enqurey" do
      attributes_table_for Contact.find(params[:notification_id]) do
        row :full_name
        row :email
        row :brand
        row :comment
        row :company
        row :phone
        row :original, -> (contact) {contact.original==true ? "Original" : "Do not"}
        row :alternative, -> (contact) {contact.alternative==true ? "Alternative" : "Do not"}
      end
    end    


    f.inputs do
      f.input :reply
      f.input :replyable_id, :input_html => { :value => params[:notification_id] }, as: :hidden
      f.input :replyable_type, :input_html => { :value => "contactReply" }, as: :hidden
    end
    f.actions

  end

  controller do 
    def create
      @reply_id = ReplyNotification.new(replyable_id: reply_params[:reply_notification][:replyable_id], reply: reply_params[:reply_notification][:reply])
      @reply = @reply_id.update_attribute(:replyable_type, reply_params[:reply_notification][:replyable_type])
      contact = Contact.find(reply_params[:reply_notification][:replyable_id])
      @reply_id.send_mail_to_contact_user(contact.email, reply_params[:reply_notification][:reply])
      if @reply
        flash[:success] = "Message send to contact email id!"
        redirect_to admin_contacts_path
      else
        flash[:error] = "Message failed to send contact email id!"
        redirect_to new_admin_reply_notification_path(notification_id: params[:replyable_id])
      end
    end

    private

    def reply_params
      params.permit(reply_notification: [:reply, :replyable_id, :replyable_type])
    end

  end

end
