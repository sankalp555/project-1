  ActiveAdmin.register SuppliersUser do
    menu parent: "Proveedores", label: "Permisos"
    permit_params :user_id, :supplier_id, :role, user_attributes: [:id, :name, :last_name, :email, :phone, :password, :password_confirmation]

    filter :supplier

    index do
      selectable_column
      id_column
      column :supplier
      column :user, -> (role) { role.user&.email }
      column :role
      column :created_at
      actions
    end

    form do |f|
      f.inputs do
        f.input :supplier, as: :select, collection: Supplier.pluck(:name, :id)
        f.input :role, as: :select, collection: [['Vendedor', 'Vendedor']], include_blank: false
      end
      f.inputs "User", for: [:user, suppliers_user.user || User.new] do |c|
        c.input :name
        c.input :last_name
        c.input :email
        c.input :phone
        if c.object.new_record?
          c.input :password
          c.input :password_confirmation
        end
      end
      f.actions
    end

  show do |f|
    panel "Provider" do
      attributes_table_for f, :supplier, :user, :role
    end

    panel "Detalles de User" do
      attributes_table_for f.user do
        row :email
        row :encrypted_password
        row :reset_password_token
        row :reset_password_sent_at
        row :remember_created_at
        row :current_sign_in_at
        row :last_sign_in_at
        row :current_sign_in_up
        row :last_sign_in_ip
        row :created_at
        row :updated_at
        row :name
        row :phone
        row :deleted_at
      end
    end

    panel "Brands"  do
      span do
        link_to "Destroy All", remove_supplier_brand_path(resource.supplier_id), :method => :delete, :data => {:confirm => "¿Está seguro de que quiere eliminar esto?"}
      end  

      table_for(f.supplier.part_catalogs) do |brand|
        column :id do |brands|
          brands.id
        end
        column :catalog do |brands|
          brands.api_catalog.name
        end
        column :name
        column "Action" do |brands|
          link_to "retirar", remove_supplier_brand_path(resource.supplier_id, catalog_id: brands.id), :method => :delete, :data => {:confirm => "¿Está seguro de que quiere eliminar esto?"}
        end
      end
    end
  end

  action_item :view, only: :show do
    link_to 'Añadir marca', new_supplier_brand_path(supplier_id: resource.supplier_id, supplier_user_id: resource.id)
  end

  controller do 
    def create
      super
      resource.user.generate_reset_password_token_and_notify if resource.persisted?
    end
  end
end
