ActiveAdmin.register ApiCatalog, { :sort_order => :name_asc } do
  permit_params :name, :active, :code, :logo, :small_logo, :vehicle_type 
  menu parent: "API Data"

  show do
    attributes_table do
      row :name
      row :code
      row :vehicle_type
      row :active
    end
    panel "Parts Catalog" do
      table_for(api_catalog.part_catalogs) do |part_catalog|
        column :id
        column :name
        column :group_name
        column :active
        column "Action", -> (part_catalog) {link_to "Edit", admin_api_catalog_part_catalog_path(api_catalog_id: api_catalog.id, id: part_catalog.id), method: "get"}
        column "Action", -> (part_catalog) {link_to (part_catalog.active == true ? "Active" : "In-Active"), active_path(part_catalog_id: part_catalog.id)}
      end
    end
  end

  index do
    selectable_column
    id_column
    column :name
    column :active
    column :vehicle_type
    column :code
    column :created_at
    column :updated_at
    actions do |api_catalog|
      link_to admin_api_catalog_api_models_path(api_catalog.id), title: "Models" do
        raw ('<i class="fa fa-cogs" style="font-size:13px;"></i>')
      end
    end
  end
end
