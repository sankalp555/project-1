ActiveAdmin.register Notice do
  menu parent: "Internals"
  permit_params :title, :discription, :publish, :notice_type, :page

  form do |f|
    f.inputs "Añadir Notice" do
      f.input :title
      f.input :discription
      f.input :notice_type, as: :select, :collection => Notice::TYPE
      f.input :page, as: :select, :collection => Notice::Page
      f.input :publish
    end
    
    f.actions
  end 
end
