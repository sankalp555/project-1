ActiveAdmin.register Tax do 
  menu parent: "Configuración"
  permit_params :name, :percent, :year, :active, :lower_amount, :upper_amount
end
