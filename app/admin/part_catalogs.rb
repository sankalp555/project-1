ActiveAdmin.register PartCatalog, { :sort_order => :name_asc } do
  permit_params :name, :group_name, :api_catalog_id, :logo, :api_vehicle_model_id, :active
  menu parent: "API Data"
  belongs_to :api_catalog


  show do
    attributes_table do
      row :name
      row :group_name
      row :active
    end
    panel "Parts" do
      table_for(part_catalog.api_parts) do |part|
        column :id
        column :name
        column :group_name
        column :active
        column "Action", -> (part) {link_to "Edit", admin_part_catalog_api_part_path(part_catalog_id: part_catalog.id, id: part.id), method: "get"}
        column "Action", -> (part) {link_to (part.active == true ? "Active" : "In-Active"), active_part_path(part_id: part.id)}
      end
    end
  end


  controller do
  
    def active
      part_catalog = PartCatalog.find(params['part_catalog_id'])
      if part_catalog.active?
        part_catalog.update_attributes(active: false)
      else
        part_catalog.update_attributes(active: true)
      end
      redirect_to request.referer
    end
  
  end

end
