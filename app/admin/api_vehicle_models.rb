ActiveAdmin.register ApiVehicleModel do
  permit_params :engine, :market, :release_date
  menu parent: "API Data"
  belongs_to :api_vehicle

  form do |f|
    f.inputs do
      f.input :engine
      f.input :market
    end
    f.actions
  end

end
