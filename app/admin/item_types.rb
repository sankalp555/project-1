ActiveAdmin.register ItemType do 
  menu parent: "Catalogar"
  permit_params :name, :active, :hot_spot_no, :category_part_id, attachments_attributes: [:id, :name, :attachment, :from_year, :to_year, :model_id, :brand_id, :_destroy]


  form do |f|
    f.inputs do
      f.input :name
      f.input :category_part_id, :as => :select, :collection => CategoryPart.pluck(:name, :id)
      f.input :active
      f.input :hot_spot_no
      f.has_many :attachments do |attachment|
        attachment.input :attachment, :type => :file
        attachment.input :from_year, :as => :select, :collection => QuotationRequest::YEAR
        attachment.input :to_year, :as => :select, :collection => QuotationRequest::YEAR
        attachment.input :brand_id, :as => :select, :collection => Brand.pluck(:name, :id), :input_html => { :id => "brand" }
        if f.object.new_record?
          attachment.input :model_id, :as => :select, :collection => "", :required => true, :input_html => { :id => "model" }
        else
          if f.object.attachments.present?
            brand = Brand.find_by_id(f.object.attachments.last.brand_id)
            if brand.present?
              attachment.input :model_id, :as => :select, :collection => brand.models.pluck(:name, :id), :required => true,  :input_html => { :id => "model" }
            else
              attachment.input :model_id, :as => :select, :collection => "", :required => true, :input_html => { :id => "model" }
            end
          else
            attachment.input :model_id, :as => :select, :collection => "", :required => true, :input_html => { :id => "model" }
          end
        end
        attachment.input :_destroy, :as => :boolean, :required => false, :label => 'Remove' 
      end
    end

    f.actions
  end

  index do
    selectable_column
    id_column
    column :name
    column :active
    column :created_at
    column :updated_at
    column :category_part_id do |item|
      item.category_part.name
    end
    actions do |item_type|
      link_to admin_item_type_category_part_items_path(item_type.id), title: "Parts" do
        raw ('<i class="fa fa-cogs" style="font-size:13px;"></i>')
      end
    end
  end

  show do
    attributes_table do
      row :name
      row :active
      row :created_at
      row :updated_at
      row :category_part_id do |item|
        item.category_part.name
      end
      row "Attachment" do |q|
        if q.attachments.present?
          q.attachments.each do |attachment|
            span do
              image_tag attachment.attachment, :style => "width:350px; height: 220px;"
            end
            span do
              link_to "Make Hotspots", open_image_to_process_path(id: item_type.id)
            end
          end
          raw('&nbsp;')
        end
      end
    end

    panel "Category Part Items" do
      table_for(item_type.category_part_items) do |part_item|
        column :id
        column :name
        column "Sub-Parts", -> (category_part_item) {category_part_item.sub_parts==true ? category_part_item.category_sub_part_item.sub_parts : "No Sub Parts"}
        column "Action", -> (category_part_item) {link_to "Delete", admin_item_type_category_part_item_path(item_type_id: item_type.id,id: category_part_item.id), method: "delete", :data => {:confirm => '¿Estás seguro?', :title => '¿Estás seguro?', :commit=> "Confirmar", :cancel=> "Cancelar"}}
      end
    end
  end

  controller do
    def open_image_to_process
      @item_type = ItemType.find params[:id]
      @item_attachment = @item_type.attachments.last
    end
    def create_hot_spot
      no_of_hot_spot = 1
      hot_spot_params[:hot_spots].each do |spot|
        check_position = HotSpot.where({x_position: (hot_spot_params[:hot_spots][spot]["x_position"].to_f - 0.00001)..(hot_spot_params[:hot_spots][spot]["x_position"].to_f + 0.00001), y_position: (hot_spot_params[:hot_spots][spot]["y_position"].to_f - 0.00001)..(hot_spot_params[:hot_spots][spot]["y_position"].to_f + 0.00001), attachment_id: hot_spot_params["attachment_id"].to_i})
        if check_position.blank?
          category_part_item = CategoryPartItem.create({name: hot_spot_params[:hot_spots][spot]["category_part_item_name"], item_type_id: hot_spot_params["item_type_id"].to_i, active: hot_spot_params["available"].eql?('true')})
          if !hot_spot_params[:hot_spots][spot]["category_sub_part_item_ids"].blank? 
            hot_spot_params[:hot_spots][spot]["category_sub_part_item_ids"].each do |sub_option_id|
              category_part_item.category_sub_part_items.create(category_sub_option_id: sub_option_id) if !sub_option_id.blank?
            end
          end
          if !hot_spot_params[:hot_spots][spot]["category_part_item_attachment"].blank? && hot_spot_params[:hot_spots][spot]["category_part_item_attachment"] != "undefined"
            extension = 'png'
            tempfile = File.join(Rails.root, 'tmp', "#{ SecureRandom.uuid}.#{extension}")
            File.open(tempfile, "wb") do |file|
              file.write(Base64.decode64(hot_spot_params[:hot_spots][spot]["category_part_item_attachment"]))
            end
            category_part_item.attachments.create!(attachment: File.open(tempfile))
            File.delete(tempfile) if File.exists? tempfile
          end
          hot_spot = HotSpot.create({x_position: hot_spot_params[:hot_spots][spot]["x_position"], y_position: hot_spot_params[:hot_spots][spot]["y_position"], note: hot_spot_params[:hot_spots][spot]["note"], category_part_item_id: category_part_item.id, attachment_id: hot_spot_params["attachment_id"].to_i, item_type_id: hot_spot_params["item_type_id"].to_i, hot_spot_no: no_of_hot_spot})
        end
        no_of_hot_spot +=1
      end
      render json: { status: "ok", message: "Successfully created all hot spot!" }
    end
    def delete_hot_spot
      hot_spot = HotSpot.find params[:id]
      category_part_item = CategoryPartItem.find hot_spot.category_part_item_id
      category_part_item.destroy
      if hot_spot.destroy
        render json: { status: "ok", message: "Successfully deleted hot spot!" }
      else
        render json: { status: "error", message: "Not Hot Spot found!" }
      end
    end
    private
    def hot_spot_params
      params.permit(:attachment_id, :item_type_id, hot_spots: [:x_position, :y_position, :note, :category_part_item_name, :category_part_item_attachment, :available, :category_sub_part_item_ids => [] ])
    end
  end
end
