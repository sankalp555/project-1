ActiveAdmin.register TermsAndCondition do 
  menu parent: "Internals"
  permit_params :title, :content

  form do |f|
    f.inputs do
      f.input :title
      f.input :content, as: :ckeditor
    end
    f.actions
  end
  
end
