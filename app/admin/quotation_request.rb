ActiveAdmin.register QuotationRequest do
  menu parent: "Cotizaciones", label: "Solicitud"

  filter :name
  filter :brand
  filter :created_at

  index do
    selectable_column
    id_column
    column :user
    column :amount, -> (quotation_request) { quotation_request.quotation_request_parts.count }
    column :created_at
    actions
  end

  show do 
    attributes_table do
      row :user
      row :created_at
      row :updated_at
    end

    panel "Piezas" do
      table_for quotation_request.quotation_request_parts do
        column :make, -> (part){ part.model.brand }
        column :model
        column :year
        column :chasis_number
        column :description
        column :amount
        column :original
        column :alternative
      end
    end
  end
end
