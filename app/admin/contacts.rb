ActiveAdmin.register Contact do
  menu parent: "Internals"
  permit_params :full_name, :email, :brand, :comment, :phone, :original, :alternative, :company

  index do
    selectable_column
    id_column
    column :full_name
    column :email
    column :brand
    column :comment
    column :company
    column :phone
    column :original, -> (contact) {contact.original==true ? "Original" : "Do not"}
    column :alternative, -> (contact) {contact.alternative==true ? "Alternative" : "Do not"}
    column :reply do |contact|
      link_to "Reply", new_admin_reply_notification_path(notification_id: contact.id)
    end
    column :created_at
  end

  show do |f|
    panel "Contacts" do
      attributes_table_for f, :contact
    end
  end
end