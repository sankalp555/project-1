ActiveAdmin.register Coupon do 
  menu parent: "Configuración"
  permit_params :code, :discount, :active, :discount_type

  form do |f|
    f.inputs do
      f.input :code
      f.input :discount
      f.input :discount_type, as: :select, :collection => Coupon::DiscountType
      f.input :active
    end
    f.actions
  end

  action_item :view, only: :show do
    link_to 'Add User', new_user_coupon_path(resource.id)
  end

  controller do
    def add_user
      @user_coupon = UserCoupon.new
      @user = User.find_by_id(params[:user_id])
      @user_coupon.user_id = params[:user_id]
      @user_coupon.coupon_id = params[:coupon_id]
      @coupon = Coupon.find_by_id(params[:coupon_id])
      if @user_coupon.save
        UserMailer.coupon_assigned(@user, @coupon).deliver_now
        flash[:success] = "Succesfully addigned Coupon!"
        redirect_to new_user_coupon_path(params[:coupon_id])
      else
        flash[:error] = @user_coupon.errors.full_messages.join(', ')
        redirect_to new_user_coupon_path(params[:coupon_id])
      end
    end

    def assign_all
      ids = params['user_ids'] rescue []
      if ids.present?
        ids.each do |id|
          user = User.find_by_id(id)
          @user_coupon = UserCoupon.new
          @user_coupon.user_id = id
          @coupon = Coupon.find_by_id(params[:coupon_id])
          @user_coupon.coupon_id = params[:coupon_id]
          @user_coupon.save
          UserMailer.coupon_assigned(user, @coupon).deliver_now
        end
        redirect_to new_user_coupon_path(params[:coupon_id])
      end
    end

    def new_user_coupon
      supplier_users_ids = SuppliersUser.pluck(:user_id)
      @users = User.where.not(id: supplier_users_ids)
    end

    def remove_coupon_user
      @user_coupon = UserCoupon.where(user_id: params["user_id"], coupon_id: params["coupon_id"])
      if @user_coupon.present?
        @user_coupon.delete_all
        flash[:success] = "Succesfully Removed Coupon"
        redirect_to request.referer
      else
        flash[:error] = "Brand cant be removed!"
        redirect_to request.referer
      end
    end
  end
end
