ActiveAdmin.register Brand, { :sort_order => :name_asc } do
  menu parent: "Catalogar"
  permit_params :name, :brand_id, :supplier_id, :category_id, attachment_attributes: [:id, :name, :attachment, :_destroy]

  filter :name
  filter :created_at

  # collection_action :create, method: :post do
  #   brand = Brand.new (permitted_params['brand'])
  #   # brand.category_id = params['brand']['category_id']
  #   # brand.name = params['brand']['name']
  #   if brand.save && params['brand']['attachment'].present?
  #     attachment =  brand.attachments.new(params['brand']['attachment'])
  #     attachment.save
  #   end
  #   redirect_to admin_brands_path(brand.id)
  # end

  # member_action :update, method: :put do
  #   brand = Brand.find params[:id]
  #   brand.update_attributes(supplier_brand_params)
  #   if brand.save && params_attributes.present?
  #     attachment = brand.update_attributes(params_attributes)
  #   end
  #   redirect_to admin_brands_path(brand.id)
  # end



  index do
    selectable_column
    id_column
    column :name
    column :models, ->(brand) { brand.models.count }
    column :created_at
    actions do |brand|
      link_to "Models", admin_brand_models_path(brand.id)
    end
  end

  form do |f|
    f.inputs do
      f.input :category_id, :as => :select, :collection => Category.pluck(:name, :id)
      f.input :name
    end
    
    f.inputs "Attachment", for: [:attachment, brand.attachment || brand.build_attachment] do |attachment|
      attachment.input :name
      attachment.input :attachment, :type => :file
    end

    f.actions
  end

  show do |f|
    attributes_table do
      row :id
      row :name
      row :created_at
      row :updated_at
      row "Attachment" do |q|
        if q.attachment.present?
          span do
            image_tag(q.attachment.attachment.url)
          end
        end
      end
    end

  end
  controller do

    def create
      brand = Brand.new (permitted_params['brand'])
      if brand.save 
        redirect_to admin_brand_path(brand.id)
      end
    end

    def new_supplier_brand
      @supplier = Supplier.find (params['supplier_id'])
      @catalogs = ApiCatalog.all
    end

    def add_supplier_brand
      @api_catalog_supplier = ApiCatalogsSupplier.new(supplier_brand_params)
      @api_catalog_supplier.part_catalog_id = params['part_catalog_id']
      if @api_catalog_supplier.save
        flash[:success] = "Succesfully added!"
        redirect_to admin_suppliers_user_path(params[:supplier_user_id])
      else
        flash[:error] = @api_catalog_supplier.errors.full_messages.join(', ')
        redirect_to admin_suppliers_user_path(params[:supplier_user_id])
      end
    end

    def remove_supplier_brand
      @api_catalogs_suppliers = ApiCatalogsSupplier.where(part_catalog_id: params["catalog_id"], supplier_id: params["supplier_id"] )
      if @api_catalogs_suppliers.present?
        @api_catalogs_suppliers.delete_all
        flash[:success] = "¡Marca eliminada con éxito para el proveedor!"
        redirect_to request.referer
      else
        flash[:error] = "¡La marca no se puede eliminar!"
        redirect_to request.referer
      end
    end

    def remove_supplier_category_part
      @brand_supplier = BrandsSupplier.where(category_part_id: params["category_part_id"], supplier_id: params["supplier_id"])
      if @brand_supplier.present?
        @brand_supplier.delete_all
        flash[:success] = "¡Marca eliminada con éxito para el proveedor!"
        redirect_to request.referer
      else
        flash[:error] = "¡La marca no se puede eliminar!"
        redirect_to request.referer
      end
    end
    

    private

    def supplier_brand_params
      params.permit(:part_catalog_id, :supplier_id)
    end
    # def params_attributes
    #   params.require(:brand).permit(attachments_attributes: [:id, :name, :attachment, :_destroy])
    # end
  end
end
