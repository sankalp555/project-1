ActiveAdmin.register ReferralInstruction do
  menu parent: "Usuarios", label: "Referral Instructions", priority: 100
  permit_params :referral_max_amount, :amount_to_be_awarded_per_refer, :refer_description, :expire_in_days, :max_no_of_refer, :active, :referral_type
end
