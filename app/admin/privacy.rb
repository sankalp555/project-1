ActiveAdmin.register Privacy do 
  menu parent: "Internals"
  permit_params :name, :content

  form do |f|
    f.inputs do
      f.input :title
      f.input :content, as: :ckeditor
    end
    f.actions
  end
end
