ActiveAdmin.register User do 
  menu parent: "Usuarios", label: "Regular", priority: 100
  permit_params :name, :email, :phone, :password, :password_confirmation


  index do 
    selectable_column
    id_column
    column :name
    column :email
    column :phone
    actions
  end

  filter :name
  filter :email
  filter :phone

  form do |f|
    f.inputs do
      f.input :name
      f.input :email
      f.input :phone
      if f.object.new_record?
        f.input :password
        f.input :password_confirmation
      end
    end
    f.actions
  end

  controller do 
    def create
      super
      resource.generate_reset_password_token_and_notify if resource.persisted?
   end
  end
  
end
