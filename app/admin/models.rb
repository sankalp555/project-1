ActiveAdmin.register Model, { :sort_order => :name_asc } do
  belongs_to :brand
  permit_params :name, :brand_id

  filter :name
  filter :brand
  filter :created_at

  index do
    selectable_column
    id_column
    column :name
    column :brand
    column :created_at
    actions
  end

  form do |f|
    f.inputs do
      f.input :name
    end

    f.actions
  end
end
