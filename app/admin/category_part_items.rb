ActiveAdmin.register CategoryPartItem do
  menu parent: "Configuración"
  belongs_to :item_type
  #permit_params :name, :active, :item_type_id, :category_sub_part_item_ids, attachment_attributes: [:id, :name, :attachment, :description, :preference_number ,:_destroy]

  form do |f|
    f.inputs do
      f.input :name
      unless params['item_type_id'].present?
        f.input :item_type_id, :as => :select, :collection => ItemType.pluck(:name, :id)
      end
      f.inputs "Sub-Parts-Options" do
        f.input :category_sub_part_items, as: :select2_multiple, collection: CategorySubOption.all.collect {|category_sub_option| [category_sub_option.name, category_sub_option.id] }, selected: category_part_item.category_sub_part_items.pluck(:category_sub_option_id),class: 'select2-hidden-accessible'
      end
      f.has_many :attachments do |attachment|
        attachment.input :name
        attachment.input :from_year, :as => :select, :collection => QuotationRequest::YEAR
        attachment.input :to_year, :as => :select, :collection => QuotationRequest::YEAR
        attachment.input :brand_id, :as => :select, :collection => Brand.pluck(:name, :id), :input_html => { :id => "brand" }
        if f.object.new_record?
          attachment.input :model_id, :as => :select, :collection => "", :input_html => { :id => "model" }
        else
          if f.object.attachments.present? && !f.object.attachments.last.brand_id.nil?
            brand = Brand.find_by_id(f.object.attachments.last.brand_id)
            attachment.input :model_id, :as => :select, :collection => brand.models.pluck(:name, :id), :input_html => { :id => "model" }
          else
            attachment.input :model_id, :as => :select, :collection => "", :input_html => { :id => "model" }
          end
        end
        attachment.input :attachment, :type => :file
      end
      f.input :active
    end
    f.actions
  end

  show do
    attributes_table do
      row :name
      row :active
      row :created_at
      row :updated_at
      row :item_type_id do |part|
        part.item_type.name
      end
      if !category_part_item.category_sub_part_items.blank?
        row "Sub-Parts" do |rmv_sub|
          if rmv_sub.category_sub_part_items.present?
            rmv_sub.category_sub_part_items.each do |sub_item|
              span do
                if sub_item.category_sub_option_id.present?
                  CategorySubOption.find(sub_item.category_sub_option_id).name
                end
              end
              span do
                link_to remove_category_sub_part_path(category_sub_part_id: sub_item.id), method: :delete, data: {confirm: "Are you sure?"} do
                  raw('<i class="fa fa-trash" style="color: red;"></i>&nbsp;|') 
                end
              end
            end
            raw('&nbsp;')
          end
        end
      end
      row "Attachments" do |att|
        if att.attachments.present?
          att.attachments.each do |attachment|
            span do
              image_tag attachment.attachment, :style => "width:350px; height: 220px;" 
            end
          end
          raw('&nbsp;')
        end
      end
    end
  end

  controller do
    def create
      item_type = ItemType.find params[:item_type_id]
      category_part_item = item_type.category_part_items.create(category_part_item_params)
      if category_part_item
        params[:category_part_item][:category_sub_part_item_ids].each do |id|
          category_part_item.category_sub_part_items.create(category_sub_option_id: id) if !id.blank?
        end
      end
      flash[:success] = "Successfully added category part!"
      redirect_to admin_item_type_category_part_item_path(id: category_part_item.id)
    end
    def update
      category_part_item = CategoryPartItem.find params[:id]
      category_part_item.update(category_part_item_params)
      # parts = CategorySubPartItem.where(category_sub_option_id: params[:category_part_item][:category_sub_part_item_ids])
      # unless parts.present?
        if !params[:category_part_item][:category_sub_part_item_ids].blank?
          parts = params[:category_part_item][:category_sub_part_item_ids].reject { |c| c.empty? }
          parts.each do |id|
            opt_present = CategorySubPartItem.where(category_sub_option_id: id, category_part_item_id: category_part_item.id)
            if opt_present.blank?
              category_part_item.category_sub_part_items.create(category_sub_option_id: id) if !id.blank?
            end
          end
        end
      # end
      flash[:success] = "Successfully updated category part!"
      redirect_to admin_item_type_category_part_item_path(id: category_part_item.id)
    end

    def destroy_category_sub_part
      sub_part_item = CategorySubPartItem.find params[:category_sub_part_id]
      category_part_item = CategoryPartItem.find sub_part_item.category_part_item_id
      if sub_part_item.destroy
        flash[:success] = "Succcessfully deleted category sub part item!"
        redirect_to admin_item_type_category_part_item_path(id: category_part_item.id, item_type_id: category_part_item.item_type_id)
      end
    end
    private
    def category_part_item_params
      params.require(:category_part_item).permit(:name, :active, :item_type_id, :category_sub_part_item_ids, attachments_attributes: [:id, :name, :attachment, :description, :from_year, :to_year, :model_id, :brand_id, :_destroy])
    end
  end
end