ActiveAdmin.register CatalogType, { :sort_order => :name_asc } do
	permit_params :name, :catalog_type
  menu parent: "API Data"

end
