ActiveAdmin.register Markup do 
  menu parent: "Usuarios", label: "Markups", priority: 100
  permit_params :role, :input, :supplier_id, :active

  # index do 
  #   selectable_column
  #   id_column
  #   actions
  # end

  form do |f|
    f.inputs do
      f.input :supplier, include_blank: "Select Supplier"
      f.input :role, as: :select, :collection => Markup::Role, include_blank: "Select Role"
      f.input :input
      f.input :active, as: :boolean
    end
    f.actions
  end

end