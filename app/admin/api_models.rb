ActiveAdmin.register ApiModel, { :sort_order => :name_asc } do
  permit_params :name, :active, :api_catalog_id, :logo
  menu parent: "API Data"
  belongs_to :api_catalog

  index do
    selectable_column
    id_column
    column :name
    column :active
    column :created_at
    column :updated_at
    column :api_catalog_id do |item|
      item.api_catalog.name
    end
    actions do |api_model|
      link_to (api_model.active == true ? "Active" : "In-Active"), active_model_path(id: api_catalog.id, model_id: api_model.id)
    end
  end

  controller do

    def active_model
      model = ApiModel.find(params['model_id'])
      if model.active?
        model.update_attributes(active: false)
      else
        model.update_attributes(active: true)
      end
      redirect_to request.referer
    end
  
  end



end
