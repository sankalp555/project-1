ActiveAdmin.register CategorySubOption do
  menu parent: "Catalogar"
  permit_params :name, :active
end