ActiveAdmin.register ReferralLink do
  config.clear_action_items!
  menu parent: "Usuarios", label: "Referrals", priority: 100

  index :title => 'Referrals' do
    selectable_column
    id_column
    column :referral_instruction, -> (referral_instruction) {ReferralInstruction.find(referral_instruction.referral_instruction_id)}
    column :refer_by, -> (referral_instruction) {User.find(referral_instruction.user_id)}
    column :refer_to
    column :refer_code
    column :used
    column :awarded
    column :created_at
    column :updated_at
  end

end