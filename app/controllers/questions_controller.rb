class QuestionsController < ApplicationController
  layout 'private'
  include CreateNotification

  def index
    # @quotation_part = QuotationPart.find(params[:my_shopping_id])
    # @quotation_request_part = @quotation_part.quotation_request_part
    @quotation_request_part = QuotationRequestPart.find(params['quotation_request_part_id'])
    @questions = Question.where(quotation_request_part_id: params['quotation_request_part_id'])
    if @answer = Reply.where(question_id: @questions.pluck(:id))
      @answer.update_all(read: true)
      @notifications = Notification.where(notifiable_id: @answer.pluck(:id), notifiable_type: "answer")
      @notifications.update_all(read: true)
    end
  end

  def new
    @quotation_request_part = QuotationRequestPart.find(params[:quotation_part_id])
    @quotation_request_part.questions.update_all(read: true)
    @questions = @quotation_request_part.questions
    @my_question = Question.new
  end

  def create
    @question = Question.new(question_params)
    if @question.save
      if (params["question"]["attachments"]["attachment"] rescue nil).present?
        @attachment = @question.attachments.new(user_id: current_user.id)
        @attachment.attachment = params["question"]["attachments"]["attachment"]
        @attachment.save
      end
      create_notification(@question.id, "question")
      flash[:success] = "Your question submitted successfully!"
      redirect_to all_questions_path(part_id: params[:question][:quotation_request_part_id])
    else
      flash[:error] = "Your question could not submitted!"
      redirect_to all_questions_path(part_id: params[:question][:quotation_request_part_id])
    end
  end

  def show_pdf
    @attachment = Attachment.find(params["attachment_id"])
    pdf_filename = @attachment.attachment.file.file
    send_file(pdf_filename, :filename => @attachment.attachment, :type => "application/pdf")
  end

  def all_questions
    @models = Model.pluck(:name, :id)
    @question = Question.new
    @attachment = @question.attachments.new
    Notification.where(notifiable_type: 'question').update_all(read: true) if !current_user.suppliers_users.present?
    Notification.where(notifiable_type: 'answer').update_all(read: true) if current_user.suppliers_users.present?
    if params['part_id'].present?
      @quotation_request_part = QuotationRequestPart.find(params['part_id'])
      @all_questions = Question.where(quotation_request_part_id: params['part_id']).order(created_at: :desc)
    else
      @all_questions = Question.all.order(created_at: :desc)
    end
  end
  
  def filter_question
    @models = Model.pluck(:name, :id)
    @quotation_request_parts = QuotationRequestPart.where(model_id: params[:filter].to_i)
    @quotation_parts = QuotationPart.where(quotation_request_part_id: @quotation_request_parts.pluck(:id))
    @filtered_questions = Question.where(quotation_part_id: @quotation_parts.pluck(:id))
  end
  
  private

  def question_params
    params.require(:question).permit(:question, :quotation_request_part_id)
  end

end