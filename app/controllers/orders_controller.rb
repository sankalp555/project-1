class OrdersController < ApplicationController
  include CreateNotification
  require 'transbank/sdk'
  require 'mercadopago.rb'
  protect_from_forgery except: [:hook, :mercado_hook]
  layout 'v2_private',  only: [:index, :show]
  $mp = MercadoPago.new('1703237584159932', 'JJzSofowRgEy4p3k8aqZakuJlzfVdmQX')
  
  def index
    if current_user.suppliers_users.present?
      # @orders = Order.where(supplier_id: current_user.suppliers_users.pluck(:supplier_id)).order("created_at DESC").page params[:page]
      ids = []
      OrderItem.all.each do |item|
        if item.quotation_part.present?
          if item.quotation_part.quotation.user_id == current_user.id
            ids << item.id
          end
        end
      end
      @orders = Order.joins(:order_items).where("order_items.id IN (?)", ids).order("created_at DESC").page params[:page]
    else
      @orders = current_user.orders.order("created_at DESC").page params[:page]
    end
    @orders = @orders if params[:time].blank? || params[:time] == "all"
    @orders = @orders.today if params[:time] == "today"
    @orders = @orders.this_week if params[:time] == "week"
    @orders = @orders.this_month if params[:time] == "month"
    @orders = @orders.this_year if params[:time] == "year"

    @orders = @orders.where(status: params[:status]) if params[:status].present? && ['confirmation_pending', 'in_process', 'delivered'].include?(params[:status])
  end
  
  def show
    @order = Order.find(params[:id])
    @payment = Payment.find_by_order_id(@order.id)
    @order_items = @order.order_items
    if current_user.suppliers_users.present?
      ids = current_user.quotations.pluck(:id)
      @order_items = @order.order_items.joins(:quotation_part).where("quotation_parts.quotation_id IN (?)", ids)
    end
    @quotation_parts = QuotationPart.where('id IN (?)', @order_items.pluck(:quotation_part_id))
    if current_user.suppliers_users.present?
      @supplier_user = @order.user
    else
      @supplier_user = @order.order_items.last.quotation_part.quotation.user if @order.order_items.present?
    end
    @courier_job = @order.courier_jobs.last
  end

  #***********PayPal payment gateway******* START
  def paypal
    @cart = Cart.find(params[:cart_id])
    @cart_items = @cart.cart_items
    supplier_id = QuotationPart.find_by_id(@cart_items.last.quotation_part_id).quotation.supplier_id
    @order = Order.create(payment_type: "paypal", status: 'confirmation_pending', user_id: current_user.id, delivery_status: 'Not Shipped Yet', supplier_id: supplier_id, coupon_id: @cart.coupon_id )
    @cart_items.each do |cart_item|
      supplier_id = cart_item.quotation_part.quotation.supplier_id
      user = cart_item.quotation_part.quotation.user if user.present?
      send_web_push_notification(user.id, "Su cotización ha sido aceptada.") if user.present?
      OrderItem.create(quotation_part_id: cart_item.quotation_part_id, order_id: @order.id, quantity: cart_item.quantity, delivery_type: cart_item.delivery_type, status: 'pending', supplier_id: supplier_id)
    end
    @cart.update_column('coupon_id', nil)
    quotes_part_id = @cart_items.where(delivery_type: 'delivery').pluck(:quotation_part_id)
    delivery_cost = QuotationPart.where(id: quotes_part_id).sum(:shipping_price)
    discount = QuotationPart.where(id: @cart_items.pluck(:quotation_part_id)).sum(:discount)
    @total_amount = @cart.total_amount.to_i - discount + delivery_cost
    if (current_user.reedim_amount > 0) 
      @total = check_delivery_type(params[:delivery_type], @total_amount - current_user.reedim_amount)
    else
      @total = check_delivery_type(params[:delivery_type],@total_amount)
    end
    Payment.create(total_amount: @total, payment_gateway: "paypal", order_id: @order.id, status: "pending")
    redirect_to @order.paypal_url(order_path(@order.id), @total, @cart.id)
  end

  def hook
    params.permit! # Permit all Paypal input params
    status = params[:payment_status]
    if status == "Completed"
      @order = Order.find params[:invoice]
      @payment = Payment.find_by_order_id(@order.id)
      @payment.update_attributes(status: "completed", payment_type: params['payment_type'], authorization_code: params['verify_sign']) if @payment.present?
      @order.order_items.update_all(status: "in_process")
      @order.update_attributes(notification_params: params, total_pay_amount: params[:mc_gross], status: "in_process", transaction_id: params[:txn_id], purchased_at: Time.now)
      user_cart_item = CartItem.where(cart_id: params[:cart_id].to_i)
      user_cart_item.delete_all
      user_cart = Cart.find(params[:cart_id].to_i)
      user = @order.user
      user_purchasing = user_cart.total_amount + user.total_purchasing
      user.update_attributes(total_purchasing: user_purchasing)
      user_cart.update(total_amount: 0)
      QuotationMailer.quotes_sold(@order).deliver_now
    else
      render :payment_failed
    end
  end

  # def payment_method
  #   @parts = QuotationPart.where(id: params[:quotation_part_ids])
  #   part_price = 0
  #   @parts.each do |part|
  #     now_price = part.price*part.amount
  #     part_price = now_price+part_price
  #   end
  #   @total = check_delivery_type(params[:delivery_type],part_price)
  #   @delivery_type=params[:delivery_type]
  # end

  #***********PayPal payment gateway ******* END *****

  #Mercado payment method......******>>.........START........<<******
  def mercado_payment_proccess
    # @parts = QuotationPart.where(id: params[:quotation_part_ids])
    # part_price = 0
    # @order = Order.create(address_id: 1, payment_type: "mercado", status: 'Confirmation Pending', user_id: current_user.id, delivery_status: 'not_shipped_yet')
    # @parts.each do |part|
    #   now_price = part.price*part.amount
    #   part_price = now_price+part_price
    #   OrderItem.create(quotation_part_id: part.id, order_id: @order.id) 
    # end
    
    @cart = Cart.find(params[:cart_id])
    user_purchasing = @cart.total_amount + current_user.total_purchasing
    current_user.update_attributes(total_purchasing: user_purchasing)
    @cart_items = @cart.cart_items
    supplier_id = QuotationPart.find_by_id(@cart_items.last.quotation_part_id).quotation.supplier_id
    @order = Order.create(payment_type: "mercado", status: 'confirmation_pending', user_id: current_user.id, delivery_status: 'Not Shipped Yet', supplier_id: supplier_id, coupon_id: @cart.coupon_id   )
    @cart_items.each do |cart_item|
      supplier_id = cart_item.quotation_part.quotation.supplier_id
      user = cart_item.quotation_part.quotation.user
      send_web_push_notification(user.id, "Su cotización ha sido aceptada.") if user.present?
      OrderItem.create(quotation_part_id: cart_item.quotation_part_id, order_id: @order.id, quantity: cart_item.quantity, delivery_type: cart_item.delivery_type, status: 'pending', supplier_id: supplier_id)
    end
    @cart.update_column('coupon_id', nil)
    @total = check_delivery_type(params[:delivery_type],@cart.total_amount)
    Payment.create(total_amount: @total, payment_gateway: "paypal", order_id: @order.id, status: "pending")
    
    preference_data = {
      "items": [
        {
          "invoice": @order.id,
          "unit_price": @total,
          "quantity": 1,
          "currency_id": "ARS",
          "description": "Payment for quotation part"
        }
      ],
      "payer": {
        "name": current_user.name,
        "email": current_user.email
      },
      "back_urls": {
        "success": "#{Rails.application.secrets.app_host}#{mercado_return_path(id: @order.id, cart_id: current_user.cart_id, total: @total)}"
      },
      "installments": 0,
      "notification_url": "#{Rails.application.secrets.app_host}/mercado_hook"

    }
    @preference = $mp.create_preference(preference_data)
    redirect_to @preference["response"]["sandbox_init_point"]
  end

  def mercado_return
    params.permit!
    status = params[:collection_status]
    if status == "approved"
      @order = Order.find params[:id]
      # @payment = Payment.find_by_order_id(@order.id)
      # @payment.update_attributes(status: "completed", payment_type: params['payment_type'], authorization_code: params['verify_sign']) if @payment.present?
      @order.order_items.update_all(status: "in_process")
      @order.update_attributes(notification_params: params, total_pay_amount: params[:total], status: "in_process", transaction_id: params[:preference_id], purchased_at: Time.now)
      user_cart_item = CartItem.where(cart_id: params[:cart_id].to_i)
      user_cart_item.delete_all
      user_cart = Cart.find(params[:cart_id].to_i)
      user = @order.user
      user_purchasing = user_cart.total_amount + user.total_purchasing
      user.update_attributes(total_purchasing: user_purchasing)
      user_cart.update(total_amount: 0)
      QuotationMailer.quotes_sold(@order).deliver_now
      redirect_to order_path(@order.id)
    else
      render :payment_failed
    end
  end

  def mercado_hook
    params.permit!
    payment_id = params["id"]
    @payment_info = $mp.get_payment(payment_id)
  end
  #Mercado payment method......******>>.........END........<<******

  #Transbank with onepay payment method......******>>.........START........<<******
  def onepay
    Transbank::Onepay::Base.integration_type = :TEST
    Transbank::Onepay::Base.api_key = "dKVhq1WGt_XapIYirTXNyUKoWTDFfxaEV63-O5jcsdw"
    Transbank::Onepay::Base.shared_secret = "?XW#WOLG##FBAGEAYSNQ5APD#JF@$AYZ"
    # Transbank::Onepay::Base.app_scheme = ""
    Transbank::Onepay::Base.callback_url = "#{Rails.application.secrets.app_host}/commit?cart_id="+current_user.cart_id.to_s
    #Transbank::Onepay::Base.qr_width_height = 100 # Opcional
    #Transbank::Onepay::Base.commerce_logo_url = "http://some.url/logo" # Opcional
    
    cart = Transbank::Onepay::ShoppingCart.new
    cart.add(Transbank::Onepay::Item.new(
            amount: 10,
            quantity: 1,
            description: "Quotation request payment",
            additional_data: nil,
            expire: -1))
    #channel = params["channel"] # channel will be "WEB" for desktop app and "MOBILE" for mobile app
    channel = Transbank::Onepay::Channel::WEB
    @response = Transbank::Onepay::Transaction.create(shopping_cart: cart,channel: channel)
    @call_back = "#{Rails.application.secrets.app_host}/commit?cart_id="+current_user.cart_id.to_s
    # redirect_to "https://onepay.ionix.cl/mobile-payment-emulator/"

  end


  def supplier_detail
    @supplier = Supplier.find(params[:supplier_id])
    @order_item = OrderItem.find_by_id(params[:item_id])
    @supplier_user = @order_item.quotation_part.quotation.user
    @messages= Message.where("messages.receiver_id =?", current_user.id).order('sent_at')
  end

  def commit
    @status = params[:status]
    @occ = params[:occ]
    @external_unique_number = params[:externalUniqueNumber]

    if @status && @status != 'PRE_AUTHORIZED'
      return render json: {message: 'Error while committing', occ: @occ, external_unique_number: @external_unique_number}, status: 403
    end

    @transaction_commit_response = Transbank::Onepay::Transaction.commit(occ: @occ, external_unique_number: @external_unique_number)
  end

  def onepay_hook
  end
  #Transbank with onepay payment method......******>>.........END........<<******
  
  def self.public_ip
    @public_ip ||= `curl -s ifconfig.me`.strip
  end

  def check_delivery_type(type,total)
    if type == "exp"
      return total+20
    else
      return total
    end
  end

  def payment_failed

  end

  private

  def order_params
    params.require(:order).permit(:quotation_part_id, :quantity, :price, :payment_type).merge(status: 'confirmation_pending', user_id: current_user.id)
  end

end
 
