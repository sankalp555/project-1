class Public::LandingController < Public::BaseController
  include CreateQuotationRequest
  include CreateNotification
  def index
    if current_user.present?
      redirect_to dashboard_index_path 
    else
      @categories = Category.where(active: true).order('name asc').pluck(:name, :id)
      @brands = []
      @main_brands = Brand.first 30
    end
  end

  # GET
  def quote_create
    session["quote_data"] ||= []
    @quote_data = params["quote_param"]
    session["quote_data"] << (@quote_data.to_unsafe_h) if @quote_data.present?
    redirect_to landing_quote_path
  end

  # def quote
  #   @brand = Brand.find_by_id(session["quote_data"].last['brand_id'])
  #   @model = Model.find_by_id(session["quote_data"].last['model_id'])
  #   @category = Category.find_by_id(session["quote_data"].last['category_id'])
  #   @year = session["quote_data"].last['year']
  #   @brands = Brand.pluck(:name, :id)
  #   @models = Model.where(brand_id: session['quote_data'].last["brand_id"]).pluck(:name, :id) rescue []
  #   @resource = User.new
  #   @new_user = User.new
  # end

  def quote
    @sub_part = ApiSubPart.find_by_id(session["quote_data"].last['sub_part_id'])
    @part_catalog = PartCatalog.find_by_id(session["quote_data"].last['part_catalog_id'])
    @api_part = @sub_part.api_part 
    @model = ApiModel.find_by_id(session["quote_data"].last['model_id'])
    @sub_model = ApiSubModel.find_by_id(session["quote_data"].last['sub_model_id'])
    @catalog = ApiCatalog.find_by_id(session["quote_data"].last['catalog_id'])
    @resource = User.new
    @new_user = User.new
  end


  #GET

  def models
    @api_models = ApiModel.where(api_catalog_id: params['catalog_id'])
    data = @api_models.order('name asc').pluck(:name, :id)
    respond_to do |format|
      format.json { render json: data }
    end
  end


  # Start

  def get_date
    @category = Category.find_by_id(params['category_id'])
  end


  def get_brands
    @category = Category.find_by_id(params['category_id'])
    @year = params['year']
    @brands = Brand.where(category_id: params['category_id']).order('name asc')
  end

  def get_models
    @category = Category.find_by_id(params['category_id'])
    @brand = Brand.find(params['brand_id'])
    @year = params['year']
    brand_ids = Brand.where(name: @brand.name).pluck :id if @brand.present?
    @models = Model.where(brand_id: brand_ids)
    if params['model_id'].present?
      @model = Model.find(params['model_id']).order('name asc')
    end  
  end

  def get_fields
    @category = Category.find_by_id(params['category_id'])
    @brand = Brand.find(params['brand_id'])
    @model = Model.find(params['model_id'])
    @year = params['year']
  end

  #Finish


  def brands
    @api_sub_models = ApiSubModel.where(api_model_id: params['model_id']).order('name asc')
    data = @api_sub_models.order('name asc').pluck(:name, :id)
    respond_to do |format|
      format.json { render json: data }
    end
  end

  def get_vehicle
    vehicle = ApiVehicle.find(params['vehicles']['ids'].last)
    @api_sub_model = vehicle.api_sub_model
    redirect_to api_data_api_vehicles_path(sub_model_id: @api_sub_model.id)
  end

  def category_parts
    @category_parts = CategoryPart.all.order('name asc')
    @brand = Brand.find(params['brand_id'])
    @model = Model.find(params['model_id'])
    @year = params['year']
    @category_part_items = CategoryPartItem.all
  end

  def part_items
    if params["part_item"] && params["part_item"]["item_name"].present?
      @category_part_items = CategoryPartItem.where("name ILIKE ?", "%#{params['part_item']['item_name']}%")
      @item_type = @category_part_items.last.item_type
      @brand = Brand.find(params["quote_param"]['brand_id'])
      @category = @brand.category
      @model = Model.find(params["quote_param"]['model_id'])
      @year = params["quote_param"]['year']
      @attachment = @item_type.attachments.where('from_year <= ? AND to_year >= ? AND model_id = ?', @year.to_i, @year.to_i, @model.id).last
    else
      if params['part_item'].present?
        if params['part_item']['vehicle_id'].present?
          @vehicle = Vehicle.find params['part_item']['vehicle_id']
          @category = Category.find(@vehicle.category_id)
          @category_parts = CategoryPart.all
          @brand = @vehicle.brand
          @model = @vehicle.model
          @year = @vehicle.year
          @category_part_items = CategoryPartItem.where("name ILIKE ?", "%#{params['part_item']['item_name']}%")
        end
      else
        @category = Category.find_by_id(params['category_id']) if params['category_id'].present?
        @category_parts = CategoryPart.all
        @brand = Brand.find(params['brand_id'])
        @category = @brand.category
        @model = Model.find(params['model_id'])
        @year = params['year']
        @item_type = ItemType.find(params['item_type_id'])
        @attachment = @item_type.attachments.where('from_year <= ? AND to_year >= ? AND model_id = ?', @year.to_i, @year.to_i, @model.id).last
        @category_part_items = @item_type.category_part_items
      end
    end
  end

  # POST 
  def quote_with_session
    @user = User.find_by(email: user_params[:email])
    valid_password = @user&.valid_password? user_params[:password]

    if !@user or !valid_password
      @user ||= User.new(user_params)
      @user.errors.add(:password, "Correo electrónico o contraseña inválido")
      redirect_to landing_quote_path
    else
      sign_in @user
      redirect_to landing_quote_path
    end
  end

  def login_popup
  end


  def login_with_address
    @user = User.find_by(email: user_params[:email])
    valid_password = @user&.valid_password? user_params[:password]
    if !@user or !valid_password
      @user ||= User.new(user_params)
      flash[:notice] = "Invalid Email Credentials"
      redirect_to final_cart_path
    else
      sign_in @user
      redirect_to final_cart_path
    end
  end 


  def quote_with_registration
    @new_user = User.create(user_registration_params)

    if !@new_user.persisted?
      redirect_to landing_quote_path
    else
      sign_in @new_user
      redirect_to landing_quote_path
    end
  end

  #create quotation after sign in and registraton
  def after_session_create_quotation_request
    if !current_user.addresses.present?
      @address = current_user.addresses.new
      @address.name = params["address"]["name"]
      @address.street_address = params["address"]["street_address"]
      @address.region = params["address"]["region"]
      @address.city = params["address"]["city"]
      @address.phone = params["address"]["phone"]
      @address.primary = true
      @address.save
    end
    quotation_request = create_quotation_request
    create_notification(quotation_request.first.quotation_request_id,"quotation_request")
    flash[:success] = "Solicitud de cotización enviada al proveedor con éxito!"
    session["quote_data"] = nil
    redirect_to dashboard_index_path
  end
  # POST 
  def quote_with_omniauth
    create_quotation_request
    redirect_to dashboard_index_path
  end
  # POST
  def quote_with_current_user
    create_quotation_request
    create_vehicle
    flash[:success] = "Solicitud de cotización enviada al proveedor con éxito!"
    session["quote_data"] = nil
    redirect_to dashboard_index_path
  end
  
  def category_sub_parts
    @category_part_item = CategoryPartItem.find params[:id]
    @sub_parts = @category_part_item.category_sub_part_items
    @brand = Brand.find(params[:brand_id])
    @model = Model.find(params[:model_id])
    @year = params['year']
  end

  def checkout
    @part_catalog = PartCatalog.find_by_id(params['part_catalog_id'])
    @model = ApiModel.find_by_id(params['model_id'])
    @sub_model = ApiSubModel.find_by_id(params['sub_model_id'])
    @catalog = ApiCatalog.find_by_id(params['catalog_id'])
    @sub_part = ApiSubPart.find_by_id(params['sub_part_id'])
  end

  def final_cart
    @sub_part = ApiSubPart.find_by_id(session['quote_data'][0]['sub_part_id'])
  end

  def remove_cart
    if session["quote_data"].count == 1
      sub_part = ApiSubPart.find(session["quote_data"][0]["sub_part_id"])
      session["quote_data"].delete_at(params['indexing'].to_i)
      redirect_to api_data_api_parts_path(part_id: sub_part.api_part.id)
    else
      session["quote_data"].delete_at(params['indexing'].to_i)
      redirect_to landing_quote_path
    end
  end

  def empty_cart
    sub_part = ApiSubPart.find(session["quote_data"][0]["sub_part_id"])
    sub_model = ApiSubModel.find(session["quote_data"][0]["sub_model_id"])
    session["quote_data"].delete_if { |h| h["sub_part_id"].present? }
    redirect_to api_data_api_parts_path(part_id: sub_part.api_part.id, sub_model_id: sub_model.id)
  end

  def update_quantity
    idx = params["data"]["index"].to_i
    session["quote_data"][idx]["quantity"] = params["data"]["quantity"]
    redirect_to landing_quote_path
  end

  def get_part_catalogs
    @part_catalogs = PartCatalog.where(api_catalog_id: params['api_catalog_id']).order('name asc')
    data = @part_catalogs.pluck(:name, :id)
    respond_to do |format|
      format.json { render json: data }
    end
  end

  private

  def create_vehicle
    params["pieces"].each do |piece|
      vehicle = current_user.vehicles.new
      vehicle.model_id = piece['model_id']
      vehicle.brand_id = Model.find(piece['model_id']).brand_id rescue ''
      vehicle.year = piece["year"]
      vehicle.chasis_number = piece["chasis_number"]
      vehicle.save
    end
  end

  def user_params
    params.require(:user).permit(:email, :password)
  end

  def user_registration_params
    params.require(:user).permit(User::REGISTRATION_PARAMS)
  end
end
