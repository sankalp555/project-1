module Public
  module Api
    class BrandsController < BaseController
      before_action :set_brand, only: [:show]

      def index
        render status: :ok, json: Brand.all
      end

      def show
        render status: :ok, json: @brand
      end

      private

      def set_brand
        @brand ||= Brand.find_by params[:id]
        render status: :not_found, json: { errors: { id: "brand not found" } } unless @brand
        @brand
      end
    end
  end
end
