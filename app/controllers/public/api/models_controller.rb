module Public
  module Api
    class ModelsController < BaseController
      before_action :set_brand
      
      def index
        render status: :ok, json: @brand.models
      end

      private

      def set_brand
        @brand = Brand.find params[:brand_id]
        render status: :not_found, json: { errors: { brand_id: 'Brand not found' } } unless @brand
      end
    end
  end
end
