class OrderItemsController < ApplicationController
  include CreateNotification
  before_action :authenticate_user!, only: [:approve]

  def delivered
    @order_item = OrderItem.find(params[:order_item_id])
    @order_item.update_attributes(status: 'confirmation_pending')
    create_notification(@order_item.id, "order_item")
    flash[:success] = "Su artículo está en confirmación pendiente y necesita la forma de confirmación de la mano del usuario."
    redirect_to order_path(@order_item.order.id)
  end

  def approve
    @order_item = OrderItem.find(params[:order_item_id])
    @order_item.update_attributes(status: 'delivered')
    flash[:success] = "Gracias por su confirmación"
    redirect_to order_path(@order_item.order.id)
  end
end
