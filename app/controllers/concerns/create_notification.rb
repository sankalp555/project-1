module CreateNotification
  def create_notification(notifiable_id, notifiable_type)
  	notification = Notification.new(notifiable_id: notifiable_id)
  	notification.update_attribute(:notifiable_type, notifiable_type)
  end
end