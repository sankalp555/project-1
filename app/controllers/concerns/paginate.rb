module Paginate
  def paginate(scope)
    scroll = @page - 1
    count = scope.count
    result = { per_page: @per_page, page: @page, count: count, total_pages: count / @per_page }
    result[:data] = scope
      .limit(@per_page)
      .offset(@per_page * scroll)
      .order(created_at: :desc)
      .map { |it| block_given? ? yield(it) : it }
    result
  end
end
