# module CreateQuotationRequest
#   def create_quotation_request
#     pieces_params = params.permit(pieces: [:model_id, :year, :description, :chasis_number, :amount, :original, :alternative])["pieces"]
#     return if !pieces_params || pieces_params.size.zero?
#     ActiveRecord::Base.transaction do 
#       quotation_request = QuotationRequest.create!(user: current_user)
#       pieces_params.map! do |part_params|        
#         QuotationRequestPart.create!(part_params.merge(quotation_request: quotation_request))
#       end
#     end
#   end
# end


module CreateQuotationRequest
  def create_quotation_request
    pieces_params = params.permit(pieces: [:quantity, :sub_part_id, :model_id, :sub_model_id, :catalog_id, :original, :alternative, :part_catalog_id])["pieces"]
    return if !pieces_params || pieces_params.size.zero?
    ActiveRecord::Base.transaction do
      quotation_request = QuotationRequest.create!(user: current_user)
      pieces_params.map! do |part_params|
        sub_part = ApiSubPart.find(part_params['sub_part_id'])
        vehicle = sub_part.api_part.part_catalog.api_vehicle_model.api_vehicle
        vehicle.update_attributes(user_id: current_user.id)
        parts_hash = part_params
        parts_hash['api_vehicle_id'] = vehicle.id
        parts_hash['api_sub_part_id'] = sub_part.id
        parts_hash.delete('sub_part_id')
        parts_hash.delete('model_id')
        parts_hash.delete('catalog_id')
        parts_hash['api_sub_model_id'] = parts_hash['sub_model_id']
        parts_hash.delete('sub_model_id')
        QuotationRequestPart.create!(parts_hash.merge(quotation_request: quotation_request))
      end
    end
  end
end