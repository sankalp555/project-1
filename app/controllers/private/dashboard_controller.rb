class Private::DashboardController < Private::BaseController
  layout 'v2_private'
  
  def index
  	@unread_messages = current_user.messages.unread
  	@pending_quotes = current_user.quotation_requests.pending
  	@quoted_quotes = current_user.quotation_requests.in_process

    if current_user.suppliers_users.present?
      # @total_orders = Order.where(supplier_id: current_user.suppliers_users.pluck(:supplier_id)).order("created_at DESC")
      ids = []
      OrderItem.all.each do |item|
        if item.quotation_part.present?
          if item.quotation_part.quotation.user_id == current_user.id
            ids << item.id
          end
        end
      end
      @orders = Order.joins(:order_items).where("order_items.id IN (?)", ids).order("created_at DESC").page params[:page]
    else
      @total_orders = current_user.orders.order("created_at DESC")
    end

    @total_orders = @total_orders if params[:time].blank? || params[:time] == "all"
    @total_orders = @total_orders.today if params[:time] == "today"
    @total_orders = @total_orders.this_week if params[:time] == "week"
    @total_orders = @total_orders.this_month if params[:time] == "month"
    @total_orders = @total_orders.this_year if params[:time] == "year"
    @total_orders = @total_orders.where(status: params[:status]) if params[:status].present? && ['confirmation_pending', 'in_process', 'delivered'].include?(params[:status])
    @inprocess_orders = @total_orders.inprocess if @total_orders.present?
    @orders = @total_orders.page(params[:page]).per(10) if @total_orders.present?
  end

  def notices
    @notices = Notice.published
  end
end
