class Private::Api::QuotationsController < Private::BaseController
  def index
    Notification.where(notifiable_type: 'quote').update_all(read: true)
    @notices = Notice.published.notice.mis_pedidos.last(5)
    @tips = Notice.published.tip.mis_pedidos.last(5)
    qr = current_user.quotation_requests
    @quotations = Quotation.where(quotation_request_id: qr)
  end

  def show
    # quotations = Quotation.where(quotation_request_id: params[:id])
    # quotation_request_parts = []
    # quotation_parts_detail = []
    # quotations.each do |quotation|  
    #   quotation_parts = quotation.quotation_parts
    #   @data = {quotation:quotation, quotation_parts_detail:quotation_parts_detail}
    #   # quotation_request_parts.push({quotation:quotation})
    #   quotation_parts.each do |quotation_part|
    #     @data[quotation_parts_detail.push(part_id: quotation_part.id, availability: quotation_part.availability,price: quotation_part.price, warranty: quotation_part.warranty, amount: quotation_part.amount, discount: quotation_part.discount, quotation_part: quotation_part.quotation_request_part)]
    #     # data = {price: quotation_part.price, amount: quotation_part.amount, quotation_part: quotation_part.quotation_request_part} 
    #     # quotation_request_parts.push(data)
    #   end
    # end
    # if !@data.nil?
    #   @notices = Notice.published.notice.mis_pedidos.last(5)
    #   @tips = Notice.published.tip.mis_pedidos.last(5)
    #   @data
    # else
    #   @notices = Notice.published.notice.mis_pedidos.last(5)
    #   @tips = Notice.published.tip.mis_pedidos.last(5)
    # end
    @quotation_request_parts = QuotationRequestPart.where(quotation_request_id: params[:id])
    @notices = Notice.published.notice.mis_pedidos.last(5)
    @tips = Notice.published.tip.mis_pedidos.last(5)
  end

  def show_details
    #quotation = Quotation.where("quotation_request_id": params[:quotation_request_id])
    quotation = Quotation.find(params[:id])
    quotation_parts = QuotationPart.where(id: params[:part_id])
    quotation_request_parts = []
    quotation_parts_detail = []
    @data = {quotation:quotation, quotation_parts_detail:quotation_parts_detail}
    # quotation_request_parts.push({quotation:quotation})
    quotation_parts.each do |quotation_part|
      @data[quotation_parts_detail.push(price: quotation_part.price, amount: quotation_part.amount, quotation_part: quotation_part.quotation_request_part)]
      # data = {price: quotation_part.price, amount: quotation_part.amount, quotation_part: quotation_part.quotation_request_part} 
      # quotation_request_parts.push(data)
    end
    if quotation_parts
      @notices = Notice.published.notice.mis_pedidos.last(5)
      @tips = Notice.published.tip.mis_pedidos.last(5)
      @data
    else
      @notices = Notice.published.notice.mis_pedidos.last(5)
      @tips = Notice.published.tip.mis_pedidos.last(5)
    end
  end

  def update
  end

  def destroy
  end

  private

  def quotations_params
    params.permit(:id, :discount, :quotation_request_id, :supplier_id, :user_id)
  end
end
