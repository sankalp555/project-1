class Private::Api::Suppliers::QuotationRequestsController < Private::BaseController
  layout :resolve_layout

  def index
    @notifications = Notification.where(notifiable_type: "quotation_request")
    @notifications.update_all(read: true)
    
    @quotation_requests = scope if params[:time].blank? || params[:time] == "all"
    @quotation_requests = scope.today if params[:time] == "today"
    @quotation_requests = scope.this_week if params[:time] == "week"
    @quotation_requests = scope.this_month if params[:time] == "month"
    @quotation_requests = scope.this_year if params[:time] == "year"
    @quotation_requests = @quotation_requests.where(status: params[:status]) if params[:status].present?  && ['pending', 'in_process', 'approved'].include?(params[:status])
    @total_quotation_requests = @quotation_requests.order("created_at DESC")
    @quotation_requests = @total_quotation_requests.page params[:page]
    
    @notices = Notice.published.notice.first(5)
    @tips = Notice.published.tip.first(5)
    @message = Message.new
  end

  def show
    @quotation_req = scope.find(params[:id])
  
    if @quotation_req
      @quotation_request_parts = @quotation_req.quotation_request_parts
      sub_part_ids = @quotation_request_parts.pluck :api_sub_part_id
      @api_sub_parts = ApiSubPart.where('id IN (?)', sub_part_ids)
      @api_vehicle = @quotation_request_parts.last.api_vehicle
      @api_sub_model = @api_vehicle.api_sub_model
      @api_model = @api_sub_model.api_model
      @api_catalog = @api_model.api_catalog
    else
      flash[:error] = "No record found!"
    end
    @quotations = @quotation_req.quotations.where(user_id: current_user.id)
    if @quotations.present?
      @markup = @quotations.last.supplier.markups.where(active: true).last if @quotations.last.supplier.markups.present?
    end
  end

  private

  def scope
    quotation_requests = QuotationRequest.new
    catalogs = current_user.supplier_catalogs.pluck(:id)
    QuotationRequest.joins(quotation_request_parts: :api_vehicle).where("quotation_request_parts.part_catalog_id IN (?)", catalogs).where("quotation_requests.created_at >= ?", current_user.created_at).order(created_at: :desc).distinct
  end


  def resolve_layout
    case action_name
    when "show"
      "public"
    else
      "v2_private"
    end
  end
end
