class Private::Api::Suppliers::BrandsController < Private::BaseController
  before_action :set_brand, only: [:create, :destroy]

  def index
    @supplier_brands = current_supplier.brands
    @brands = @supplier_brands.pluck(:name, :id)
  end

  def new
    
  end

  def create
    if @brand
      current_supplier.brands << @brand
      flash[:success] = "Brand added successfully!"
      redirect_to private_api_suppliers_brands_path
    else
      flash[:error] = "Brand could not added!"
      redirect_to private_api_suppliers_brands_path
    end
  end

  def destroy
    if @brand
      current_supplier.brands.delete @brand
      flash[:success] = "Brand deleted successfully!"
      redirect_to private_api_suppliers_brands_path
    else
      flash[:error] = "Brand could not deleted!"
      redirect_to private_api_suppliers_brands_path
    end
  end

  private

  def current_supplier
    current_user.suppliers.first
  end

  def set_brand
    @brand = Brand.find_by(id: params[:brand_id] || params[:id])
  end
end
