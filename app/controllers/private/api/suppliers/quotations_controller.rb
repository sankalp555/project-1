class Private::Api::Suppliers::QuotationsController < Private::BaseController
  include CreateNotification
  layout 'v2_private'

  def index
    @notices = Notice.published.notice.mis_pedidos.last(5)
    @tips = Notice.published.tip.mis_pedidos.last(5)
    @quotations = current_user.supplier.quotations.order(created_at: :desc).page params[:page]
  end

  def show
    # quotations = Quotation.where(quotation_request_id: params[:id])
    # quotation_request_parts = []
    # quotation_parts_detail = []
    # quotations.each do |quotation|  
    #   quotation_parts = quotation.quotation_parts
    #   @data = {quotation:quotation, quotation_parts_detail:quotation_parts_detail}
    #   # quotation_request_parts.push({quotation:quotation})
    #   quotation_parts.each do |quotation_part|
    #     @data[quotation_parts_detail.push(part_id: quotation_part.id,price: quotation_part.price, warranty: quotation_part.warranty, amount: quotation_part.amount, discount: quotation_part.discount, availability: quotation_part.availability, quotation_part: quotation_part.quotation_request_part)]
    #     # data = {price: quotation_part.price, amount: quotation_part.amount, quotation_part: quotation_part.quotation_request_part} 
    #     # quotation_request_parts.push(data)
    #   end
    # end
    # if !@data.nil?
    #   @notices = Notice.published.notice.mis_pedidos.last(5)
    #   @tips = Notice.published.tip.mis_pedidos.last(5)
    #   @data
    # else
    #   @notices = Notice.published.notice.mis_pedidos.last(5)
    #   @tips = Notice.published.tip.mis_pedidos.last(5)
    # end
    @quotation_request_parts = QuotationRequestPart.where(quotation_request_id: params[:id])
    @notices = Notice.published.notice.mis_pedidos.last(5)
    @tips = Notice.published.tip.mis_pedidos.last(5)
  end

  def new
    quotation_req = scope.find(params[:id])
    if quotation_req
      @markup = current_user.supplier.markups.where(active: true).last if current_user.supplier.markups.present?
      @quotation_request = quotation_req.quotation_request_parts
      @quotation_request_parts = QuotationRequestPart.where(id: params['part_ids'])
    else
      flash[:error] = "ningún record fue encontrado!"
    end
  end

  def create
    @quote_created = create_with_parts
    if @quote_created
      user = @quote_created.quotation_request.user
      quotation_request = QuotationRequest.find_by_id(params["quotation_request_id"])
      quotation_request.update_column('status', 'in_process') if quotation_request.present?
      send_web_push_notification(user.id, "New Quote has been Provided to your request")
      create_notification(@quote_created.id, "quote")
      flash[:success] = "Cita creado con éxito!"
      redirect_to private_api_suppliers_quotation_requests_path
    else
      flash[:error] = "¡La cita no pudo ser creada!"
      redirect_to private_api_suppliers_quotation_requests_path
    end
  end

  def edit
    quotation_req = scope.find(params[:id])
    if quotation_req
      @quotation_request = quotation_req.load_parts
    else
      flash[:error] = "ningún record fue encontrado!"
    end
  end

  def update
  end

  def destroy
  end

  def approve_quote
    quote = QuotationPart.find(params[:id])
    if quote.update(accepted: true)
      QuotationMailer.quote_approved(quote).deliver_now
      flash[:success] = "Cita aceptada con éxito!"
      redirect_to private_api_quotation_path(params[:quotation_id])
    else
      flash[:success] = "No se pudo aceptar cotización!"
      redirect_to private_api_quotation_path(params[:quotation_id])
    end
  end

  private

  def quotation_params
    aux = params.permit(:quotation_request_id, :discount)
    aux[:user_id] = current_user.id
    aux[:supplier_id] = current_user.supplier.id
    aux
  end

  def quotation_parts_params
    params.permit(quotation_parts: [:quotation_request_part_id, :price, :amount, :discount, :type_of_request, :shipping, :brand, :shipping_price, :availability, :warranty])["quotation_parts"]
  end

  def create_with_parts
    quotation = nil
    quotation = Quotation.create!(quotation_params)
    params['quotation_parts'].each do |k, part|
      if part["check"] == "true"
        quotation_part = QuotationPart.new
        quotation_part.quotation_id = quotation.id
        quotation_part.quotation_request_part_id = part['quotation_request_part_id']
        quotation_part.price = part['price']
        quotation_part.amount = part['amount']
        quotation_part.brand = part['brand']
        quotation_part.type_of_request = part['type_of_request']
        quotation_part.warranty = part['warranty']
        quotation_part.shipping_type = part['shipping_type']
        quotation_part.shipping_price = part['shipping_price']
        quotation_part.discount = part['discount']
        quotation_part.shipping = part['shipping_date']
        quotation_part.save
      end
    end
    quotation
  end

  def scope
    brands = current_user.supplier_brands
    QuotationRequest.joins(quotation_request_parts: :vehicle)
      .where("vehicles.brand_id IN (?)", brands.pluck(:id))
      .distinct
  end
end
