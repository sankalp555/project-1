class Private::Api::ProfileController < Private::BaseController
  
  layout :resolve_layout
  #********address START *********
  def new_address
  end

  def address_create
    @address = current_user.addresses.new(address_params)
    country = CS.states(:cl)
    @address.region = country[params['address']['region'].to_sym]
    if @address.save
      address_params["primary"].present? ? check_primary_address(@address.id) : ''
      flash[:success] = "Successfully added address!"
      redirect_to private_api_profile_show_path
    else
      flash[:error] = @address.errors.full_messages.join(", ")
      redirect_to private_api_profile_show_path
    end
  end

  def edit_address
    @address = Address.find(params[:id])
    @regions = CS.states(:cl)
    region_code = @regions.key(@address.region)
    @states = CS.states(:cl).values
    @cities = CS.cities(region_code)
  end

  def update_address
    @address = Address.find(params[:id])
    if @address.update(address_params.merge(primary: address_params["primary"].present?))
      address_params["primary"].present? ? check_primary_address(@address.id) : ''
      flash[:success] = "Successfully updated address!"
      redirect_to private_api_profile_show_path
    else
      flash[:error] = "Address could not update please try again later!"
      @address_errors = @address.errors.full_messages
      redirect_to private_api_profile_show_path
    end
  end

  def destroy_address
    @address = Address.find(params[:id])
    user = @address.addressable
    if @address.primary? && user.addresses.count >= 2
      user.addresses.where.not(id: @address.id).last.update_attributes(primary: true)
    end
    if @address.destroy
      flash[:success] = "Successfully deleted address!"
      redirect_to private_api_profile_show_path
    else
      flash[:error] = "Address could not be deleted!"
      redirect_to private_api_profile_show_path
    end
  end
  
  def check_primary_address id
    @not_primary = Address.where.not(id: id)
    if @not_primary
      @not_primary.update_all(primary: false)
      Address.where(id: id).update(primary: true)
    end
  end
  #***********address END ****
  
  def show

  end
  
  def edit
    @preference = UserNotifyPreference.find_by_user_id(current_user.id)
  end

  def update
    if current_user.valid_password?(params['user']['current_password'])
      user = current_user
      @notify = UserNotifyPreference.find_by_user_id(current_user.id)
      if user.update(which_params)

        if !params["notify"].nil?
          if @notify.update(new_conf_shipments: notify_preference_params["new_conf_shipments"].present?, promo_discount: notify_preference_params["promo_discount"].present?, payment: notify_preference_params["payment"].present?, dispatch_delivery: notify_preference_params["dispatch_delivery"].present?)
            flash[:success] = "Successfully update preferences!"
            redirect_to private_api_profile_show_path
          else
            flash[:error] = "Nothing updated in preferences!"
            redirect_to private_api_profile_show_path
          end
        else
          @notify.update(new_conf_shipments: false, promo_discount: false, payment: false, dispatch_delivery: false)
          flash[:success] = "Successfully updated preferences!"
          redirect_to private_api_profile_show_path
        end
      else
        flash[:error] = user.errors.full_messages
        redirect_to private_api_profile_show_path
      end
    else
      flash[:error] = "Current Password is not Correct"
      redirect_to private_api_profile_show_path
    end
  end

  def update_password

  end

  def edit_notify_preference
    # @preference = UserNotifyPreference.find_by_user_id(current_user.id)
  end
  def update_notify_preference
    # @notify = UserNotifyPreference.find_by_user_id(current_user.id)
    # if !params["notify"].nil?
    #   if @notify.update(new_conf_shipments: notify_preference_params["new_conf_shipments"].present?, promo_discount: notify_preference_params["promo_discount"].present?, payment: notify_preference_params["payment"].present?, dispatch_delivery: notify_preference_params["dispatch_delivery"].present?)
    #     flash[:success] = "Successfully update preferences!"
    #     redirect_to private_api_profile_show_path
    #   else
    #     flash[:error] = "Nothing updated in preferences!"
    #     redirect_to private_api_profile_show_path
    #   end
    # else
    #   @notify.update(new_conf_shipments: false, promo_discount: false, payment: false, dispatch_delivery: false)
    #   flash[:success] = "Successfully updated preferences!"
    #   redirect_to private_api_profile_show_path
    # end
  end
  private

  def profile_params
    params.require(:user).permit(:name, :last_name, :phone, :cellular, :office, :email, :dob, :password, :password_confirmation, addresses_attributes: [:id, :name, :street_address, :region, :city, :comuna, :phone, :_destroy])
  end

  def profile_params_without_password
    params.require(:user).permit(:name, :last_name,  :phone, :cellular, :office, :email, :dob)
  end

  def which_params
    if ((params[:user][:password_current].present? && current_user.valid_password?(params[:user][:password_current])) || params[:user][:password].present? || params[:user][:password_confirmation].present?)
        profile_params
    else
      profile_params_without_password
    end
  end
  
  def address_params
    params.require(:address).permit(:name, :street_address, :region, :city, :comuna, :phone, :_destroy, :primary)
  end

  def notify_preference_params
    params.require(:notify).permit(:new_conf_shipments, :promo_discount, :payment, :dispatch_delivery)
  end

  def resolve_layout
    if (action_name == "show")
      'v2_private'
    else
      'private'
    end
  end
end
