class Private::Api::QuotationRequestsController < Private::BaseController
  include CreateQuotationRequest
  include CreateNotification
  layout 'v2_private'
  #include Paginate
  #before_action :set_pagination_defaults

  def index
    if params[:time].present? || params[:status].present?
      @quotation_requests = current_user.quotation_requests
      @quotation_requests = @quotation_requests if params[:time].blank? || params[:time] == "all"
      @quotation_requests = @quotation_requests.today if params[:time] == "today"
      @quotation_requests = @quotation_requests.this_week if params[:time] == "week"
      @quotation_requests = @quotation_requests.this_month if params[:time] == "month"
      @quotation_requests = @quotation_requests.this_year if params[:time] == "year"
      @quotation_requests = @quotation_requests.where(status: params[:status]) if params[:status].present?  && ['pending', 'in_process', 'approved'].include?(params[:status])
      
      @total_quotation_requests = @quotation_requests.order("created_at DESC")
    else
      @total_quotation_requests = current_user.quotation_requests.order("created_at DESC")
    end

    @quotation_requests = @total_quotation_requests.page params[:page]
    @pending_count = @total_quotation_requests.pending.count
    @quoted_count = @total_quotation_requests.in_process.count
    @approved_count = @total_quotation_requests.approved.count

    @question = Question.new
    @button = 'new_quotation'
    @notices = Notice.published.notice.mis_pedidos.last(5)
    @tips = Notice.published.tip.mis_pedidos.last(5)
  end

  def new
    @user_vehicles = ApiVehicle.where(user_id: current_user.id)
  end
  
  def show
    quot_request = current_user.quotation_requests.find(params[:id])
    if quot_request.present?
      @quotation_request = quot_request.load_parts
      @quotation_recived = quot_request.quotations
    else
      flash[:error] = "No record found!"
    end
  end

  def create
    begin
      @quotation_request = current_user.quotation_requests.create
      create_notification(@quotation_request.id,"quotation_request")
      @quotation_request.transaction do
        if params["vehicle-selection"] == "new_vehicle"
          vehicle = current_user.vehicles.new(vehicle_params)
          if vehicle.save
            vehicle_ids = [vehicle.id]
            create_items(vehicle_ids)
          end
        elsif params["vehicle-selection"] == "my_vehicle"
          vehicle_ids = params["vehicles"]["ids"]
          create_items(vehicle_ids)
        end
        redirect_to private_api_quotation_requests_path  
      end
      rescue ActiveRecord::RecordInvalid => invalid
    end
  end

  def create_items(vehicles)
    items_hash = eval(params["items"])
    request_items = []
    vehicles.each do |vehicle|
      items_hash.each do |item|
        item['vehicle_id'] = vehicle
        request_items << item
      end
    end
    @quotation_request.quotation_request_parts.create(request_items)
  end

  def edit
    @brands = Brand.pluck(:name, :id)
    @user_vehicles = current_user.vehicles
    # @quotation_request = current_user.quotation_requests.find(params[:id])
    # @quotation_request_part = @quotation_request.quotation_request_parts.last
    @quotation_request_part = QuotationRequestPart.find params[:id]
  end

  def update

    @quotation_request_part = QuotationRequestPart.find params[:id]

    pieces_params = params.permit(:description, :amount, :original, :alternative)
    
    #return if !pieces_params || pieces_params.size.zero?
    if @quotation_request_part.update_attributes(pieces_params)
      flash[:success] = "Quotation request updated successfully!"
      redirect_to private_api_quotation_requests_path
    end
    # ActiveRecord::Base.transaction do 
    #   pieces_params.map! do |part_params|
    #   end
    # end
  end

  def destroy
  end

  def get_user_vehicle
    @user_vehicles = current_user.vehicles.where(brand_id: params[:brand_id])
    data = [@user_vehicles.first.model_id, @user_vehicles.first.year, @user_vehicles.first.chasis_number]
    respond_to do |format|
      format.json { render json: data }
    end
  end
  
  def quotation_request_detail
    @request = QuotationRequest.find_by_id(params['quotation_request_id'])
    @request.quotations.update_all(flag: true) if @request.quotations.present?
    @quotation_requests = QuotationRequestPart.where(quotation_request_id: params[:quotation_request_id]).order(created_at: :desc).page params[:page]
    @vehicle = @quotation_requests.last.api_vehicle
    @question_count = @quotation_requests.last.questions.where(read: false).count if @quotation_requests.present?
  end

  def vehicle_params
    params.require(:vehicle).permit(:brand_id, :model_id, :chasis_number, :year, :patent)
  end
end
