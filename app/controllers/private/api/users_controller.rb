class Private::Api::UsersController < Private::Api::BaseController
  def index
    render status: :ok, json: current_user.profile_summary
  end
end
