class Private::Api::BaseController < ActionController::API
  include Devise::Controllers::Helpers

  before_action :authenticate_user!

  private 

  def set_pagination_defaults
    @per_page = params[:per_page]&.to_i || 10
    @page = params[:page]&.to_i || 1
  end
end
