class Private::BaseController < ActionController::Base
  include Devise::Controllers::Helpers
  
  protect_from_forgery with: :exception
  layout 'private'
  before_action :authenticate_user!

  def send_web_push_notification(user_id, message_body)
    subscription = User.find(user_id).web_push_subscription
    message = {
        title: "You have a notification!",
        body: "#{message_body}",
        tag: "new-Notification"
    }
    unless subscription.empty?
      begin
        Webpush.payload_send(
          message: JSON.generate(message),
          endpoint: subscription["endpoint"], 
          p256dh: subscription["keys"]["p256dh"], 
          auth: subscription["keys"]["auth"], 
          ttl: 15, 
          vapid: { 
            subject: 'mailto:admin@example.com', 
            public_key: $vapid_public,
            private_key: $vapid_private
          }
        )
      rescue Webpush::InvalidSubscription => exception
        User.find(user_id).update_attributes(web_push_subscription: {})
      end
    end
  end


end
