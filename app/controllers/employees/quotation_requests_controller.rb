class Employees::QuotationRequestsController < Employees::BaseController
 
  def index
    @quotations = QuotationRequest.all.order(created_at: :desc).page params[:page]
  end

  def show

  end
end
