class Employees::BaseController < ActionController::Base
  protect_from_forgery with: :exception
  layout :layout_for

  protected

  def layout_for
    current_employee ? 'employee_dashboard' : 'employee'
  end

  def set_pagination_defaults
    @per_page = params[:per_page]&.to_i || 10
    @page = params[:page]&.to_i || 1
  end
end