class Employees::QuotationsController < Employees::BaseController

  def index
  end

  def show
    @quotation_request_parts = QuotationRequestPart.where(quotation_request_id: params[:id])
  end
  
end
