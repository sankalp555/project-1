# frozen_string_literal: true
class Employees::SessionsController < Devise::SessionsController
  layout :layout_for
  # before_action :configure_sign_in_params, only: [:create]

  # GET /resource/sign_in
  # def new
  #   @resource = Employee.new
  #   super
  # end

  # POST /resource/sign_in
  def create
    employee = Employee.find_by(email:employee_params[:email])
    valid_privilege = employee&.valid_privilege?
    if valid_privilege
      valid_password = employee&.valid_password?(employee_params[:password])
      if !employee or !valid_password
        @resource = Employee.new(employee_params)
        flash[:error] = "Correo electrónico o contraseña inválido"
        redirect_to new_employee_session_path
      else
        super
      end
    else
      flash[:error] = "No access permission or Not registred yet!"
      redirect_to new_employee_session_path
    end
  end

  # DELETE /resource/sign_out
  # def destroy
  #   super
  # end

  protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.permit(:sign_in, keys: [:attribute])
  # end

  def after_sign_in_path_for(resource)
    "/employee_dashboard"
  end
  
  def employee_params
    params.require(:employee).permit(:email, :password)
  end

  def layout_for
    current_employee ? 'employee_dashboard' : 'employee'
  end
end
