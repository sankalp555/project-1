class CartsController < ApplicationController
  layout 'v2_private', only: [:index, :entrega]


  def index
    @cart_id = check_cart_id
    @cart_items = CartItem.where(cart_id: @cart_id)
    @cart = Cart.find(@cart_id)
    if @cart.coupon.present?
      @cart.update(total_amount: @cart.total_amount.to_i - @cart.coupon.discount, coupon_applied: true) if @cart.coupon_applied == false
    end
    quotation_part_ids = @cart_items.pluck(:quotation_part_id)
    @quotation_parts = QuotationPart.where(id: quotation_part_ids)
    @delivey_cost = @quotation_parts.where(shipping_type: ['delivery', 'both']).sum(:shipping_price)
  end
  
  def new
  end
  
  def entrega
    @cart_id = check_cart_id
    @cart_items = CartItem.where(cart_id: @cart_id)
    @cart_items.each {|ci| ci.update_attributes(delivery_type: params["part"]["#{ci.quotation_part_id}"]["delivery_type"])} if params["part"].present?
    quotes_part_id = @cart_items.where(delivery_type: 'delivery').pluck(:quotation_part_id)
    delivery_cost = QuotationPart.where(id: quotes_part_id).sum(:shipping_price)
    discount = QuotationPart.where(id: @cart_items.pluck(:quotation_part_id)).sum(:discount)
    @cart = Cart.find(@cart_id)
    @total_amount = @cart.total_amount.to_i - discount + delivery_cost
  end

  def change_address
    available_id = params["pickup_ids"]
    available_id.delete(params["quotation_part_id"]) if available_id.present?
    pickup_part_ids = available_id

    @cart = Cart.find(check_cart_id)
    @cart_items = CartItem.where(cart_id: @cart.id)
    quotation_part_ids = @cart_items.pluck(:quotation_part_id)
    @quotation_parts = QuotationPart.where(id: quotation_part_ids)

    #get previous total
    if pickup_part_ids.present?
      previous_total = @quotation_parts.where(shipping_type: ['delivery', 'both']).where(id: [pickup_part_ids]).present? ? @quotation_parts.where(shipping_type: ['delivery', 'both']).where(id: [pickup_part_ids]).sum(:shipping_price).to_i : 0
    else
      previous_total = 0
    end

    @delivey_cost = @quotation_parts.where(shipping_type: ['delivery', 'both']).sum(:shipping_price)

    @quotation_part = QuotationPart.find(params['quotation_part_id'])
    if params['value'] == 'delivery'
      @address = current_user.addresses.primary_address.last
    elsif params['value'] == 'pickup'
      @delivey_cost = @delivey_cost - (@quotation_part.shipping_price.to_i + previous_total)
      @address = @quotation_part.quotation.supplier.address
    end
  end

  def finalizar
    @cart = Cart.find(params["cart_items"]["cart_id"].to_i)
    @cart_items = CartItem.where(cart_id: @cart.id)
    @subtotal = @cart.total_amount
    @total = check_delivery_type(params[:delivery_type],@cart.total_amount)
    @delivery_type=params[:delivery_type]
  end

  def check_delivery_type(type,total)
    if type == "exp"
      return total+20
    else
      return total
    end
  end
  
  def create
    @cart_id = check_cart_id
    quotation_part = QuotationPart.find(params["cart_items"]["quotation_part_id"])
    quotation_part.update_attributes(status: "approved")
    quotation_request = quotation_part.quotation_request_part.quotation_request
    @quotation_request_part = quotation_part.quotation_request_part
    if current_user.cart.cart_items.present?
      current_user.cart.cart_items.each do |cart_item|
        part = QuotationPart.where(quotation_request_part_id: cart_item.quotation_part.quotation_request_part_id).last
        if part.quotation_request_part_id == quotation_part.quotation_request_part_id
          current_user.cart.update(total_amount: 0, coupon_applied: false)
          cart_item.destroy
        end
      end
    end

    quotation_request.update_column('status', 'approved')
    check_quotation_part_id = CartItem.where(quotation_part_id: params["cart_items"]["quotation_part_id"], cart_id: @cart_id)
    if check_quotation_part_id.present?
      quantity = check_quotation_part_id.first.quantity+params["cart_items"]["quantity"].to_i
      if check_quotation_part_id.update_all(quantity: quantity)
        @cart = Cart.find(@cart_id)
        total_amount_current_pro_now = params["cart_items"]["price"].to_f * params["cart_items"]["quantity"].to_i
        total_amount_now = @cart.total_amount + total_amount_current_pro_now
        @cart.update(total_amount: total_amount_now)
      end
    else  
      @cart_item = CartItem.new(quotation_part_id: params["cart_items"]["quotation_part_id"], cart_id: @cart_id, quantity:  params["cart_items"]["quantity"].to_i)
      if @cart_item.save
        @cart = Cart.find(@cart_item.cart_id)
        current_price = params["cart_items"]["price"].to_f * params["cart_items"]["quantity"].to_i
        total_amount_now = @cart.total_amount+current_price
        @cart.update(total_amount: total_amount_now)
      end
    end

    @request = quotation_request
    @quotation_requests = QuotationRequestPart.where(quotation_request_id: @request.id).order(created_at: :desc).page params[:page]
    @vehicle = @quotation_requests.last.api_vehicle
    @question_count = @quotation_requests.last.questions.where(read: false).count if @quotation_requests.present?
    current_user.cart.cart_items.reload
  end

  def apply_coupon
    coupon = Coupon.where("code = ? AND active = ?", params['coupon'], true).last
    user_coupon = current_user.user_coupons.where(coupon_id: coupon.id, applied: false).last if coupon.present?
    if coupon.present? && user_coupon.present?
      @cart = current_user.cart
      if coupon.discount_type == 'amount'
        amount_after_discount = @cart.total_amount - coupon.discount.to_i
      elsif coupon.discount_type == "percent"
        discount_amount = (coupon.discount.to_f * @cart.total_amount)/ 100
        amount_after_discount = @cart.total_amount - discount_amount
      end
      @cart.update_attributes(total_amount: amount_after_discount, coupon_id: coupon.id)
      user_coupon.update_column('applied', true)
      render json: { coupon: coupon, status: 200, cart: @cart }
    else
      render json: {message: "Invalid Coupon Code", status: 200}
    end
  end

  def remove_coupon
    coupon = current_user.cart.coupon
    @cart = current_user.cart
    if coupon.present? 
      if coupon.discount_type == 'amount'
        amount_after_discount = @cart.total_amount.to_i
      elsif coupon.discount_type == "percent"
        p = 100.0 - coupon.discount.to_f
        amount_after_discount = (@cart.total_amount * 100)/p
      end
      user_coupon = current_user.user_coupons.where(coupon_id: coupon.id, applied: true).last
      user_coupon.update_column('applied', false) if user_coupon.present?
      @cart.update_attributes(total_amount: amount_after_discount, coupon_id: nil)
      render json: { coupon: coupon, status: 200, cart: @cart }
    else
      render json: {message: "Invalid Coupon Code", status: 200}
    end
  end
  
  def check_cart_id
    if !current_user.cart_id.nil?
      return current_user.cart_id
    else
      @user_cart = Cart.new(total_amount: 0)
      if @user_cart.save
        User.where(id: current_user.id).update(cart_id: @user_cart.id)    
      end
      return @user_cart.id
    end
  end

  # def destroy
  #   @cart_item = CartItem.find(params[:id])
  #   qotation_part = QuotationPart.find(@cart_item.quotation_part_id)
  #   qotation_part.update_attributes(status: "pending")
  #   to_minus_total_amount = qotation_part.price * qotation_part.amount rescue 0
  #   @cart = Cart.find(@cart_item.cart_id)
  #   if @cart_item.destroy
  #     total_amount = @cart.total_amount-to_minus_total_amount rescue 0
  #     @cart.update(total_amount: total_amount)
  #     @cart.update_attributes(total_amount: 0) if @cart.cart_items.empty?
  #     flash[:success] = "¡El artículo del carrito fue eliminado exitosamente!"
  #     redirect_to carts_path
  #   end
  # end

  def delete_cart
    @cart_item = CartItem.find(params[:id])
    qotation_part = QuotationPart.find(@cart_item.quotation_part_id)
    qotation_part.update_attributes(status: "pending")
    to_minus_total_amount = qotation_part.price * qotation_part.amount rescue 0
    @cart = Cart.find(@cart_item.cart_id)
    if @cart_item.destroy
      total_amount = @cart.total_amount-to_minus_total_amount rescue 0
      @cart.update(total_amount: total_amount)
      @cart.update_attributes(total_amount: 0) if @cart.cart_items.empty?
      flash[:success] = "¡El artículo del carrito fue eliminado exitosamente!"
      redirect_to private_api_quotation_request_quotation_request_detail_path(quotation_request_id: qotation_part.quotation.quotation_request_id)
    end
  end 

  def get_cities
    @regions = CS.states(:cl)
    code = @regions.invert[params['region']]
    data = CS.cities(code, :cl)
    respond_to do |format|
      format.json { render json: data }
    end
  end 

  
  def cart

  end
  private

  def cart_params
    params.require(:cart_items).permit(:quotation_part_id, :price, :quantity, :delivery_type, :address_id, :cart_id)["quotation_part_ids"]
  end
end
