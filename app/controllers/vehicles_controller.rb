class VehiclesController < ApplicationController
  layout 'v2_private'

  def index
    @vehicles = ApiVehicle.where(user_id: current_user.id).page params[:page]
    @notices = Notice.published.notice.mis_vehiculos.last(5)
    @tips = Notice.published.tip.mis_vehiculos.last(5)
    @button = 'my_vehicle'
  end

  def new
    @api_catalogs = ApiCatalog.order('name asc').pluck(:name, :id)
    @api_models = []
    @api_sub_models = []
    @vehicle = ApiVehicle.new
  end

  def edit
    @vehicle = ApiVehicle.find(params[:id])
    @categories = Category.order('name asc').pluck(:name, :id)
    @brands = Brand.where(category_id: @vehicle.category_id).order('name asc').pluck(:name, :id)
    @models = Model.where(brand_id: @vehicle.brand_id).order('name asc').pluck(:name, :id)
  end

  def show
    vehicles = current_user.vehicles.find(params[:id])
    if vehicles
      render json: vehicle.load_parts
    else
      render status: :not_found, json: nil
    end
  end

  def create
    create_vehicle = current_user.vehicles.new(vehicle_params)
    if create_vehicle.save
    	flash[:success] = "Mis vehiclous added successfully!"
      redirect_to vehicles_path
    else
      flash[:error] = "Brand ya está en uso and Model ya está en uso"
      redirect_to vehicles_path
    end
  end

  def update
    @vehicle = Vehicle.find(params[:id])
    if @vehicle.update(vehicle_params)
      flash[:success] = "Mis vehiclous updated successfully!"
      redirect_to vehicles_path
    else
      @brands = Brand.pluck(:name, :id)
      @models = Model.where(brand_id: @vehicle.brand_id).pluck(:name, :id)
      flash[:error] = @vehicle.errors.full_messages
      redirect_to vehicles_path
    end
  end

  def destroy
    @vehicle = Vehicle.find(params[:id])
    if @vehicle.destroy
      flash[:success] = "Mis vehiclous deleted successfully!"
      redirect_to vehicles_path
    end
  end

  private

  def vehicle_params
    params.require(:vehicle).permit(:brand_id, :model_id, :year, :chasis_number, :patent, :category_id)
  end
end
