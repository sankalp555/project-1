class ApplicationController < ActionController::Base
  protect_from_forgery
  $vapid_private = "l-X1MEzjaeK6R-75UVbECtG7L6XK73oYfwmAI1wMk0k="
  $vapid_public = "BM_vHmX4AwGUqfyezvFdez_9cZ-dqdvdYAsP_KR4w3MOsFfU5HRfm1BBQCjTzowLEpWo9jrsOikT-82f-wGpTGQ="


  def send_web_push_notification(user_id, message_body)
    subscription = User.find(user_id).web_push_subscription
    message = {
        title: "You have a notification!",
        body: "#{message_body}",
        tag: "new-Notification"
    }
    unless subscription.empty?
      Webpush.payload_send(
        message: JSON.generate(message),
        endpoint: subscription["endpoint"], 
        p256dh: subscription["keys"]["p256dh"], 
        auth: subscription["keys"]["auth"], 
        ttl: 15, 
        vapid: { 
          subject: 'mailto:admin@example.com', 
          public_key: $vapid_public,
          private_key: $vapid_private
        }
      )
    end
  end

end
