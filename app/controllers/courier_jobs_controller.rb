class CourierJobsController < ApplicationController
  layout 'private'

  def index
    if current_user.suppliers_users.present?
      # @q = current_user.suppliers_users.last.supplier.orders.ransack(params[:q])
      @courier_jobs = CourierJob.all.order(created_at: :desc).page params[:page]
    end
  end

  def new
    @order_item = OrderItem.find(params[:order_item_id])
    @courier_job = CourierJob.new
  end

  def create
    @order_item = OrderItem.find(params[:order_item_id])
    @order = @order_item.order
    @courier_job = @order_item.build_courier_job(courier_job_params)
    if @courier_job.save
      send_web_push_notification(@order.user_id, "Your Order is assigned to Courier service")
    	flash[:success] = "Servicio de mensajería enviado con éxito!"
      redirect_to order_path(@order)
    else
      errors = @courier_job.errors.full_messages.join(', ')
      flash[:error] = errors
      redirect_to order_path(@order)
    end
  end

  def edit
    @courier_job = CourierJob.find(params[:id])
  end

  def update
    @courier_job = CourierJob.find(params[:id])
    @order = @courier_job.order
    if @courier_job.update_attributes(courier_job_params)
      flash[:success] = "Actualización de trabajo de mensajería con éxito!"
      redirect_to order_path(@order)
    else
      errors = @courier_job.errors.full_messages.join(', ')
      flash[:error] = errors
      redirect_to order_path(@order)
    end
  end

  private

  def courier_job_params
    params.require(:courier_job).permit(:courier_company_id, :status, :tracking_number, :initial_date, :final_date, :message, :send_detail)
  end
end
