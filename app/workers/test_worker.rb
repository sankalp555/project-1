class TestWorker
  include Sidekiq::Worker

  def perform
    UserMailer.test_email.deliver_now
  end
end
