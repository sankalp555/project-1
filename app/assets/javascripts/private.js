$( document).ready(function() {
  $('#quote-req-modal').click(function() {
    var quote = {}
    quote['brand_id'] = $('#brand_id').val();
    quote['model_id'] = $('#moidel_id').val();
    quote['year'] = $('#year').val();
    quote['description'] = $('#description').val();
    quote['chasis_number'] = $('#chasis_number').val();
    quote['amount'] = $('#amount').val();
    quote['original'] = $('#original').val();
    quote['alternative'] = $('#alternative').val();

    $('#new_quotation_request_data').prepend($('<tr><td>'+$('#brand_id').val()+'</td><td>'+$('#moidel_id').val()+'</td><td>'+$('#year').val()+'</td><td>'+$('#description').val()+'</td><td>'+$('#chasis_number').val()+'</td><td>'+$('#amount').val()+'</td><td><button id="tbl_quotation_request_remove">Remove</button></td></tr>'));
    $('.quotation-request-hidden-form').prepend($('<div><input type="hidden" name="pieces[][model_id]" value='+$("#moidel_id").val()+'><input type="hidden" name="pieces[][year]" value='+$("#year").val()+'><input type="hidden" name="pieces[][amount]" value='+$("#amount").val()+'><input type="hidden" name="pieces[][chasis_number]" value='+$("#chasis_number").val()+'><input type="hidden" name="pieces[][description]" value='+$("#description").val()+'></div>'));
    $('#myModal').css("display", "none");
  });

  $('#closeModal').click(function(){
    $('#myModal').css("display", "none");
  });    
});