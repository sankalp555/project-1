$(document).ready(function() {
  $('.has_many_add').click(function() {
    setTimeout(
      function() 
      {
        $('#brand').change(function() {
          brand = this.value
          $.ajax({
            url: '/models',
            type: 'GET',
            data: {
              brand_id: brand
            },
            contentType: 'application/json',
            dataType: 'json',
            success: function(data) {
              $('#model').empty();
              $('#model').append("<option value=''>Modelo</option>");
              $.each(data, function(id, value) {
                $('#model').append("<option value=" + value[1] + ">" + value[0]);
              });
            }
          });
        });
      }, 1000);
  });

  $('#brand').change(function() {
    brand = this.value
    $.ajax({
      url: '/models',
      type: 'GET',
      data: {
        brand_id: brand
      },
      contentType: 'application/json',
      dataType: 'json',
      success: function(data) {
        $('#model').empty();
        $('#model').append("<option value=''>Modelo</option>");
        $.each(data, function(id, value) {
          $('#model').append("<option value=" + value[1] + ">" + value[0]);
        });
      }
    });
  });
})