// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require sweetalert2
//= require rails-ujs
//= require turbolinks
//= require bootstrap-sprockets
//= require public
//= require private
//= require js/common
//= require fancybox
//= require serviceworker-companion
//= require password_strength
//= require jquery_strength
//= require data-confirm-modal
//= require ckeditor/init
//= require jquery-ui
//= require autocomplete-rails
//= require image_processing/hammer.min.js
//= require image_processing/imgViewer.js
//= require image_processing/imgNotes.js
//= require image_processing/jquery.mousewheel
//= require image_processing/jquery.hammer.js

jQuery(document).ready(function (e) {
  function t(t) {
      e(t).bind("click", function (t) {
          t.preventDefault();
          e(this).parent().slideUp(300)
      })
  }
  e(".dropdown-toggle").click(function () {
      var t = e(this).parents(".button-dropdown").children(".dropdown-menu").is(":hidden");
      e(".button-dropdown .dropdown-menu").slideUp(300);
      e(".button-dropdown .dropdown-toggle").removeClass("active");
      if (t) {
          e(this).parents(".button-dropdown").children(".dropdown-menu").toggle(300).parents(".button-dropdown").children(".dropdown-toggle").addClass("active")
      }
  });
  e(document).bind("click", function (t) {
      var n = e(t.target);
      if (!n.parents().hasClass("button-dropdown")) e(".button-dropdown .dropdown-menu").slideUp(300);
  });
  e(document).bind("click", function (t) {
      var n = e(t.target);
      if (!n.parents().hasClass("button-dropdown")) e(".button-dropdown .dropdown-toggle").removeClass("active");
  })
});


function registerServiceWorker() {
    navigator.serviceWorker
        .register('/serviceworker.js')
        .then(registration => {
            console.log(
                "ServiceWorker registered with scope:",
                registration.scope
            );
        })
        .catch(e => console.error("ServiceWorker failed:", e));
}
if (navigator && navigator.serviceWorker) {
    registerServiceWorker();
}