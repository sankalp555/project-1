$( document).ready(function() {
  $('.faq-item').click(function function_name() {
    if($(this).find('.faq-item__body').is(':visible')){
      $(this).find('.faq-item__body').hide();
      $(this).find('.fa-minus-circle').replaceWith('<i class="fa fa-plus-circle"></i>');
    }
    else{
      $(this).find('.faq-item__body').show();
      $(this).find('.fa-plus-circle').replaceWith('<i class="fa fa-minus-circle" style= "color: orange;"></i>');
    }
  })

  var windowSize = $('.main').height();

    if (windowSize > 534) {
      $('.layout__body__content__footer').addClass('layout__body__content__footer_position');
    }

  $('#quote_param_brand_id').change(function() {

    brand = $(this).children("option:selected").val();
    $.ajax({
      url: '/models',
      type: 'GET',
      data: {
        brand_id: brand
      },
      contentType: 'application/json',
      dataType: 'json',
      success: function(data) {
        $('#quote_param_model_id').empty();
        $('#quote_param_model_id').append("<option value=''>modelo</option>");
        $.each(data, function(id, value) {
          $('#quote_param_model_id').append("<option value=" + value[1] + ">" + value[0]);
        });
      }
    });
  });

  $('#closeModal').click(function(){
    $('#myModal').css("display", "none");
  });
});