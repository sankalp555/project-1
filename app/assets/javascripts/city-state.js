$(document).ready(function() {
  $('#region').change(function() {  
    region = $(this).val();
    $.ajax({
      url: '/get_cities',
      type: 'GET',
      data: {
        region: region
      },
      contentType: 'application/json',
      dataType: 'json',
      success: function(data) {
        $('#city').empty();
        $.each(data, function(id, value) {
          $('#city').append("<option value=" + value + ">" + value);
        });
      }
    });
  });
})