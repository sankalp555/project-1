//= require jquery
//= require jquery-ui
//= require jquery_ujs
//= require sweetalert2
//= require sweet-alert2-rails
//= require image_processing/hammer.min.js
//= require image_processing/imgViewer.js
//= require image_processing/imgNotes.js
//= require image_processing/jquery.mousewheel
//= require image_processing/jquery.hammer.js
//= require active_admin/select2
var CKEDITOR_BASEPATH = '/assets/ckeditor/';

$(document).ready(function(){
  $('#category_part_item_sub_parts').click(function(){
    if ($('#category_part_item_sub_parts').is(":checked")){
      $('#category_part_item_sub_parts_input').after('\
        <li class="boolean input optional to_remove" id="sub_part_item_fornt">\
          <label for="sub_part_item_front_checked">\
            <input id="sub_part_item_front_checked" class="checked" type="checkbox" name="category_part_item[category_sub_part_item_attributes][front]">\
            Front\
          </label>\
        </li>\
        <li class="boolean input optional to_remove" id="sub_part_item_rear">\
          <label for="sub_part_item_rear_checked">\
            <input id="sub_part_item_rear_checked" class="checked" type="checkbox" name="category_part_item[category_sub_part_item_attributes][rear]">\
            Rear\
          </label>\
        </li>\
        <li class="boolean input optional to_remove" id="sub_part_item_right">\
          <label for="sub_part_item_right_checked">\
            <input id="sub_part_item_right_checked" class="checked" type="checkbox" name="category_part_item[category_sub_part_item_attributes][right]">\
            Right\
          </label>\
        </li>\
        <li class="boolean input optional to_remove" id="sub_part_item_left">\
          <label for="sub_part_item_left_checked">\
            <input id="sub_part_item_left_checked" class="checked" type="checkbox" name="category_part_item[category_sub_part_item_attributes][left]">\
            Left\
          </label>\
        </li>\
        <li class="boolean input optional to_remove" id="sub_part_item_all">\
          <label for="sub_part_item_all_checked">\
            <input id="sub_part_item_all_checked" class="all_checked" type="checkbox" name="category_part_item[category_sub_part_item_attributes][all]">\
            All\
          </label>\
        </li>\
      ')
    }else{
      $('.to_remove').remove()
    }
  })
})
$(document).ready(function(){
  if($('.all_checked').is(":checked")){
    console.log("message")
    $('#sub_part_item_left_checked').prop('checked', true)
  }else{
    //$('.checked').prop('checked', false)
  }
})
