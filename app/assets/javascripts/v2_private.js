// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require sweetalert2
//= require jquery
//= require rails-ujs
//= require bootstrap-sprockets
//= require public
//= require private
//= require data-confirm-modal
//= require js/common
//= require fancybox
//= require serviceworker-companion
//= require password_strength
//= require jquery_strength
//= require ckeditor/init
//= require jquery-ui
//= require autocomplete-rails

//= require codebase.app.min
//= require codebase.core.min

function registerServiceWorker() {
    navigator.serviceWorker
        .register('/serviceworker.js')
        .then(registration => {
            console.log(
                "ServiceWorker registered with scope:",
                registration.scope
            );
        })
        .catch(e => console.error("ServiceWorker failed:", e));
}
if (navigator && navigator.serviceWorker) {
    registerServiceWorker();
}

