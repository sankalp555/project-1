module RepliesHelper
  
  def get_unread_messages(type)
    if type=="answer"
      brands = current_user.supplier_brands
      quotations = QuotationRequest.joins(quotation_request_parts: :vehicle).where("vehicles.brand_id IN (?)", brands.pluck(:id)).distinct
      quotation_request_part_ids = QuotationRequestPart.where(quotation_request_id: quotations.pluck(:id)).pluck(:id)
      question_ids = Question.where(quotation_request_part_id: quotation_request_part_ids).pluck(:id)
      answer_ids = Reply.where(question_id: question_ids)
      @unread_messages = Notification.where(notifiable_id: answer_ids, notifiable_type: type, read: false).order(created_at: :desc)
    elsif type=="question"
      quotation_requests = current_user.quotation_requests
      quotation_request_part_ids = QuotationRequestPart.where(quotation_request_id: quotation_requests.pluck(:id)).pluck(:id)
      question_ids = Question.where(quotation_request_part_id: quotation_request_part_ids).pluck(:id)
      question_ids = Question.where(quotation_request_part_id: quotation_request_part_ids).pluck(:id)
      @unread_messages = Notification.where(notifiable_id: question_ids, notifiable_type: type, read: false).order(created_at: :desc)
    elsif type=="message"
      message_ids = Message.where(receiver_id: current_user.id, read: false)
      @unread_chat = Notification.where(notifiable_id: message_ids, notifiable_type: type, read: false).order(created_at: :desc)
    end
  end
end
