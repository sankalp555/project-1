module OrdersHelper
  def get_supplier_quotaion_part(quotation_part_id)
    quotation_part = QuotationPart.find(quotation_part_id)
    supplier_id = Quotation.where(id: quotation_part.quotation_id).pluck(:supplier_id)
    user_id = SuppliersUser.where(id: supplier_id).pluck(:user_id)
    @supplier = User.find(user_id) 
  end

  def get_address(id)
    @address = Address.where(user_id: id, primary: true) 
  end

  def get_supplier_user(id)
  	@message = Message.find(id)
    @order = Order.find(@message.order_id)
    @supplier_user = @order.order_items.last.quotation_part.quotation.user
  end

end
