module Employees::QuotationRequestsHelper
  
  def quotation_request_part_details quotation_request_id
    @quotation_request_parts = QuotationRequestPart.where(quotation_request_id: quotation_request_id)
  end
  def quotation_requests
    @quotation_requests = QuotationRequest.all
  end  
end
