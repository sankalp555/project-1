module QuestionsHelper

  def get_questions(quotation_part_id)
  	@questions = Question.where(quotation_part_id: quotation_part_id)
  end

  def get_answers(question_id)
    @answers = Reply.where(question_id: question_id)
  end
end
