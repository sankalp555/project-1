module ApplicationHelper

  def get_brand id
    @brand = Brand.find_by_id(id)
  end

  def get_model id
    @model = Model.find(id)
  end

  # def get_unread_notification(type)
  #   if type=="quotation_request"
  #     brands = current_user.supplier_brands
  #     quotations = QuotationRequest.joins(quotation_request_parts: :vehicle)
  #     .where("vehicles.brand_id IN (?)", brands.pluck(:id))
  #     .where("quotation_requests.created_at >= ?", current_user.created_at)
  #     .distinct
  #     @unread_notifiation = Notification.where(notifiable_id: quotations.pluck(:id), notifiable_type: type, read: false).order(created_at: :desc)
  #   elsif type=="quote"
  #     # quotation_requests = current_user.quotation_requests.pluck(:id)
  #     # @unread_notifiation = Notification.where(notifiable_id: quotation_requests, notifiable_type: type, read: false).order(created_at: :desc)
  #     quotation_requests = current_user.quotation_requests
  #     ids = []
  #     quotation_requests.each do |request|
  #       ids << request.quotations.pluck(:id)
  #     end
  #     @unread_quote_notifiation = Notification.where(notifiable_id: ids.flatten, notifiable_type: type, read: false).order(created_at: :desc)
  #     @unread_notifiation = @unread_quote_notifiation
  #   elsif type=="order_item"
  #     orders = current_user.orders
  #     ids = []
  #     orders.each do |order|
  #       ids << order.order_items.pluck(:id)
  #     end
  #     @unread_order_item_notification = Notification.where(notifiable_id: ids.flatten, notifiable_type: type, read: false).order(created_at: :desc)
  #     @unread_notifiation = @unread_order_item_notification
  #   end
  # end

  def get_part_details quotation_request_parts
    "#{ quotation_request_parts.first.vehicle.brand.name}/#{quotation_request_parts.first.vehicle.model.name}/#{quotation_request_parts.first.vehicle.chasis_number}/#{quotation_request_parts.first.vehicle.year}"
  end

  def get_quotes_for_request(quotation_request_part_id)
    @quotes = QuotationPart.where(quotation_request_part_id: quotation_request_part_id)
  end

  def get_received_quotes(quotation_id)
    @quotation_received = QuotationPart.where(quotation_id: quotation_id)
  end

  def check_user_notify_preference id
    @user = UserNotifyPreference.find_by_user_id(id)
    if @user.nil?
      UserNotifyPreference.create(user_id: current_user.id)
    end
  end

  def get_quotation_request(id)
    @quotation_request = QuotationRequest.find(id)
  end

  def flash_class(level)
    case level
      when 'notice' then "alert alert-info"
      when 'success' then "alert alert-success"
      when 'error' then "alert alert-danger"
      when 'alert' then "alert alert-warning"
    end
  end

  def bootstrap_class_for flash_type
    case flash_type
      when "delivered", "approved"
        "success"
      when "cancelled"
        "danger"
      when "confirmation_pending", "quoted"
        "info"
      when "pending", "in_process"
        "warning"
      else
        flash_type.to_s
    end
  end

  def order_status_for status
    case status
      when "Completed", "approved"
        "check"
      when "Cancelled", "Failure"
        "times"
      when "Confirmation Pending"
        "spinner"
      else
        "times"
    end
  end

  def get_time_lable(time)
    return "Todas" if time.blank? || time == "all"
    return  "Hoy" if time == "today"
    return  "Esta semana" if time == "week"
    return  "Este Mes" if time == "month"
    return  "Este Año" if time == "year"
  end

  def get_status_lable(status)
    return "Todas" if status.blank? || status == "all"
    return "Pendientes" if ["pending", "confirmation_pending"].include?(status)
    return "En Proceso" if status == "in_process"
    return "Aprobado" if status == "approved"
    return "En proceso" if status == "in_process"
    return "Entregado" if status == "delivered"
  end
  
  def translate_for(status)
    #quotation status
    return "Pendiente" if status == "pending"
    return "En Aprobación" if status == "approved"
    return "En Proceso" if status == "in_process"
    return "Cancelada" if status == "cancelled"
    return "Citado" if status == "quoted"
    #order status
    return "Confirmacion pendiente" if status == "confirmation_pending"
    return "En proceso" if status == "in_process"
    return "Entregado" if status == "delivered"
    return "En Aprobación" if status == "approved"
  end

  def catalog_image(type)
    if type.nil? || type == ''
      image = 'all_catalog.png'
    end
    if type == "0"
      image = 'car_icon.png' 
    end
    if type == "1" 
      image = 'commercial_vehicle_icon.png' 
    end
    if type == "2"
      image = 'motorbike.png' 
    end
    return image
  end
end


