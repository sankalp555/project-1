module CartsHelper
  def get_quotation_part_details id
    @quotation_details = QuotationPart.find(id) 
  end
  def get_quotation_request_part id
    @quotation_request_part = QuotationRequestPart.find(id)
  end
  def user_cart_items
  	@cart_items = CartItem.where(cart_id: current_user.cart_id)
  end
end
