class UserMailer < ApplicationMailer
  def test_email 
    mail to: "mario@requies.cl", subject: "This is a test email"
  end

  def new_supplier_user(user, token)
    @user = user
    @token = token
    mail to: @user.email, subject: "Bienvenido a InParts!"
  end

  def courier_job(user, order_item_id)
    @user = user
    @order_item = OrderItem.find(order_item_id)
    @courier_job = @order_item.courier_job
    mail to: @user.email, subject: "El pedido se envía a la empresa dispach!"
  end

  def referral_amount_added(user, referral)
    @user = user
    @referral = referral
    mail to: @user.email, subject: "Las bonificaciones por recomendación se han acreditado en su cuenta!"
  end

  def coupon_assigned(user, coupon)
    @user = user
    @coupon = coupon
    mail to: @user.email, subject: "Se le ha asignado un nuevo cupón."
  end

  def send_new_user_message(user)
    @user = user
    mail to: @user.email, subject: "Bienvenido a InParts!"
  end

  def send_delivery_confirmation(item_id, user_id)
    @order_item = OrderItem.find(item_id)
    @user = User.find(user_id)
    mail to: @user.email, subject: "Su artículo ha sido entregado por favor apruebe desde el enlace dado."
  end
end
