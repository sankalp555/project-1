class ReplyMailer < ApplicationMailer
  
  def reply_to_contact(email, reply)
    @email = email
    @reply = reply
    mail to: email, subject: "Reply for your information by INPARTS!"
  end

end