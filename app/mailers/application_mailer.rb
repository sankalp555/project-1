class ApplicationMailer < ActionMailer::Base
  default from: 'soporte@inparts.cl'
  layout 'mailer'
end
