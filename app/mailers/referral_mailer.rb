class ReferralMailer < ApplicationMailer

  def new_referral_user(data)
    @data = data
    email = @data.refer_to
    @current_user = User.find(@data.user_id)
    mail to: email, subject: "You have been refer by your friend "+@current_user.name
  end

end