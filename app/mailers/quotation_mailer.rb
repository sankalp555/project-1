class QuotationMailer < ApplicationMailer

  def send_mail_supplier_quotation_request(quotation)
    @sub_part = ApiSubPart.find(quotation.api_sub_part_id)
    @api_part = @sub_part.api_part
    @part_catalog = PartCatalog.find(@api_part.part_catalog_id) 
    @model_name = @part_catalog.api_vehicle_model.api_vehicle.api_sub_model.name + '' +  @part_catalog.api_vehicle_model.api_vehicle.api_sub_model.api_model.name
    @api_catalog = ApiCatalog.find(quotation.api_vehicle.api_sub_model.api_model.api_catalog_id)
    supplier_ids = ApiCatalogsSupplier.where(part_catalog_id: @part_catalog.id).pluck(:supplier_id)
    supplier_users = SuppliersUser.where(supplier_id: supplier_ids).pluck(:user_id)
    suppliers = User.where(id: supplier_users).pluck(:email)
    suppliers.each do |supplier|
      @email = supplier
      @quotation = quotation
      mail to: @email, subject: "You have recive one quotation request!"
    end
  end

  def send_mail_buyer_quotes_created(quote)
    quotation_request_id = Quotation.where(id: quote.quotation_id).pluck(:quotation_request_id)
    user_id = QuotationRequest.where(id: quotation_request_id).pluck(:user_id)
    @user_email = User.where(id: user_id).pluck(:email)
    @quote_got = quote
    @quote_requested = QuotationRequestPart.find(quote.quotation_request_part_id)
    @model = @quote_requested.vehicle.model
    @brand = @quote_requested.vehicle.brand
    mail to: @user_email, subject: "You have got reply for your quotation request!"
  end

  def quote_approved(quote)
    supplier_id = Quotation.where(id: quote.quotation_id).pluck(:supplier_id)
    user_id = SuppliersUser.where(supplier_id: supplier_id).pluck(:user_id)
    @supplier_emails = User.where(id: user_id).pluck(:email)
    @quote = quote
    mail to: @supplier_email, subject: "Your quotes approved by user!"
  end

  def quotes_sold(order)
    order_items = OrderItem.where(order_id: order.id)
    order_items.each do |order_item|
      @supplier = order_item.quotation_part.quotation.user
      @supplier_email = @supplier.email
      @quotation_part = QuotationPart.find(order_item.quotation_part_id)
      mail to: @supplier_email, subject: "Your quotaion has been sold!"
    end
  end

  def quote_accepted_mail_to_supplier(id)
    @order_item = OrderItem.find(id)
    @quotation_part = @order_item.quotation_part
    @user = @quotation_part.quotation.user
    mail to: @user.email, subject: "Su cotización ha sido aceptada!"
  end
end