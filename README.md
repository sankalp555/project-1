# Repuestos

### Development

First copy the `.env.example`  into your own `.env` file. It should look like this

```
APP_PORT=9096
SECRET_KEY_BASE=asdasdsa123123123sadasdsa
POSTGRES_HOST=database
POSTGRES_PORT=5432
POSTGRES_USER=postgres
POSTGRES_PASSWORD=
POSTGRES_DB=repuestos
DB_HOST_PATH=./tmp/database
COMPOSE_FILE=docker-compose.dev.yml
```

Then run `docker-compose up -d` and from there you should do some stuff from the `app` container.

```
docker-compose exec app bash
bundle install 
yarn install
```

Now you could start the server with `rails s -b 0.0.0.0`
