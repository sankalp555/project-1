FROM ruby:2.3.7

RUN apt-get update -qq && \
    apt-get install -y build-essential postgresql-client-9.6 libpq-dev && \
    curl -sL https://deb.nodesource.com/setup_8.x | bash - && \
    apt-get install -y nodejs && \
    gem install bundler && \
    npm install -g yarn

ENV RAILS_ENV production
ENV SECRET_KEY_BASE some-secret-key-to-compile-assets
ENV RAILS_ROOT /app
RUN mkdir -p $RAILS_ROOT
WORKDIR $RAILS_ROOT

ADD Gemfile Gemfile
ADD Gemfile.lock Gemfile.lock

RUN bundle install --without development test

COPY . .

RUN yarn install
RUN bundle exec rails webpacker:compile
RUN bundle exec rails assets:precompile

CMD puma -C config/puma.rb
