# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password')

brnad = Brand.create(name: 'BMW')
brnad.models.new(name: 'X2')

brnad = Brand.create(name: 'Mercedez')
brnad.models.new(name: 'S-Class')

brnad = Brand.create(name: 'Audi')
brnad.models.new(name: 'A3')

user = User.create(email: 'test_user@gmail.com', password: '123456')
suplier = user.suppliers.new(name: 'Test', address: 'test address', phone: '9874563214')
supplier.supliers_users.new(role: 'Ejecutivo', user_id: user.id)