# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20200124114454) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: :cascade do |t|
    t.string "namespace"
    t.text "body"
    t.string "resource_type"
    t.bigint "resource_id"
    t.string "author_type"
    t.bigint "author_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id"
    t.index ["namespace"], name: "index_active_admin_comments_on_namespace"
    t.index ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id"
  end

  create_table "addresses", force: :cascade do |t|
    t.string "name"
    t.string "street_address"
    t.integer "number"
    t.string "region"
    t.string "city"
    t.string "comuna"
    t.bigint "addressable_id"
    t.boolean "primary", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "phone"
    t.string "addressable_type"
    t.index ["addressable_id"], name: "index_addresses_on_addressable_id"
  end

  create_table "admin_users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_admin_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true
  end

  create_table "api_brands", force: :cascade do |t|
    t.string "name"
    t.integer "brand_code"
    t.string "logo"
    t.string "logo_string"
    t.string "supportframesearch"
    t.string "frameexample"
    t.string "supportvinsearch"
    t.string "vinexample"
    t.string "universal_classifier"
    t.string "type_moto"
    t.string "type_auto"
    t.string "type_transport"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "api_catalogs", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.string "code"
    t.string "logo"
    t.string "small_logo"
    t.integer "vehicle_type"
    t.boolean "active", default: true
  end

  create_table "api_catalogs_suppliers", id: false, force: :cascade do |t|
    t.bigint "supplier_id", null: false
    t.integer "part_catalog_id"
  end

  create_table "api_models", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "api_catalog_id"
    t.string "logo"
    t.boolean "active", default: true
  end

  create_table "api_parts", force: :cascade do |t|
    t.string "name"
    t.string "group_name"
    t.string "logo"
    t.string "link"
    t.string "next"
    t.integer "part_catalog_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "internal_parts"
    t.boolean "active", default: true
  end

  create_table "api_sub_models", force: :cascade do |t|
    t.bigint "api_model_id"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "logo"
    t.index ["api_model_id"], name: "index_api_sub_models_on_api_model_id"
  end

  create_table "api_sub_parts", force: :cascade do |t|
    t.string "number"
    t.string "code"
    t.string "name"
    t.integer "quantity"
    t.string "node_link"
    t.string "parent_group_name"
    t.integer "api_part_id"
    t.jsonb "additional_info", default: {}
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "logo"
  end

  create_table "api_vehicle_models", force: :cascade do |t|
    t.integer "api_vehicle_id"
    t.string "release_date"
    t.string "market"
    t.string "engine"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "link"
    t.jsonb "additional_info", default: {}
  end

  create_table "api_vehicles", force: :cascade do |t|
    t.integer "api_sub_model_id"
    t.string "ssd"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "vin"
    t.string "frame"
    t.integer "api_catalog_id"
    t.integer "user_id"
    t.string "plate_no"
  end

  create_table "attachments", force: :cascade do |t|
    t.string "attachment"
    t.integer "attached_item_id"
    t.string "attached_item_type"
    t.string "name"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "description"
    t.integer "model_id"
    t.integer "brand_id"
    t.integer "from_year"
    t.integer "to_year"
    t.index ["user_id"], name: "index_attachments_on_user_id"
  end

  create_table "brands", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "category_id"
    t.index ["category_id"], name: "index_brands_on_category_id"
  end

  create_table "brands_suppliers", id: false, force: :cascade do |t|
    t.bigint "supplier_id", null: false
    t.bigint "brand_id", null: false
    t.bigint "category_part_id"
    t.index ["category_part_id"], name: "index_brands_suppliers_on_category_part_id"
  end

  create_table "cart_items", force: :cascade do |t|
    t.bigint "quotation_part_id"
    t.bigint "cart_id"
    t.integer "order_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "quantity"
    t.string "delivery_type"
    t.index ["cart_id"], name: "index_cart_items_on_cart_id"
    t.index ["order_id"], name: "index_cart_items_on_order_id"
    t.index ["quotation_part_id"], name: "index_cart_items_on_quotation_part_id"
  end

  create_table "carts", force: :cascade do |t|
    t.float "total_amount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "coupon_id"
    t.boolean "coupon_applied", default: false
  end

  create_table "catalog_types", force: :cascade do |t|
    t.string "name"
    t.string "catalog_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.boolean "active", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "categories_suppliers", id: false, force: :cascade do |t|
    t.bigint "supplier_id", null: false
    t.bigint "category_id", null: false
    t.index ["supplier_id", "category_id"], name: "index_categories_suppliers_on_supplier_id_and_category_id"
  end

  create_table "category_part_items", force: :cascade do |t|
    t.string "name"
    t.boolean "active", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "item_type_id"
    t.decimal "price"
    t.index ["item_type_id"], name: "index_category_part_items_on_item_type_id"
  end

  create_table "category_parts", force: :cascade do |t|
    t.string "name"
    t.boolean "active", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "category_sub_options", force: :cascade do |t|
    t.string "name"
    t.boolean "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "category_sub_part_items", force: :cascade do |t|
    t.bigint "category_part_item_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "category_sub_option_id"
    t.index ["category_part_item_id"], name: "index_category_sub_part_items_on_category_part_item_id"
    t.index ["category_sub_option_id"], name: "index_category_sub_part_items_on_category_sub_option_id"
  end

  create_table "contacts", force: :cascade do |t|
    t.string "full_name"
    t.string "email"
    t.string "brand"
    t.text "comment"
    t.string "company"
    t.string "phone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "original"
    t.boolean "alternative"
    t.string "reason"
  end

  create_table "coupons", force: :cascade do |t|
    t.string "code"
    t.decimal "discount", default: "0.0"
    t.boolean "active", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "discount_type"
  end

  create_table "courier_companies", force: :cascade do |t|
    t.string "name"
    t.boolean "active", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "courier_jobs", force: :cascade do |t|
    t.bigint "order_id"
    t.bigint "courier_company_id"
    t.string "tracking_number"
    t.date "initial_date"
    t.date "final_date"
    t.text "message"
    t.string "status"
    t.boolean "send_detail", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "order_item_id"
    t.index ["courier_company_id"], name: "index_courier_jobs_on_courier_company_id"
    t.index ["order_id"], name: "index_courier_jobs_on_order_id"
  end

  create_table "employees", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.boolean "privilege", default: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_employees_on_email", unique: true
    t.index ["reset_password_token"], name: "index_employees_on_reset_password_token", unique: true
  end

  create_table "hot_spots", force: :cascade do |t|
    t.float "x_position"
    t.float "y_position"
    t.text "note"
    t.bigint "attachment_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "hot_spot_no"
    t.integer "api_sub_part_id"
    t.integer "api_part_id"
    t.index ["attachment_id"], name: "index_hot_spots_on_attachment_id"
  end

  create_table "item_types", force: :cascade do |t|
    t.string "name"
    t.boolean "active", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "category_part_id"
    t.boolean "hot_spot_no", default: true
    t.index ["category_part_id"], name: "index_item_types_on_category_part_id"
  end

  create_table "markups", force: :cascade do |t|
    t.bigint "supplier_id"
    t.decimal "input"
    t.string "role"
    t.boolean "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["supplier_id"], name: "index_markups_on_supplier_id"
  end

  create_table "messages", force: :cascade do |t|
    t.text "message"
    t.bigint "quotation_request_id"
    t.integer "sender_id"
    t.boolean "read", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "receiver_id"
    t.datetime "sent_at"
    t.integer "order_id"
    t.index ["quotation_request_id"], name: "index_messages_on_quotation_request_id"
  end

  create_table "models", force: :cascade do |t|
    t.string "name"
    t.bigint "brand_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["brand_id"], name: "index_models_on_brand_id"
  end

  create_table "notices", force: :cascade do |t|
    t.string "title"
    t.text "discription"
    t.boolean "publish"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "notice_type"
    t.string "page"
  end

  create_table "notifications", force: :cascade do |t|
    t.boolean "read", default: false
    t.string "notifiable_type"
    t.bigint "notifiable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["notifiable_type", "notifiable_id"], name: "index_notifications_on_notifiable_type_and_notifiable_id"
  end

  create_table "order_items", force: :cascade do |t|
    t.bigint "quotation_part_id"
    t.bigint "order_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "quantity"
    t.string "delivery_type"
    t.string "status"
    t.integer "supplier_id"
    t.index ["order_id"], name: "index_order_items_on_order_id"
    t.index ["quotation_part_id"], name: "index_order_items_on_quotation_part_id"
  end

  create_table "orders", force: :cascade do |t|
    t.bigint "user_id"
    t.string "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "notification_params"
    t.string "transaction_id"
    t.datetime "purchased_at"
    t.decimal "total_pay_amount"
    t.string "delivery_status"
    t.string "payment_type"
    t.bigint "supplier_id"
    t.integer "coupon_id"
    t.string "reference_number"
    t.index ["supplier_id"], name: "index_orders_on_supplier_id"
    t.index ["user_id"], name: "index_orders_on_user_id"
  end

  create_table "part_catalogs", force: :cascade do |t|
    t.string "name"
    t.string "group_name"
    t.string "logo"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "link"
    t.integer "api_vehicle_model_id"
    t.integer "api_catalog_id"
    t.string "next"
    t.boolean "active", default: true
  end

  create_table "payments", force: :cascade do |t|
    t.decimal "total_amount", default: "0.0"
    t.integer "order_id"
    t.string "payment_gateway"
    t.string "payment_type"
    t.string "authorization_code"
    t.string "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "privacies", force: :cascade do |t|
    t.string "title"
    t.text "content"
    t.datetime "updated_on"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "questions", force: :cascade do |t|
    t.text "question"
    t.boolean "read", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "quotation_request_part_id"
    t.index ["quotation_request_part_id"], name: "index_questions_on_quotation_request_part_id"
  end

  create_table "quotation_parts", force: :cascade do |t|
    t.bigint "quotation_id"
    t.bigint "quotation_request_part_id"
    t.decimal "price"
    t.integer "amount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "warranty"
    t.integer "discount"
    t.datetime "shipping"
    t.boolean "accepted", default: false
    t.string "type_of_request", default: "original"
    t.string "shipping_type", default: "deliver"
    t.decimal "shipping_price"
    t.string "status", default: "pending"
    t.string "brand"
    t.decimal "selling_price"
    t.index ["quotation_id"], name: "index_quotation_parts_on_quotation_id"
    t.index ["quotation_request_part_id"], name: "index_quotation_parts_on_quotation_request_part_id"
  end

  create_table "quotation_request_parts", force: :cascade do |t|
    t.bigint "quotation_request_id"
    t.string "description"
    t.integer "amount"
    t.boolean "original"
    t.boolean "alternative"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "api_vehicle_id"
    t.integer "api_sub_part_id"
    t.integer "api_sub_model_id"
    t.integer "part_catalog_id"
    t.index ["quotation_request_id"], name: "index_quotation_request_parts_on_quotation_request_id"
  end

  create_table "quotation_requests", force: :cascade do |t|
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "status", default: "pending"
    t.index ["user_id"], name: "index_quotation_requests_on_user_id"
  end

  create_table "quotations", force: :cascade do |t|
    t.bigint "quotation_request_id"
    t.bigint "supplier_id"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "flag", default: false
    t.index ["quotation_request_id"], name: "index_quotations_on_quotation_request_id"
    t.index ["supplier_id"], name: "index_quotations_on_supplier_id"
    t.index ["user_id"], name: "index_quotations_on_user_id"
  end

  create_table "referral_instructions", force: :cascade do |t|
    t.integer "referral_max_amount"
    t.integer "amount_to_be_awarded_per_refer"
    t.text "refer_description"
    t.string "referral_type"
    t.integer "expire_in_days"
    t.integer "max_no_of_refer"
    t.boolean "active", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "referral_links", force: :cascade do |t|
    t.bigint "referral_instruction_id"
    t.bigint "user_id"
    t.string "refer_to"
    t.string "refer_code"
    t.boolean "used", default: false
    t.boolean "awarded", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["referral_instruction_id"], name: "index_referral_links_on_referral_instruction_id"
    t.index ["user_id"], name: "index_referral_links_on_user_id"
  end

  create_table "referral_users", force: :cascade do |t|
    t.bigint "referral_link_id"
    t.integer "referral_user_id"
    t.boolean "purchase", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["referral_link_id"], name: "index_referral_users_on_referral_link_id"
  end

  create_table "replies", force: :cascade do |t|
    t.bigint "question_id"
    t.text "reply"
    t.boolean "read", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["question_id"], name: "index_replies_on_question_id"
  end

  create_table "reply_notifications", force: :cascade do |t|
    t.text "reply"
    t.boolean "read", default: false
    t.string "replyable_type"
    t.bigint "replyable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["replyable_type", "replyable_id"], name: "index_reply_notifications_on_replyable_type_and_replyable_id"
  end

  create_table "spot_numbers", force: :cascade do |t|
    t.string "name"
    t.string "top"
    t.string "left"
    t.string "width"
    t.string "height"
    t.integer "api_part_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "suppliers", force: :cascade do |t|
    t.string "name"
    t.string "phone"
    t.string "url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "active", default: false
    t.string "delivery_option"
  end

  create_table "suppliers_users", force: :cascade do |t|
    t.bigint "supplier_id"
    t.bigint "user_id"
    t.string "role"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["supplier_id"], name: "index_suppliers_users_on_supplier_id"
    t.index ["user_id"], name: "index_suppliers_users_on_user_id"
  end

  create_table "taxes", force: :cascade do |t|
    t.string "name"
    t.integer "year"
    t.decimal "percent"
    t.integer "upper_amount"
    t.integer "lower_amount"
    t.boolean "active", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "terms_and_conditions", force: :cascade do |t|
    t.string "title"
    t.text "content"
    t.datetime "updated_on"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_coupons", force: :cascade do |t|
    t.integer "user_id"
    t.integer "coupon_id"
    t.boolean "applied", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_notify_preferences", force: :cascade do |t|
    t.bigint "user_id"
    t.boolean "new_conf_shipments", default: false
    t.boolean "promo_discount", default: false
    t.boolean "payment", default: false
    t.boolean "dispatch_delivery", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_user_notify_preferences_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.string "phone"
    t.datetime "deleted_at"
    t.bigint "cart_id"
    t.boolean "status", default: true
    t.date "dob"
    t.string "password_salt"
    t.string "cellular"
    t.string "office"
    t.json "web_push_subscription", default: {}
    t.decimal "total_purchasing", default: "0.0"
    t.float "reedim_amount", default: 0.0
    t.string "last_name"
    t.index ["cart_id"], name: "index_users_on_cart_id"
    t.index ["deleted_at"], name: "index_users_on_deleted_at"
    t.index ["email"], name: "index_users_on_email"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "vehicles", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "brand_id"
    t.bigint "model_id"
    t.string "year"
    t.string "chasis_number"
    t.string "patent"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "category_id"
    t.index ["brand_id"], name: "index_vehicles_on_brand_id"
    t.index ["category_id"], name: "index_vehicles_on_category_id"
    t.index ["model_id"], name: "index_vehicles_on_model_id"
    t.index ["user_id"], name: "index_vehicles_on_user_id"
  end

  add_foreign_key "api_sub_models", "api_models"
  add_foreign_key "messages", "quotation_requests"
  add_foreign_key "models", "brands"
  add_foreign_key "quotation_parts", "quotation_request_parts"
  add_foreign_key "quotation_parts", "quotations"
  add_foreign_key "quotation_request_parts", "quotation_requests"
  add_foreign_key "quotation_requests", "users"
  add_foreign_key "quotations", "quotation_requests"
  add_foreign_key "quotations", "suppliers"
  add_foreign_key "quotations", "users"
  add_foreign_key "suppliers_users", "suppliers"
  add_foreign_key "suppliers_users", "users"
  add_foreign_key "vehicles", "brands"
  add_foreign_key "vehicles", "models"
  add_foreign_key "vehicles", "users"
end
