class CreateApiParts < ActiveRecord::Migration[5.1]
  def change
    create_table :api_parts do |t|
    	t.string :name
      t.string :group_name
      t.string :logo
      t.string :link
      t.string :next
      t.integer :part_catalog_id

      t.timestamps
    end
  end
end
