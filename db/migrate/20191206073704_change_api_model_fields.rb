class ChangeApiModelFields < ActiveRecord::Migration[5.1]
  def change
    remove_column :api_models, :api_brand_id, :integer
    add_column :api_models, :api_catalog_id, :integer, index: true
  end
end
