class CreateMyReplies < ActiveRecord::Migration[5.1]
  def change
    create_table :my_replies do |t|
      t.references :my_question, index: true
      t.text :reply
      t.boolean :read, default: false
      t.timestamps
    end
  end
end
