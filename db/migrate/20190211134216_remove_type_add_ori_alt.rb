class RemoveTypeAddOriAlt < ActiveRecord::Migration[5.1]
  def change
  	remove_column :contacts, :type_of_product
  	add_column :contacts, :original, :boolean
  	add_column :contacts, :alternative, :boolean
  end
end
