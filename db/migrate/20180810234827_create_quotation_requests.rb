class CreateQuotationRequests < ActiveRecord::Migration[5.1]
  def change
    create_table :quotation_requests do |t|
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
