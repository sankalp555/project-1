class AddTypeToCategoryPartItems < ActiveRecord::Migration[5.1]
  def change
  	add_column :category_part_items, :item_type, :string
  end
end
