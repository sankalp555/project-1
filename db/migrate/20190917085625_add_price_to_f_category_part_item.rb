class AddPriceToFCategoryPartItem < ActiveRecord::Migration[5.1]
  def change
  	 add_column :category_part_items, :price, :decimal
  end
end
