class RemoveColumnsFromQuotationParts < ActiveRecord::Migration[5.1]
  def change
  	remove_column :quotation_parts, :availability, :boolean
  end
end
