class AddJsonToVehicleModels < ActiveRecord::Migration[5.1]
  def change
  	add_column :api_vehicle_models, :additional_info, :json, default: {}
  	add_column :part_catalogs, :next, :string
  end
end
