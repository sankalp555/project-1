class CreateCoupons < ActiveRecord::Migration[5.1]
  def change
    create_table :coupons do |t|
    	t.string :code
    	t.decimal :discount, default: 0.0
    	t.boolean :active, default: false
    	
      t.timestamps
    end
  end
end
