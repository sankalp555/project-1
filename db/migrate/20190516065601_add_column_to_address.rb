class AddColumnToAddress < ActiveRecord::Migration[5.1]
  def change
    add_column :addresses, :addressable_type, :string
    rename_column :addresses, :user_id, :addressable_id
    remove_column :suppliers, :address, :string
  end
end
