class CreateTaxes < ActiveRecord::Migration[5.1]
  def change
    create_table :taxes do |t|
      t.string :name
      t.integer :year
      t.decimal :percent
      t.integer :upper_amount
      t.integer :lower_amount
      t.boolean :active, default: false

      t.timestamps
    end
  end
end
