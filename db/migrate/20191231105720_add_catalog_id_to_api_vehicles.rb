class AddCatalogIdToApiVehicles < ActiveRecord::Migration[5.1]
  def change
  	add_column :api_vehicles, :api_catalog_id, :integer
  end
end
