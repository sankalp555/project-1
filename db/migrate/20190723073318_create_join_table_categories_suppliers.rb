class CreateJoinTableCategoriesSuppliers < ActiveRecord::Migration[5.1]
  def change
  	create_join_table :suppliers, :categories do |t|
      t.index [:supplier_id, :category_id]
      # t.index [:brand_id, :supplier_id]
    end
  end
end
