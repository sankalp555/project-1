class AddLogoToApiModels < ActiveRecord::Migration[5.1]
  def change
  	add_column :api_models, :logo, :string
  end
end
