class AddQuotationPartsDetails < ActiveRecord::Migration[5.1]
  def change
  	add_column :quotation_parts, :original, :boolean
  	add_column :quotation_parts, :alternative, :boolean
  	add_column :quotation_parts, :availability, :boolean
  	add_column :quotation_parts, :warranty, :string
  	add_column :quotation_parts, :discount, :integer
  	add_column :quotation_parts, :shipping, :datetime
  end
end
