class AddFromToYearToAttachments < ActiveRecord::Migration[5.1]
  def change
  	remove_column :attachments, :year, :integer
  	add_column :attachments, :from_year, :integer
  	add_column :attachments, :to_year, :integer
  end
end
