class AddDeliveryStatusPayType < ActiveRecord::Migration[5.1]
  def change
  	add_column :orders, :delivery_status, :string
  	add_column :orders, :payment_type, :string
  end
end
