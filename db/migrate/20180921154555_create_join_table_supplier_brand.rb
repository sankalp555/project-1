class CreateJoinTableSupplierBrand < ActiveRecord::Migration[5.1]
  def change
    create_join_table :suppliers, :brands do |t|
      # t.index [:supplier_id, :brand_id]
      # t.index [:brand_id, :supplier_id]
    end
  end
end
