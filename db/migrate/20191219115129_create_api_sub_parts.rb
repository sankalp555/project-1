class CreateApiSubParts < ActiveRecord::Migration[5.1]
  def change
    create_table :api_sub_parts do |t|
      t.string :number
      t.string :code
      t.string :name
      t.integer :quantity
      t.string :node_link
      t.string :parent_group_name
      t.integer :api_part_id
      t.jsonb :additional_info, default: {}

      t.timestamps
    end
  end
end