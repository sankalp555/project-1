class CreateQuotationRequestParts < ActiveRecord::Migration[5.1]
  def change
    create_table :quotation_request_parts do |t|
      t.references :quotation_request, foreign_key: true
      t.references :model, foreign_key: true
      t.integer :year
      t.string :description
      t.string :chasis_number
      t.integer :amount
      t.boolean :original
      t.boolean :alternative

      t.timestamps
    end
  end
end
