class AddColumnToSuppliers < ActiveRecord::Migration[5.1]
  def change
    add_column :suppliers, :delivery_option, :string
  end
end
