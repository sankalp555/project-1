class CreateSuppliersUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :suppliers_users do |t|
      t.references :supplier, foreign_key: true
      t.references :user, foreign_key: true
      t.string :role

      t.timestamps
    end
  end
end
