class AddDeliveryTypeToOrderItem < ActiveRecord::Migration[5.1]
  def change
  	add_column :order_items, :delivery_type, :string
  	remove_column :orders, :delivery_type, :string
  end
end
