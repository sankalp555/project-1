class AddSubpartsToApiParts < ActiveRecord::Migration[5.1]
  def change
    add_column :api_parts, :internal_parts, :text
  end
end
