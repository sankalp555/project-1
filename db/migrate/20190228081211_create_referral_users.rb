class CreateReferralUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :referral_users do |t|
      t.references :referral_link, index: true
      t.integer :referral_user_id
      t.boolean :purchase, default: false
      t.timestamps
    end
  end
end
