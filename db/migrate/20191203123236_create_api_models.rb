class CreateApiModels < ActiveRecord::Migration[5.1]
  def change
    create_table :api_models do |t|
      t.string :name
      t.references :api_brand, foreign_key: true
      t.timestamps
    end
  end
end
