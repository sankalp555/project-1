class AddCouponApplyToCart < ActiveRecord::Migration[5.1]
  def change
  	add_column :carts, :coupon_applied, :boolean, default: :false
  end
end
