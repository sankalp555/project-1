class AddSellingPriceQuotationPart < ActiveRecord::Migration[5.1]
  def change
    add_column :quotation_parts, :selling_price, :decimal
  end
end
