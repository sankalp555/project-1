class CreateApiCatalogs < ActiveRecord::Migration[5.1]
  def change
    create_table :api_catalogs do |t|
      t.string :catalog_type_id
      t.string :catalog_type_name
      t.timestamps
    end
  end
end
