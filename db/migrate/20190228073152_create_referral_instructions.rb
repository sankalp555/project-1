class CreateReferralInstructions < ActiveRecord::Migration[5.1]
  def change
    create_table :referral_instructions do |t|
      t.integer :referral_max_amount
      t.integer :amount_to_be_awarded_per_refer
      t.text :refer_description
      t.string :referral_type
      t.integer :expire_in_days
      t.integer :max_no_of_refer
      t.boolean :active, default: false
      t.timestamps
    end
  end
end
