class AddFieldsToCatalogs < ActiveRecord::Migration[5.1]
  def change
    add_column :api_catalogs, :name, :string
    add_column :api_catalogs, :code, :string
    add_column :api_catalogs, :logo, :string
    add_column :api_catalogs, :small_logo, :string
    remove_column :api_catalogs, :catalog_type_id, :string
    remove_column :api_catalogs, :catalog_type_name, :string
  end
end
