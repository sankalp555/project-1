class Remove < ActiveRecord::Migration[5.1]
  def change
  	remove_index :orders, :quotation_part_id
  	remove_column :orders, :quotation_part_id
  	remove_column :orders, :quantity, :integer
  	remove_column :orders, :price, :decimal
  end
end
