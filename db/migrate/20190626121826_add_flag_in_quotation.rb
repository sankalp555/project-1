class AddFlagInQuotation < ActiveRecord::Migration[5.1]
  def change
  	add_column :quotations, :flag, :boolean, default: false
  end
end
