class AddAcceptedQuotationParts < ActiveRecord::Migration[5.1]
  def change
    add_column :quotation_parts, :accepted, :boolean, default: false
  end
end
