class AddUserToVehicles < ActiveRecord::Migration[5.1]
  def change
    add_column :api_vehicles, :user_id, :integer
    add_column :api_vehicles, :plate_no, :string
  end
end
