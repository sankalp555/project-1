class RenameTableNameForQuestionAndReply < ActiveRecord::Migration[5.1]
  def change
    rename_column :my_replies, :my_question_id, :question_id
    rename_table :my_questions, :questions
    rename_table :my_replies, :replies
  end
end
