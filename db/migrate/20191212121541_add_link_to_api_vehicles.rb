class AddLinkToApiVehicles < ActiveRecord::Migration[5.1]
  def change
    add_column :api_vehicle_models, :link, :string
  end
end
