class AddSubOptionIdToRequestPart < ActiveRecord::Migration[5.1]
  def change
  	add_column :quotation_request_parts, :sub_option_id, :integer
  end
end
