class AddTypeToApiCatalog < ActiveRecord::Migration[5.1]
  def change
    add_column :api_catalogs, :vehicle_type, :integer
  end
end
