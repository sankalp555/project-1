class AddTotalAmountPayOrders < ActiveRecord::Migration[5.1]
  def change
  	add_column :orders, :total_pay_amount, :decimal
  end
end
