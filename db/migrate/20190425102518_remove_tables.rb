class RemoveTables < ActiveRecord::Migration[5.1]
  def change
    drop_table :notification_users
    drop_table :notification_data
  end
end
