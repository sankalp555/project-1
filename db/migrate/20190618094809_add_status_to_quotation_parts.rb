class AddStatusToQuotationParts < ActiveRecord::Migration[5.1]
  def change
  	add_column :quotation_parts, :status, :string, default: "pending"
  end
end
