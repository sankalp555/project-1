class CreateTermsAndConditions < ActiveRecord::Migration[5.1]
  def change
    create_table :terms_and_conditions do |t|

      t.string :title
      t.text :content
      t.datetime :updated_on
      t.timestamps
    end
  end
end
