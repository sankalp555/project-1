class SetDefaultValForCategorySub < ActiveRecord::Migration[5.1]
  def change
    change_column :category_part_items, :sub_parts, :boolean, default: false
    change_column :category_sub_part_items, :front, :boolean, default: false
    change_column :category_sub_part_items, :rear, :boolean, default: false
    change_column :category_sub_part_items, :right, :boolean, default: false
    change_column :category_sub_part_items, :left, :boolean, default: false
    change_column :category_sub_part_items, :all, :boolean, default: false
  end
end
