class AddColumnToCourierJob < ActiveRecord::Migration[5.1]
  def change
    add_column :courier_jobs, :order_item_id, :integer
  end
end
