class AddColumnTypeToCoupon < ActiveRecord::Migration[5.1]
  def change
    add_column :coupons, :discount_type, :string
    add_column :carts, :coupon_id, :integer
    add_column :orders, :coupon_id, :integer
  end
end
