class RemoveCategoryPartFromCategoryPartItem < ActiveRecord::Migration[5.1]
  def change
  	remove_column :category_part_items, :category_part_id, :integer
  end
end
