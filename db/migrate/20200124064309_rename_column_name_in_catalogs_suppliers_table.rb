class RenameColumnNameInCatalogsSuppliersTable < ActiveRecord::Migration[5.1]
  def change
    rename_column :api_catalogs_suppliers, :api_catalog_id, :part_catalog_id
  end
end
