class AddApiSubPartToHotspots < ActiveRecord::Migration[5.1]
  def change
    remove_column :hot_spots, :category_part_item_id, :integer
    remove_column :hot_spots, :item_type_id, :integer
    add_column :hot_spots, :api_sub_part_id, :integer
    add_column :hot_spots, :api_part_id, :integer
  end
end
