class AddOrderIdToMessages < ActiveRecord::Migration[5.1]
  def change
  	add_column :messages, :order_id, :integer
  end
end
