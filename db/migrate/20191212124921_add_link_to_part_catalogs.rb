class AddLinkToPartCatalogs < ActiveRecord::Migration[5.1]
  def change
  	add_column :part_catalogs, :link, :string
  	add_column :part_catalogs, :api_vehicle_model_id, :integer
  end
end
