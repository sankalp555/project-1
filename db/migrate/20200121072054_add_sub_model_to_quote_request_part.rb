class AddSubModelToQuoteRequestPart < ActiveRecord::Migration[5.1]
  def change
  	add_column :quotation_request_parts, :api_sub_model_id, :integer
  end
end
