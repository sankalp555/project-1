class CreateAddresses < ActiveRecord::Migration[5.1]
  def change
    create_table :addresses do |t|

      t.string :name
      t.string :street_address
      t.integer :number
      t.string :region
      t.string :city
      t.string :comuna
      t.integer :phone
      t.references :user
      t.boolean :primary, default: false
      t.timestamps
    end
  end
end
