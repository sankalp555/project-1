class CreateQuotationParts < ActiveRecord::Migration[5.1]
  def change
    create_table :quotation_parts do |t|
      t.references :quotation, foreign_key: true
      t.references :quotation_request_part, foreign_key: true
      t.integer :price
      t.integer :amount

      t.timestamps
    end
  end
end
