class CreateMarkups < ActiveRecord::Migration[5.1]
  def change
    create_table :markups do |t|
      t.references :supplier, index: true
      t.decimal :input
      t.string :role
      t.boolean :active
      t.timestamps
    end
  end
end
