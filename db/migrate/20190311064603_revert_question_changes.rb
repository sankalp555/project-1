class RevertQuestionChanges < ActiveRecord::Migration[5.1]
  def change
  	remove_index :my_questions, :quotation_request_part_id
  	remove_column :my_questions, :quotation_request_part_id
  	add_reference :my_questions, :quotation_part, index: true
  end
end
