class AddBrandNameToQuotation < ActiveRecord::Migration[5.1]
  def change
  	add_column :quotation_parts, :brand, :string
  end
end
