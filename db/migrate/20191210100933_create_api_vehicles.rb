class CreateApiVehicles < ActiveRecord::Migration[5.1]
  def change
    create_table :api_vehicles do |t|
      t.integer :api_sub_model_id
      t.string :ssd

      t.timestamps
    end
  end
end
