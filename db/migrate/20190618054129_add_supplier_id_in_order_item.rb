class AddSupplierIdInOrderItem < ActiveRecord::Migration[5.1]
  def change
  	add_column :order_items, :supplier_id, :integer, default: nil
  end

end
