class QuestionWithQuotationRequestPart < ActiveRecord::Migration[5.1]
  def change
    remove_column :my_questions, :quotation_part_id
    add_reference :my_questions, :quotation_request_part, index: true
  end
end
