class AddColumnActiveToSupplier < ActiveRecord::Migration[5.1]
  def change
    add_column :suppliers, :active, :boolean, default: false
  end
end
