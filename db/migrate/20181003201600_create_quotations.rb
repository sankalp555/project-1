class CreateQuotations < ActiveRecord::Migration[5.1]
  def change
    create_table :quotations do |t|
      t.integer :discount
      t.references :quotation_request, foreign_key: true
      t.references :supplier, foreign_key: true
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
