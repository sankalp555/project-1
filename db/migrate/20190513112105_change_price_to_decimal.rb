class ChangePriceToDecimal < ActiveRecord::Migration[5.1]
  def change
  	change_column :quotation_parts, :price, :decimal
  end
end
