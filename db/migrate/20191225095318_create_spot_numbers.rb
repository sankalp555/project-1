class CreateSpotNumbers < ActiveRecord::Migration[5.1]
  def change
    create_table :spot_numbers do |t|
      t.string :name
      t.string :top
      t.string :left
      t.string :width
      t.string :height
      t.integer :api_part_id

      t.timestamps
    end
  end
end