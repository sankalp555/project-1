class CreateReferralLinks < ActiveRecord::Migration[5.1]
  def change
    create_table :referral_links do |t|
      t.references :referral_instruction, index: true
      t.references :user, index: true
      t.string :refer_to
      t.string :refer_code
      t.boolean :used, default: false
      t.boolean :awarded, default: false
      t.timestamps
    end
  end
end
