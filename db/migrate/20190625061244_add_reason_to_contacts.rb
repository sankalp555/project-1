class AddReasonToContacts < ActiveRecord::Migration[5.1]
  def change
  	add_column :contacts, :reason, :string
  end
end
