class AddColumnToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :web_push_subscription, :json, default: {}
  end
end
