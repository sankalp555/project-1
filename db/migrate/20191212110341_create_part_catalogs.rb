class CreatePartCatalogs < ActiveRecord::Migration[5.1]
  def change
    create_table :part_catalogs do |t|
      t.string :name
      t.string :group_name
      t.string :logo

      t.timestamps
    end
  end
end