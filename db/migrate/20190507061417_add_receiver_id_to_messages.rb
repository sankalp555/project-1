class AddReceiverIdToMessages < ActiveRecord::Migration[5.1]
  def change
  	add_column :messages, :receiver_id, :integer
  	add_column :messages, :sent_at, :datetime
  end
end
