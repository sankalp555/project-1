class CreateCartItems < ActiveRecord::Migration[5.1]
  def change
    create_table :cart_items do |t|

      t.references :quotation_part
      t.references :cart
      t.integer :order_id, index: true
      t.timestamps
    end
  end
end
