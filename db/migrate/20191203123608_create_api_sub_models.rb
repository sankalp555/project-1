class CreateApiSubModels < ActiveRecord::Migration[5.1]
  def change
    create_table :api_sub_models do |t|
      t.references :api_model, foreign_key: true
      t.string :name
      t.timestamps
    end
  end
end
