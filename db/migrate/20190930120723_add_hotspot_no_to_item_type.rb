class AddHotspotNoToItemType < ActiveRecord::Migration[5.1]
  def change
    add_column :item_types, :hot_spot_no, :boolean, default: true
  end
end
