class AddCategoryToVehicle < ActiveRecord::Migration[5.1]
  def change
  	add_reference :vehicles, :category, index: true
  end
end
