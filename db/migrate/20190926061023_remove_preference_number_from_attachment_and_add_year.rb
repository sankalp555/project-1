class RemovePreferenceNumberFromAttachmentAndAddYear < ActiveRecord::Migration[5.1]
  def change
  	remove_column :attachments, :preference_number, :integer
  	add_column :attachments, :year, :integer
  	add_column :attachments, :model_id, :integer
  end
end
