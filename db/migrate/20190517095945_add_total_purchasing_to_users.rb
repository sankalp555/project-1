class AddTotalPurchasingToUsers < ActiveRecord::Migration[5.1]
  def change
  	add_column :users, :total_purchasing, :decimal, default: 0.0
  end
end
