class CreateReplyNotifications < ActiveRecord::Migration[5.1]
  def change
    create_table :reply_notifications do |t|
      t.text :reply
      t.boolean :read, default: false
      t.references :replyable, polymorphic: true, index: true
      t.timestamps
    end
  end
end
