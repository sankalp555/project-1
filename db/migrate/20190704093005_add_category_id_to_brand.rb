class AddCategoryIdToBrand < ActiveRecord::Migration[5.1]
  def change
    add_reference :brands, :category, index: true
  end
end
