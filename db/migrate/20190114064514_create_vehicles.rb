class CreateVehicles < ActiveRecord::Migration[5.1]
  def change
    create_table :vehicles do |t|
    	t.references :user, foreign_key: true
    	t.references :brand, foreign_key: true
    	t.references :model, foreign_key: true
      t.string :year
      t.string :chasis_number
      t.string :patent
      t.timestamps
    end
  end
end
