class AddSubPartCategoryPartItem < ActiveRecord::Migration[5.1]
  def change
    add_column :category_part_items, :sub_parts, :boolean
  end
end
