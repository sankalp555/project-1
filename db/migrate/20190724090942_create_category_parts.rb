class CreateCategoryParts < ActiveRecord::Migration[5.1]
  def change
    create_table :category_parts do |t|
      t.string :name
      t.boolean :active, default: false
      t.timestamps
    end
  end
end
