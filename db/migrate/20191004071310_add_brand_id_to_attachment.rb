class AddBrandIdToAttachment < ActiveRecord::Migration[5.1]
  def change
  	add_column :attachments, :brand_id, :integer
  end
end
