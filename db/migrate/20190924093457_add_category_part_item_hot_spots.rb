class AddCategoryPartItemHotSpots < ActiveRecord::Migration[5.1]
  def change
    add_reference :hot_spots, :category_part_item, index: true
  end
end
