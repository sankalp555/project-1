class CreateContacts < ActiveRecord::Migration[5.1]
  def change
    create_table :contacts do |t|
      t.string :full_name
      t.string :email
      t.string :brand
      t.text :comment
      t.string :company
      t.string :phone
      t.boolean :type_of_product
      t.timestamps
    end
  end
end
