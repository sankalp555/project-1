class AddActiveToCatalogsAndModels < ActiveRecord::Migration[5.1]
  def change
  	add_column :api_catalogs, :active, :boolean, default: true
  	add_column :api_models, :active, :boolean, default: true
  end
end
