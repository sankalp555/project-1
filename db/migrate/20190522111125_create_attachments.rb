class CreateAttachments < ActiveRecord::Migration[5.1]
  def change
    create_table :attachments do |t|

      t.string :attachment
      t.integer  :attached_item_id
      t.string  :attached_item_type
      t.string :name
      t.references :user
      t.timestamps
    end
  end
end
