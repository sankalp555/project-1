class AddItemTypeToCategoryPartItem < ActiveRecord::Migration[5.1]
  def change
  	remove_column :category_part_items, :item_type, :string
  	remove_column :category_part_items, :model_id
  	add_reference :category_part_items, :item_type, index: true
  end
end
