class CreateMyQuestions < ActiveRecord::Migration[5.1]
  def change
    create_table :my_questions do |t|
      t.references :quotation_part, index: true
      t.text :question
      t.boolean :read, default: false 
      t.timestamps
    end
  end
end
