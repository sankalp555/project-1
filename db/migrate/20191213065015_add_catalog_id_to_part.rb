class AddCatalogIdToPart < ActiveRecord::Migration[5.1]
  def change
    add_column :part_catalogs, :api_catalog_id, :integer
    add_column :api_sub_models, :logo, :string
    add_column :api_vehicles, :vin, :string
    add_column :api_vehicles, :frame, :string
  end
end
