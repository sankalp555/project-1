class AddDescriptionToAttachments < ActiveRecord::Migration[5.1]
  def change
  	add_column :attachments, :description, :text
  	add_column :attachments, :preference_number, :integer
  end
end
