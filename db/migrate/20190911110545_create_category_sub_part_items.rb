class CreateCategorySubPartItems < ActiveRecord::Migration[5.1]
  def change
    create_table :category_sub_part_items do |t|
      t.references :category_part_item, index: true
      t.boolean :front
      t.boolean :rear
      t.boolean :right
      t.boolean :left
      t.boolean :all
      t.timestamps
    end
  end
end
