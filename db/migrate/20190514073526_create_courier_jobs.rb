class CreateCourierJobs < ActiveRecord::Migration[5.1]
  def change
    create_table :courier_jobs do |t|

      t.references :order
      t.references :courier_company
      t.string :tracking_number
      t.date :initial_date
      t.date :final_date
      t.text :message
      t.string :status
      t.boolean :send_detail, default: :true
      t.timestamps
    end
  end
end
