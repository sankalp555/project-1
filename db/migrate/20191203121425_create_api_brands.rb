class CreateApiBrands < ActiveRecord::Migration[5.1]
  def change
    create_table :api_brands do |t|
      t.string :name
      t.integer :brand_code
      t.string :logo
      t.string :logo_string
      t.string :supportframesearch
      t.string :frameexample
      t.string :supportvinsearch
      t.string :vinexample
      t.string :universal_classifier
      t.string :type_moto
      t.string :type_auto
      t.string :type_transport
      t.timestamps
    end
  end
end
