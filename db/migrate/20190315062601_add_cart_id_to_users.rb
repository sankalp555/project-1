class AddCartIdToUsers < ActiveRecord::Migration[5.1]
  def change
  	add_reference :users, :cart, index: true
  end
end
