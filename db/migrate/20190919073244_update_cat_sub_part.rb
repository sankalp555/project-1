class UpdateCatSubPart < ActiveRecord::Migration[5.1]
  def change
    remove_column :category_sub_part_items, :front, :boolean
    remove_column :category_sub_part_items, :rear, :boolean
    remove_column :category_sub_part_items, :right, :boolean
    remove_column :category_sub_part_items, :left, :boolean
    remove_column :category_sub_part_items, :all, :boolean
    remove_column :category_part_items, :sub_parts, :boolean
    add_reference :category_sub_part_items, :category_sub_option, index: true

  end
end
