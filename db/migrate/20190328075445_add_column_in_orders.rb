class AddColumnInOrders < ActiveRecord::Migration[5.1]
  def change
  	add_reference :orders, :quotation_part, index: true
  	add_reference :orders, :address, index: true
  	add_column :orders, :quantity, :integer
  	add_column :orders, :price, :decimal
  	add_column :orders, :notification_params, :string
  	add_column :orders, :transaction_id, :string
  	add_column :orders, :purchased_at, :datetime
  end
end
