class CreateApiVehicleModels < ActiveRecord::Migration[5.1]
  def change
    create_table :api_vehicle_models do |t|
      t.integer :api_vehicle_id
      t.string :release_date
      t.string :market
      t.string :engine

      t.timestamps
    end
  end
end