class AddColumnStatusToOrderItem < ActiveRecord::Migration[5.1]
  def change
    add_column :order_items, :status, :string
    remove_column :orders, :address_id, :integer
  end
end
