class AddSupplierIdToOrders < ActiveRecord::Migration[5.1]
  def change
  	add_reference :orders, :supplier, index: true
  end
end
