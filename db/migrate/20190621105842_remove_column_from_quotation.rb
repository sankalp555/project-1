class RemoveColumnFromQuotation < ActiveRecord::Migration[5.1]
  def change
  	remove_column :quotations, :discount, :integer
  end
end
