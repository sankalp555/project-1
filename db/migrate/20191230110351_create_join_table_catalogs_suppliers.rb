class CreateJoinTableCatalogsSuppliers < ActiveRecord::Migration[5.1]
  def change
  	create_join_table :api_catalogs, :suppliers do |t|
      t.index [:supplier_id, :api_catalog_id]
    end
  end
end
