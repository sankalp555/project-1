class QuotationRequestPartColumnChanges < ActiveRecord::Migration[5.1]
  def change
    remove_column :quotation_request_parts, :vehicle_id, :integer
    remove_column :quotation_request_parts, :category_part_item_id, :integer
    remove_column :quotation_request_parts, :sub_option_id, :integer
    add_column :quotation_request_parts, :api_vehicle_id, :integer
    add_column :quotation_request_parts, :api_sub_part_id, :integer
  end
end
