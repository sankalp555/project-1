class CreatePayments < ActiveRecord::Migration[5.1]
  def change
    create_table :payments do |t|
      t.decimal :total_amount, default: 0.0
      t.integer :order_id
      t.string :payment_gateway
      t.string :payment_type
      t.string :authorization_code
      t.string :status  

      t.timestamps
    end
  end
end