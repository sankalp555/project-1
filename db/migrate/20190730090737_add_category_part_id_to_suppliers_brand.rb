class AddCategoryPartIdToSuppliersBrand < ActiveRecord::Migration[5.1]
  def change
  	add_reference :brands_suppliers, :category_part, index: true
  end
end
