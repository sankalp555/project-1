class AddItemTypeToCategoryPart < ActiveRecord::Migration[5.1]
  def change
  	add_reference :item_types, :category_part, index: true
  end
end
