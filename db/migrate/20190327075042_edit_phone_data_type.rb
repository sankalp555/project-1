class EditPhoneDataType < ActiveRecord::Migration[5.1]
  def change
  	remove_column :addresses, :phone, :integer
  	add_column :addresses, :phone, :bigint
  end
end
