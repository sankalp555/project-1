class RemoveColumnsFromQuotationRequestPart < ActiveRecord::Migration[5.1]
  def change
    remove_column :quotation_request_parts, :model_id, :integer
    remove_column :quotation_request_parts, :year, :date
    remove_column :quotation_request_parts, :chasis_number, :string
    add_column :quotation_request_parts, :vehicle_id, :integer
  end
end
