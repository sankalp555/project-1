class MakeJsonbToJson < ActiveRecord::Migration[5.1]
  def change
  	remove_column :api_vehicle_models, :additional_info, :json, default: {}
  	add_column :api_vehicle_models, :additional_info, :jsonb, default: {}
  end
end
