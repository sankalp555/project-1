class CreateNotificationUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :notification_users do |t|
    	t.integer :notify_id
    	t.string :email
    	t.string :auth_key

      t.timestamps
    end
  end
end
