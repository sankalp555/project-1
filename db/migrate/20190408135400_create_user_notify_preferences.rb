class CreateUserNotifyPreferences < ActiveRecord::Migration[5.1]
  def change
    create_table :user_notify_preferences do |t|
      t.references :user, index: true
      t.boolean :new_conf_shipments, default: false
      t.boolean :promo_discount, default: false
      t.boolean :payment, default: false
      t.boolean :dispatch_delivery, default: false     
      t.timestamps
    end
  end
end
