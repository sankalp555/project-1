class AddPhoneToUsers < ActiveRecord::Migration[5.1]
  def change
  	add_column :users, :cellular, :string
  	add_column :users, :office, :string
  end
end
