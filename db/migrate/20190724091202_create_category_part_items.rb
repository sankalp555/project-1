class CreateCategoryPartItems < ActiveRecord::Migration[5.1]
  def change
    create_table :category_part_items do |t|
    	t.string :name
    	t.boolean :active, default: false
    	t.references :category_part, index: true
    	t.references :model, index: true
      t.timestamps
    end
  end
end
