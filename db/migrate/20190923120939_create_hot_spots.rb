class CreateHotSpots < ActiveRecord::Migration[5.1]
  def change
    create_table :hot_spots do |t|
      t.float :x_position
      t.float :y_position
      t.text :note
      t.references :item_type, index: true
      t.references :attachment, index: true
      t.timestamps
    end
  end
end
