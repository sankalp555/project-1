class AddActiveToPartCatalogs < ActiveRecord::Migration[5.1]
  def change
    add_column :part_catalogs, :active, :boolean, default: :true
    add_column :api_parts, :active, :boolean, default: :true
  end
end
