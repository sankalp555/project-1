class ChangeQuotationPartsColumns < ActiveRecord::Migration[5.1]
  def change
  	remove_column :quotation_parts, :alternative, :boolean
  	remove_column :quotation_parts, :original, :boolean
  	add_column :quotation_parts, :type_of_request, :string, default: 'original'
  	add_column :quotation_parts, :shipping_type, :string, default: 'deliver'
  	add_column :quotation_parts, :shipping_price, :decimal
  end
end
