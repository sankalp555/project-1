class AddColumnStatusToQuotationRequest < ActiveRecord::Migration[5.1]
  def change
    add_column :quotation_requests, :status, :string, default: 'pending'
  end
end
