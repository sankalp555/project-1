class CreateOrderItem < ActiveRecord::Migration[5.1]
  def change
    create_table :order_items do |t|
      t.references :quotation_part, index: true
      t.references :order, index: true
      t.timestamps
    end
  end
end
