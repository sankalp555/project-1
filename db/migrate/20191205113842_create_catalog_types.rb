class CreateCatalogTypes < ActiveRecord::Migration[5.1]
  def change
    create_table :catalog_types do |t|
      t.string :name
      t.string :catalog_type

      t.timestamps
    end
  end
end
