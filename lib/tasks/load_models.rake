namespace :data do 
  task :load_models => :environment do 
    brands = {}
    ActiveRecord::Base.transaction do
      File.read('tmp/brands+models.csv').split("\n").each do |row|
        puts row
        brand_name, model_name = row.split(",")
        brand = brands[brand_name] || Brand.find_or_create_by!(name: brand_name)
        Model.create!(name: model_name, brand: brand)
      end
    end
  end
end
