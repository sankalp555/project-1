namespace :carts do
  desc 'Destroy cart after sometime'
  task :delete_cart => :environment do
    cart_items = CartItem.where('DATE(created_at) <= ?', Date.today.beginning_of_day - 3.day).destroy_all
  end
end
